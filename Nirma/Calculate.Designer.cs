﻿namespace Nirma
{
    partial class Calculate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbType = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbFittings = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.lblSubcode = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.LBLoP = new System.Windows.Forms.Label();
            this.txtMASS_FLOW_RATE = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnInsertNewForm = new System.Windows.Forms.Button();
            this.btnNewCalculation = new System.Windows.Forms.Button();
            this.btnCalculate = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.txtGAMMA = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.cmbMassFlMes = new System.Windows.Forms.ComboBox();
            this.txtVISC = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtMOLEQ_WEIGHT = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtTEMP = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblFittingLabel = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.dgvMolGrid = new System.Windows.Forms.DataGridView();
            this.panel12 = new System.Windows.Forms.Panel();
            this.cmbBUCKET_DIA = new System.Windows.Forms.ComboBox();
            this.txtBUCKET_DIA = new System.Windows.Forms.TextBox();
            this.lblBUCKET_DIA = new System.Windows.Forms.Label();
            this.cmbSEAL_DIA = new System.Windows.Forms.ComboBox();
            this.txtSEAL_DIA = new System.Windows.Forms.TextBox();
            this.lblSEAL_DIA = new System.Windows.Forms.Label();
            this.cmbLENGTH_PIPE1 = new System.Windows.Forms.ComboBox();
            this.txtLENGTH_PIPE1 = new System.Windows.Forms.TextBox();
            this.lblLENGTH_PIPE1 = new System.Windows.Forms.Label();
            this.cmbMINOR_AXIS = new System.Windows.Forms.ComboBox();
            this.txtMINOR_AXIS = new System.Windows.Forms.TextBox();
            this.lblMINOR_AXIS = new System.Windows.Forms.Label();
            this.cmbMAJOR_AXIS = new System.Windows.Forms.ComboBox();
            this.txtMAJOR_AXIS = new System.Windows.Forms.TextBox();
            this.lblMAJOR_AXIS = new System.Windows.Forms.Label();
            this.cmbPIPE_DIA1 = new System.Windows.Forms.ComboBox();
            this.txtPIPE_DIA1 = new System.Windows.Forms.TextBox();
            this.lblPIPE_DIA1 = new System.Windows.Forms.Label();
            this.cmbLENGTH_PIPE = new System.Windows.Forms.ComboBox();
            this.cmbRoughMes = new System.Windows.Forms.ComboBox();
            this.txtTHETA = new System.Windows.Forms.TextBox();
            this.lblTHETA = new System.Windows.Forms.Label();
            this.txtRbyD = new System.Windows.Forms.TextBox();
            this.lblRbyD = new System.Windows.Forms.Label();
            this.txtLENGTH_PIPE = new System.Windows.Forms.TextBox();
            this.lblLENGTH_PIPE = new System.Windows.Forms.Label();
            this.cmbPST0Mes = new System.Windows.Forms.ComboBox();
            this.cmbPipeDiaMes = new System.Windows.Forms.ComboBox();
            this.panel13 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.txtPST0 = new System.Windows.Forms.TextBox();
            this.lblPST0 = new System.Windows.Forms.Label();
            this.txtROUGHNESS = new System.Windows.Forms.TextBox();
            this.lblROUGHNESS = new System.Windows.Forms.Label();
            this.txtFITTING_QUANTITY = new System.Windows.Forms.TextBox();
            this.lblFITTING_QUANTITY = new System.Windows.Forms.Label();
            this.txtPIPE_DIA = new System.Windows.Forms.TextBox();
            this.lblPIPE_DIA = new System.Windows.Forms.Label();
            this.panel14 = new System.Windows.Forms.Panel();
            this.FittingPic = new System.Windows.Forms.PictureBox();
            this.dgvGridFinalOutPut = new System.Windows.Forms.DataGridView();
            this.btnFinalOP = new System.Windows.Forms.Button();
            this.panel10.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMolGrid)).BeginInit();
            this.panel12.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FittingPic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGridFinalOutPut)).BeginInit();
            this.SuspendLayout();
            // 
            // cmbType
            // 
            this.cmbType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbType.FormattingEnabled = true;
            this.cmbType.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3"});
            this.cmbType.Location = new System.Drawing.Point(335, 151);
            this.cmbType.Name = "cmbType";
            this.cmbType.Size = new System.Drawing.Size(325, 21);
            this.cmbType.TabIndex = 6;
            this.cmbType.SelectedIndexChanged += new System.EventHandler(this.cmbType_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(284, 152);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 15);
            this.label1.TabIndex = 93;
            this.label1.Text = "Types :";
            // 
            // cmbFittings
            // 
            this.cmbFittings.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFittings.FormattingEnabled = true;
            this.cmbFittings.Items.AddRange(new object[] {
            "Orifice",
            "Flare Tip",
            "Pipe Exit",
            "Velocity Seal",
            "Molecular Seal",
            "Straight Pipe",
            "Water Seal Drum",
            "Sudden Contaction",
            "Sudden Expansion",
            "Gradual Contraction",
            "Gradual Expansion",
            "Elbow",
            "Mitre Bend",
            "Annular Section",
            "Rectangular Duct",
            "Rectangular Elbow",
            "T Thru",
            "T Branch to Run Flow",
            "T Run to Branch Flow",
            "Gate valve",
            "Ball Valve",
            "Globe valve",
            "Butterfly Valve",
            "Check valve",
            "Angle valve",
            "Coupling",
            "Diaphram valve",
            "Foot valve",
            "Plug Cock",
            "Union"});
            this.cmbFittings.Location = new System.Drawing.Point(103, 152);
            this.cmbFittings.Name = "cmbFittings";
            this.cmbFittings.Size = new System.Drawing.Size(158, 21);
            this.cmbFittings.TabIndex = 5;
            this.cmbFittings.SelectedIndexChanged += new System.EventHandler(this.cmbFittings_SelectedIndexChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(51, 152);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(52, 15);
            this.label14.TabIndex = 90;
            this.label14.Text = "Fittings :";
            // 
            // lblSubcode
            // 
            this.lblSubcode.AutoSize = true;
            this.lblSubcode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSubcode.Location = new System.Drawing.Point(95, 153);
            this.lblSubcode.Name = "lblSubcode";
            this.lblSubcode.Size = new System.Drawing.Size(0, 15);
            this.lblSubcode.TabIndex = 79;
            this.lblSubcode.Visible = false;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.GhostWhite;
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel10.Controls.Add(this.LBLoP);
            this.panel10.Location = new System.Drawing.Point(637, 37);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(347, 71);
            this.panel10.TabIndex = 12;
            // 
            // LBLoP
            // 
            this.LBLoP.AutoSize = true;
            this.LBLoP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBLoP.Location = new System.Drawing.Point(6, 3);
            this.LBLoP.Name = "LBLoP";
            this.LBLoP.Size = new System.Drawing.Size(49, 15);
            this.LBLoP.TabIndex = 41;
            this.LBLoP.Text = "Output :";
            // 
            // txtMASS_FLOW_RATE
            // 
            this.txtMASS_FLOW_RATE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMASS_FLOW_RATE.Location = new System.Drawing.Point(262, 38);
            this.txtMASS_FLOW_RATE.MaxLength = 20;
            this.txtMASS_FLOW_RATE.Name = "txtMASS_FLOW_RATE";
            this.txtMASS_FLOW_RATE.Size = new System.Drawing.Size(258, 20);
            this.txtMASS_FLOW_RATE.TabIndex = 1;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(67, 41);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(189, 15);
            this.label9.TabIndex = 48;
            this.label9.Text = "Enter the value of Mass flow rate :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Brown;
            this.label8.Location = new System.Drawing.Point(7, 6);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(92, 18);
            this.label8.TabIndex = 0;
            this.label8.Text = "Fitting XXX";
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.Firebrick;
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel9.Location = new System.Drawing.Point(5, 515);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(989, 10);
            this.panel9.TabIndex = 88;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.Firebrick;
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Location = new System.Drawing.Point(6, 561);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(989, 10);
            this.panel7.TabIndex = 89;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Firebrick;
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Location = new System.Drawing.Point(5, 247);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(989, 10);
            this.panel6.TabIndex = 86;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.GhostWhite;
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.btnFinalOP);
            this.panel5.Controls.Add(this.btnInsertNewForm);
            this.panel5.Controls.Add(this.btnNewCalculation);
            this.panel5.Controls.Add(this.btnCalculate);
            this.panel5.Controls.Add(this.btnExit);
            this.panel5.Location = new System.Drawing.Point(6, 528);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(989, 30);
            this.panel5.TabIndex = 2000;
            // 
            // btnInsertNewForm
            // 
            this.btnInsertNewForm.Location = new System.Drawing.Point(295, 2);
            this.btnInsertNewForm.Name = "btnInsertNewForm";
            this.btnInsertNewForm.Size = new System.Drawing.Size(135, 23);
            this.btnInsertNewForm.TabIndex = 2004;
            this.btnInsertNewForm.Text = "Insert New Form";
            this.btnInsertNewForm.UseVisualStyleBackColor = true;
            this.btnInsertNewForm.Click += new System.EventHandler(this.btnInsertNewForm_Click);
            // 
            // btnNewCalculation
            // 
            this.btnNewCalculation.Location = new System.Drawing.Point(503, 3);
            this.btnNewCalculation.Name = "btnNewCalculation";
            this.btnNewCalculation.Size = new System.Drawing.Size(135, 23);
            this.btnNewCalculation.TabIndex = 2001;
            this.btnNewCalculation.Text = "New Measure";
            this.btnNewCalculation.UseVisualStyleBackColor = true;
            this.btnNewCalculation.Click += new System.EventHandler(this.btnNewCalculation_Click);
            // 
            // btnCalculate
            // 
            this.btnCalculate.Enabled = false;
            this.btnCalculate.Location = new System.Drawing.Point(711, 2);
            this.btnCalculate.Name = "btnCalculate";
            this.btnCalculate.Size = new System.Drawing.Size(135, 23);
            this.btnCalculate.TabIndex = 2000;
            this.btnCalculate.Text = "Calculate";
            this.btnCalculate.UseVisualStyleBackColor = true;
            this.btnCalculate.Click += new System.EventHandler(this.btnCalculate_Click);
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(895, 2);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 2003;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.GhostWhite;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.txtGAMMA);
            this.panel4.Controls.Add(this.label13);
            this.panel4.Controls.Add(this.panel8);
            this.panel4.Controls.Add(this.cmbMassFlMes);
            this.panel4.Controls.Add(this.txtVISC);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Controls.Add(this.txtMOLEQ_WEIGHT);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Controls.Add(this.txtTEMP);
            this.panel4.Controls.Add(this.label2);
            this.panel4.Controls.Add(this.cmbType);
            this.panel4.Controls.Add(this.label1);
            this.panel4.Controls.Add(this.cmbFittings);
            this.panel4.Controls.Add(this.label14);
            this.panel4.Controls.Add(this.lblSubcode);
            this.panel4.Controls.Add(this.panel10);
            this.panel4.Controls.Add(this.txtMASS_FLOW_RATE);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Location = new System.Drawing.Point(6, 61);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(989, 180);
            this.panel4.TabIndex = 1;
            // 
            // txtGAMMA
            // 
            this.txtGAMMA.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtGAMMA.Location = new System.Drawing.Point(798, 121);
            this.txtGAMMA.MaxLength = 20;
            this.txtGAMMA.Name = "txtGAMMA";
            this.txtGAMMA.Size = new System.Drawing.Size(186, 20);
            this.txtGAMMA.TabIndex = 102;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(534, 119);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(258, 15);
            this.label13.TabIndex = 103;
            this.label13.Text = "Enter the value of ratio of Secific heat Gamma:";
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.GhostWhite;
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.label12);
            this.panel8.Location = new System.Drawing.Point(3, 1);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(979, 30);
            this.panel8.TabIndex = 101;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Brown;
            this.label12.Location = new System.Drawing.Point(7, 6);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(116, 18);
            this.label12.TabIndex = 0;
            this.label12.Text = "Primary Inputs";
            // 
            // cmbMassFlMes
            // 
            this.cmbMassFlMes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMassFlMes.FormattingEnabled = true;
            this.cmbMassFlMes.Items.AddRange(new object[] {
            "in Kg/s",
            "in Tons/s"});
            this.cmbMassFlMes.Location = new System.Drawing.Point(536, 38);
            this.cmbMassFlMes.Name = "cmbMassFlMes";
            this.cmbMassFlMes.Size = new System.Drawing.Size(66, 21);
            this.cmbMassFlMes.TabIndex = 100;
            // 
            // txtVISC
            // 
            this.txtVISC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtVISC.Location = new System.Drawing.Point(262, 121);
            this.txtVISC.MaxLength = 20;
            this.txtVISC.Name = "txtVISC";
            this.txtVISC.Size = new System.Drawing.Size(258, 20);
            this.txtVISC.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(70, 119);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(191, 15);
            this.label4.TabIndex = 99;
            this.label4.Text = "Enter the value of Viscoaity in Pas:";
            // 
            // txtMOLEQ_WEIGHT
            // 
            this.txtMOLEQ_WEIGHT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMOLEQ_WEIGHT.Location = new System.Drawing.Point(262, 94);
            this.txtMOLEQ_WEIGHT.MaxLength = 20;
            this.txtMOLEQ_WEIGHT.Name = "txtMOLEQ_WEIGHT";
            this.txtMOLEQ_WEIGHT.Size = new System.Drawing.Size(258, 20);
            this.txtMOLEQ_WEIGHT.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(-1, 93);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(262, 15);
            this.label3.TabIndex = 97;
            this.label3.Text = "Enter the value of Molecular Weight in kg/kmol:";
            // 
            // txtTEMP
            // 
            this.txtTEMP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTEMP.Location = new System.Drawing.Point(262, 67);
            this.txtTEMP.MaxLength = 20;
            this.txtTEMP.Name = "txtTEMP";
            this.txtTEMP.Size = new System.Drawing.Size(258, 20);
            this.txtTEMP.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(34, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(227, 15);
            this.label2.TabIndex = 95;
            this.label2.Text = "Enter the value of Temperature in Kelvin:";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.GhostWhite;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.lblFittingLabel);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Location = new System.Drawing.Point(6, 16);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(989, 30);
            this.panel2.TabIndex = 83;
            // 
            // lblFittingLabel
            // 
            this.lblFittingLabel.AutoSize = true;
            this.lblFittingLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFittingLabel.ForeColor = System.Drawing.Color.Brown;
            this.lblFittingLabel.Location = new System.Drawing.Point(99, 6);
            this.lblFittingLabel.Name = "lblFittingLabel";
            this.lblFittingLabel.Size = new System.Drawing.Size(0, 18);
            this.lblFittingLabel.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Firebrick;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Location = new System.Drawing.Point(6, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(989, 10);
            this.panel1.TabIndex = 81;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Firebrick;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Location = new System.Drawing.Point(6, 48);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(989, 10);
            this.panel3.TabIndex = 82;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.GhostWhite;
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel11.Controls.Add(this.dgvMolGrid);
            this.panel11.Location = new System.Drawing.Point(3, 263);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(232, 297);
            this.panel11.TabIndex = 109;
            // 
            // dgvMolGrid
            // 
            this.dgvMolGrid.AllowUserToAddRows = false;
            this.dgvMolGrid.AllowUserToDeleteRows = false;
            this.dgvMolGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMolGrid.Location = new System.Drawing.Point(3, 3);
            this.dgvMolGrid.Name = "dgvMolGrid";
            this.dgvMolGrid.Size = new System.Drawing.Size(224, 286);
            this.dgvMolGrid.TabIndex = 0;
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.GhostWhite;
            this.panel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel12.Controls.Add(this.cmbBUCKET_DIA);
            this.panel12.Controls.Add(this.txtBUCKET_DIA);
            this.panel12.Controls.Add(this.lblBUCKET_DIA);
            this.panel12.Controls.Add(this.cmbSEAL_DIA);
            this.panel12.Controls.Add(this.txtSEAL_DIA);
            this.panel12.Controls.Add(this.lblSEAL_DIA);
            this.panel12.Controls.Add(this.cmbLENGTH_PIPE1);
            this.panel12.Controls.Add(this.txtLENGTH_PIPE1);
            this.panel12.Controls.Add(this.lblLENGTH_PIPE1);
            this.panel12.Controls.Add(this.cmbMINOR_AXIS);
            this.panel12.Controls.Add(this.txtMINOR_AXIS);
            this.panel12.Controls.Add(this.lblMINOR_AXIS);
            this.panel12.Controls.Add(this.cmbMAJOR_AXIS);
            this.panel12.Controls.Add(this.txtMAJOR_AXIS);
            this.panel12.Controls.Add(this.lblMAJOR_AXIS);
            this.panel12.Controls.Add(this.cmbPIPE_DIA1);
            this.panel12.Controls.Add(this.txtPIPE_DIA1);
            this.panel12.Controls.Add(this.lblPIPE_DIA1);
            this.panel12.Controls.Add(this.cmbLENGTH_PIPE);
            this.panel12.Controls.Add(this.cmbRoughMes);
            this.panel12.Controls.Add(this.txtTHETA);
            this.panel12.Controls.Add(this.lblTHETA);
            this.panel12.Controls.Add(this.txtRbyD);
            this.panel12.Controls.Add(this.lblRbyD);
            this.panel12.Controls.Add(this.txtLENGTH_PIPE);
            this.panel12.Controls.Add(this.lblLENGTH_PIPE);
            this.panel12.Controls.Add(this.cmbPST0Mes);
            this.panel12.Controls.Add(this.cmbPipeDiaMes);
            this.panel12.Controls.Add(this.panel13);
            this.panel12.Controls.Add(this.txtPST0);
            this.panel12.Controls.Add(this.lblPST0);
            this.panel12.Controls.Add(this.txtROUGHNESS);
            this.panel12.Controls.Add(this.lblROUGHNESS);
            this.panel12.Controls.Add(this.txtFITTING_QUANTITY);
            this.panel12.Controls.Add(this.lblFITTING_QUANTITY);
            this.panel12.Controls.Add(this.txtPIPE_DIA);
            this.panel12.Controls.Add(this.lblPIPE_DIA);
            this.panel12.Location = new System.Drawing.Point(6, 263);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(989, 246);
            this.panel12.TabIndex = 7;
            // 
            // cmbBUCKET_DIA
            // 
            this.cmbBUCKET_DIA.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBUCKET_DIA.FormattingEnabled = true;
            this.cmbBUCKET_DIA.Items.AddRange(new object[] {
            "in m",
            "in mm"});
            this.cmbBUCKET_DIA.Location = new System.Drawing.Point(921, 150);
            this.cmbBUCKET_DIA.Name = "cmbBUCKET_DIA";
            this.cmbBUCKET_DIA.Size = new System.Drawing.Size(61, 21);
            this.cmbBUCKET_DIA.TabIndex = 172;
            // 
            // txtBUCKET_DIA
            // 
            this.txtBUCKET_DIA.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBUCKET_DIA.Location = new System.Drawing.Point(751, 150);
            this.txtBUCKET_DIA.MaxLength = 20;
            this.txtBUCKET_DIA.Name = "txtBUCKET_DIA";
            this.txtBUCKET_DIA.Size = new System.Drawing.Size(164, 20);
            this.txtBUCKET_DIA.TabIndex = 170;
            // 
            // lblBUCKET_DIA
            // 
            this.lblBUCKET_DIA.AutoSize = true;
            this.lblBUCKET_DIA.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBUCKET_DIA.Location = new System.Drawing.Point(538, 150);
            this.lblBUCKET_DIA.Name = "lblBUCKET_DIA";
            this.lblBUCKET_DIA.Size = new System.Drawing.Size(207, 15);
            this.lblBUCKET_DIA.TabIndex = 171;
            this.lblBUCKET_DIA.Text = "Value of Inlet Pipe Diameter (dpipe) :";
            // 
            // cmbSEAL_DIA
            // 
            this.cmbSEAL_DIA.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSEAL_DIA.FormattingEnabled = true;
            this.cmbSEAL_DIA.Items.AddRange(new object[] {
            "in m",
            "in mm"});
            this.cmbSEAL_DIA.Location = new System.Drawing.Point(921, 124);
            this.cmbSEAL_DIA.Name = "cmbSEAL_DIA";
            this.cmbSEAL_DIA.Size = new System.Drawing.Size(61, 21);
            this.cmbSEAL_DIA.TabIndex = 169;
            // 
            // txtSEAL_DIA
            // 
            this.txtSEAL_DIA.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSEAL_DIA.Location = new System.Drawing.Point(751, 124);
            this.txtSEAL_DIA.MaxLength = 20;
            this.txtSEAL_DIA.Name = "txtSEAL_DIA";
            this.txtSEAL_DIA.Size = new System.Drawing.Size(164, 20);
            this.txtSEAL_DIA.TabIndex = 167;
            // 
            // lblSEAL_DIA
            // 
            this.lblSEAL_DIA.AutoSize = true;
            this.lblSEAL_DIA.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSEAL_DIA.Location = new System.Drawing.Point(528, 124);
            this.lblSEAL_DIA.Name = "lblSEAL_DIA";
            this.lblSEAL_DIA.Size = new System.Drawing.Size(215, 15);
            this.lblSEAL_DIA.TabIndex = 168;
            this.lblSEAL_DIA.Text = "Value of Water Seal Diameter (dseal) :";
            // 
            // cmbLENGTH_PIPE1
            // 
            this.cmbLENGTH_PIPE1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLENGTH_PIPE1.FormattingEnabled = true;
            this.cmbLENGTH_PIPE1.Items.AddRange(new object[] {
            "in m",
            "in mm"});
            this.cmbLENGTH_PIPE1.Location = new System.Drawing.Point(921, 98);
            this.cmbLENGTH_PIPE1.Name = "cmbLENGTH_PIPE1";
            this.cmbLENGTH_PIPE1.Size = new System.Drawing.Size(61, 21);
            this.cmbLENGTH_PIPE1.TabIndex = 166;
            // 
            // txtLENGTH_PIPE1
            // 
            this.txtLENGTH_PIPE1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLENGTH_PIPE1.Location = new System.Drawing.Point(751, 98);
            this.txtLENGTH_PIPE1.MaxLength = 20;
            this.txtLENGTH_PIPE1.Name = "txtLENGTH_PIPE1";
            this.txtLENGTH_PIPE1.Size = new System.Drawing.Size(164, 20);
            this.txtLENGTH_PIPE1.TabIndex = 164;
            // 
            // lblLENGTH_PIPE1
            // 
            this.lblLENGTH_PIPE1.AutoSize = true;
            this.lblLENGTH_PIPE1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLENGTH_PIPE1.Location = new System.Drawing.Point(556, 98);
            this.lblLENGTH_PIPE1.Name = "lblLENGTH_PIPE1";
            this.lblLENGTH_PIPE1.Size = new System.Drawing.Size(186, 15);
            this.lblLENGTH_PIPE1.TabIndex = 165;
            this.lblLENGTH_PIPE1.Text = "Enter the value of Pipe Length L :";
            // 
            // cmbMINOR_AXIS
            // 
            this.cmbMINOR_AXIS.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMINOR_AXIS.FormattingEnabled = true;
            this.cmbMINOR_AXIS.Items.AddRange(new object[] {
            "in m",
            "in mm"});
            this.cmbMINOR_AXIS.Location = new System.Drawing.Point(734, 215);
            this.cmbMINOR_AXIS.Name = "cmbMINOR_AXIS";
            this.cmbMINOR_AXIS.Size = new System.Drawing.Size(61, 21);
            this.cmbMINOR_AXIS.TabIndex = 163;
            // 
            // txtMINOR_AXIS
            // 
            this.txtMINOR_AXIS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMINOR_AXIS.Location = new System.Drawing.Point(562, 214);
            this.txtMINOR_AXIS.MaxLength = 20;
            this.txtMINOR_AXIS.Name = "txtMINOR_AXIS";
            this.txtMINOR_AXIS.Size = new System.Drawing.Size(164, 20);
            this.txtMINOR_AXIS.TabIndex = 161;
            // 
            // lblMINOR_AXIS
            // 
            this.lblMINOR_AXIS.AutoSize = true;
            this.lblMINOR_AXIS.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMINOR_AXIS.Location = new System.Drawing.Point(402, 216);
            this.lblMINOR_AXIS.Name = "lblMINOR_AXIS";
            this.lblMINOR_AXIS.Size = new System.Drawing.Size(158, 15);
            this.lblMINOR_AXIS.TabIndex = 162;
            this.lblMINOR_AXIS.Text = "Hight of the Square Duct H :";
            // 
            // cmbMAJOR_AXIS
            // 
            this.cmbMAJOR_AXIS.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMAJOR_AXIS.FormattingEnabled = true;
            this.cmbMAJOR_AXIS.Items.AddRange(new object[] {
            "in m",
            "in mm"});
            this.cmbMAJOR_AXIS.Location = new System.Drawing.Point(335, 212);
            this.cmbMAJOR_AXIS.Name = "cmbMAJOR_AXIS";
            this.cmbMAJOR_AXIS.Size = new System.Drawing.Size(61, 21);
            this.cmbMAJOR_AXIS.TabIndex = 160;
            // 
            // txtMAJOR_AXIS
            // 
            this.txtMAJOR_AXIS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMAJOR_AXIS.Location = new System.Drawing.Point(163, 211);
            this.txtMAJOR_AXIS.MaxLength = 20;
            this.txtMAJOR_AXIS.Name = "txtMAJOR_AXIS";
            this.txtMAJOR_AXIS.Size = new System.Drawing.Size(164, 20);
            this.txtMAJOR_AXIS.TabIndex = 158;
            // 
            // lblMAJOR_AXIS
            // 
            this.lblMAJOR_AXIS.AutoSize = true;
            this.lblMAJOR_AXIS.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMAJOR_AXIS.Location = new System.Drawing.Point(3, 213);
            this.lblMAJOR_AXIS.Name = "lblMAJOR_AXIS";
            this.lblMAJOR_AXIS.Size = new System.Drawing.Size(159, 15);
            this.lblMAJOR_AXIS.TabIndex = 159;
            this.lblMAJOR_AXIS.Text = "Width of the Square Duct B :";
            // 
            // cmbPIPE_DIA1
            // 
            this.cmbPIPE_DIA1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPIPE_DIA1.FormattingEnabled = true;
            this.cmbPIPE_DIA1.Items.AddRange(new object[] {
            "in m",
            "in mm"});
            this.cmbPIPE_DIA1.Location = new System.Drawing.Point(459, 125);
            this.cmbPIPE_DIA1.Name = "cmbPIPE_DIA1";
            this.cmbPIPE_DIA1.Size = new System.Drawing.Size(61, 21);
            this.cmbPIPE_DIA1.TabIndex = 157;
            // 
            // txtPIPE_DIA1
            // 
            this.txtPIPE_DIA1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPIPE_DIA1.Location = new System.Drawing.Point(258, 125);
            this.txtPIPE_DIA1.MaxLength = 20;
            this.txtPIPE_DIA1.Name = "txtPIPE_DIA1";
            this.txtPIPE_DIA1.Size = new System.Drawing.Size(193, 20);
            this.txtPIPE_DIA1.TabIndex = 155;
            // 
            // lblPIPE_DIA1
            // 
            this.lblPIPE_DIA1.AutoSize = true;
            this.lblPIPE_DIA1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPIPE_DIA1.Location = new System.Drawing.Point(30, 125);
            this.lblPIPE_DIA1.Name = "lblPIPE_DIA1";
            this.lblPIPE_DIA1.Size = new System.Drawing.Size(222, 15);
            this.lblPIPE_DIA1.TabIndex = 156;
            this.lblPIPE_DIA1.Text = "Upstream (Smaller) Pipe Diameter D1 :";
            // 
            // cmbLENGTH_PIPE
            // 
            this.cmbLENGTH_PIPE.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLENGTH_PIPE.FormattingEnabled = true;
            this.cmbLENGTH_PIPE.Items.AddRange(new object[] {
            "in m",
            "in mm"});
            this.cmbLENGTH_PIPE.Location = new System.Drawing.Point(921, 70);
            this.cmbLENGTH_PIPE.Name = "cmbLENGTH_PIPE";
            this.cmbLENGTH_PIPE.Size = new System.Drawing.Size(61, 21);
            this.cmbLENGTH_PIPE.TabIndex = 154;
            // 
            // cmbRoughMes
            // 
            this.cmbRoughMes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbRoughMes.FormattingEnabled = true;
            this.cmbRoughMes.Items.AddRange(new object[] {
            "in m",
            "in mm"});
            this.cmbRoughMes.Location = new System.Drawing.Point(459, 67);
            this.cmbRoughMes.Name = "cmbRoughMes";
            this.cmbRoughMes.Size = new System.Drawing.Size(61, 21);
            this.cmbRoughMes.TabIndex = 153;
            // 
            // txtTHETA
            // 
            this.txtTHETA.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTHETA.Location = new System.Drawing.Point(652, 183);
            this.txtTHETA.MaxLength = 20;
            this.txtTHETA.Name = "txtTHETA";
            this.txtTHETA.Size = new System.Drawing.Size(193, 20);
            this.txtTHETA.TabIndex = 151;
            // 
            // lblTHETA
            // 
            this.lblTHETA.AutoSize = true;
            this.lblTHETA.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTHETA.Location = new System.Drawing.Point(392, 183);
            this.lblTHETA.Name = "lblTHETA";
            this.lblTHETA.Size = new System.Drawing.Size(254, 15);
            this.lblTHETA.TabIndex = 152;
            this.lblTHETA.Text = "Enter the value of Angle of Bend W in Degree:";
            // 
            // txtRbyD
            // 
            this.txtRbyD.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtRbyD.Location = new System.Drawing.Point(163, 182);
            this.txtRbyD.MaxLength = 20;
            this.txtRbyD.Name = "txtRbyD";
            this.txtRbyD.Size = new System.Drawing.Size(193, 20);
            this.txtRbyD.TabIndex = 149;
            // 
            // lblRbyD
            // 
            this.lblRbyD.AutoSize = true;
            this.lblRbyD.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRbyD.Location = new System.Drawing.Point(15, 183);
            this.lblRbyD.Name = "lblRbyD";
            this.lblRbyD.Size = new System.Drawing.Size(149, 15);
            this.lblRbyD.TabIndex = 150;
            this.lblRbyD.Text = "Enter the value of R by D : ";
            // 
            // txtLENGTH_PIPE
            // 
            this.txtLENGTH_PIPE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLENGTH_PIPE.Location = new System.Drawing.Point(751, 70);
            this.txtLENGTH_PIPE.MaxLength = 20;
            this.txtLENGTH_PIPE.Name = "txtLENGTH_PIPE";
            this.txtLENGTH_PIPE.Size = new System.Drawing.Size(164, 20);
            this.txtLENGTH_PIPE.TabIndex = 147;
            // 
            // lblLENGTH_PIPE
            // 
            this.lblLENGTH_PIPE.AutoSize = true;
            this.lblLENGTH_PIPE.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLENGTH_PIPE.Location = new System.Drawing.Point(556, 70);
            this.lblLENGTH_PIPE.Name = "lblLENGTH_PIPE";
            this.lblLENGTH_PIPE.Size = new System.Drawing.Size(189, 15);
            this.lblLENGTH_PIPE.TabIndex = 148;
            this.lblLENGTH_PIPE.Text = "Enter the value of Length of Pipe :";
            // 
            // cmbPST0Mes
            // 
            this.cmbPST0Mes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPST0Mes.FormattingEnabled = true;
            this.cmbPST0Mes.Items.AddRange(new object[] {
            "in kPa",
            "in bar",
            "in kgf/cm2"});
            this.cmbPST0Mes.Location = new System.Drawing.Point(459, 96);
            this.cmbPST0Mes.Name = "cmbPST0Mes";
            this.cmbPST0Mes.Size = new System.Drawing.Size(61, 21);
            this.cmbPST0Mes.TabIndex = 140;
            // 
            // cmbPipeDiaMes
            // 
            this.cmbPipeDiaMes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPipeDiaMes.FormattingEnabled = true;
            this.cmbPipeDiaMes.Items.AddRange(new object[] {
            "in m",
            "in mm"});
            this.cmbPipeDiaMes.Location = new System.Drawing.Point(459, 40);
            this.cmbPipeDiaMes.Name = "cmbPipeDiaMes";
            this.cmbPipeDiaMes.Size = new System.Drawing.Size(61, 21);
            this.cmbPipeDiaMes.TabIndex = 133;
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.GhostWhite;
            this.panel13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel13.Controls.Add(this.label11);
            this.panel13.Location = new System.Drawing.Point(2, 3);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(982, 30);
            this.panel13.TabIndex = 132;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Brown;
            this.label11.Location = new System.Drawing.Point(7, 6);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(121, 18);
            this.label11.TabIndex = 0;
            this.label11.Text = "Optional Inputs";
            // 
            // txtPST0
            // 
            this.txtPST0.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPST0.Location = new System.Drawing.Point(258, 97);
            this.txtPST0.MaxLength = 20;
            this.txtPST0.Name = "txtPST0";
            this.txtPST0.Size = new System.Drawing.Size(193, 20);
            this.txtPST0.TabIndex = 10;
            // 
            // lblPST0
            // 
            this.lblPST0.AutoSize = true;
            this.lblPST0.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPST0.Location = new System.Drawing.Point(20, 97);
            this.lblPST0.Name = "lblPST0";
            this.lblPST0.Size = new System.Drawing.Size(235, 15);
            this.lblPST0.TabIndex = 131;
            this.lblPST0.Text = "Enter the value of Downstream Pressure : ";
            // 
            // txtROUGHNESS
            // 
            this.txtROUGHNESS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtROUGHNESS.Location = new System.Drawing.Point(258, 70);
            this.txtROUGHNESS.MaxLength = 20;
            this.txtROUGHNESS.Name = "txtROUGHNESS";
            this.txtROUGHNESS.Size = new System.Drawing.Size(193, 20);
            this.txtROUGHNESS.TabIndex = 9;
            // 
            // lblROUGHNESS
            // 
            this.lblROUGHNESS.AutoSize = true;
            this.lblROUGHNESS.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblROUGHNESS.Location = new System.Drawing.Point(79, 70);
            this.lblROUGHNESS.Name = "lblROUGHNESS";
            this.lblROUGHNESS.Size = new System.Drawing.Size(176, 15);
            this.lblROUGHNESS.TabIndex = 129;
            this.lblROUGHNESS.Text = "Enter the value of Roughness : ";
            // 
            // txtFITTING_QUANTITY
            // 
            this.txtFITTING_QUANTITY.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFITTING_QUANTITY.Location = new System.Drawing.Point(751, 40);
            this.txtFITTING_QUANTITY.MaxLength = 20;
            this.txtFITTING_QUANTITY.Name = "txtFITTING_QUANTITY";
            this.txtFITTING_QUANTITY.Size = new System.Drawing.Size(164, 20);
            this.txtFITTING_QUANTITY.TabIndex = 8;
            // 
            // lblFITTING_QUANTITY
            // 
            this.lblFITTING_QUANTITY.AutoSize = true;
            this.lblFITTING_QUANTITY.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFITTING_QUANTITY.Location = new System.Drawing.Point(636, 45);
            this.lblFITTING_QUANTITY.Name = "lblFITTING_QUANTITY";
            this.lblFITTING_QUANTITY.Size = new System.Drawing.Size(109, 15);
            this.lblFITTING_QUANTITY.TabIndex = 127;
            this.lblFITTING_QUANTITY.Text = "Enter the Quantity :";
            // 
            // txtPIPE_DIA
            // 
            this.txtPIPE_DIA.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPIPE_DIA.Location = new System.Drawing.Point(258, 40);
            this.txtPIPE_DIA.MaxLength = 20;
            this.txtPIPE_DIA.Name = "txtPIPE_DIA";
            this.txtPIPE_DIA.Size = new System.Drawing.Size(193, 20);
            this.txtPIPE_DIA.TabIndex = 7;
            // 
            // lblPIPE_DIA
            // 
            this.lblPIPE_DIA.AutoSize = true;
            this.lblPIPE_DIA.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPIPE_DIA.Location = new System.Drawing.Point(63, 40);
            this.lblPIPE_DIA.Name = "lblPIPE_DIA";
            this.lblPIPE_DIA.Size = new System.Drawing.Size(192, 15);
            this.lblPIPE_DIA.TabIndex = 125;
            this.lblPIPE_DIA.Text = "Enter the value of Pipe Diameter : ";
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.GhostWhite;
            this.panel14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel14.Controls.Add(this.panel11);
            this.panel14.Controls.Add(this.FittingPic);
            this.panel14.Location = new System.Drawing.Point(1001, 4);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(240, 567);
            this.panel14.TabIndex = 2001;
            // 
            // FittingPic
            // 
            this.FittingPic.Image = global::Nirma.Properties.Resources._6;
            this.FittingPic.Location = new System.Drawing.Point(3, 3);
            this.FittingPic.Name = "FittingPic";
            this.FittingPic.Size = new System.Drawing.Size(232, 249);
            this.FittingPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.FittingPic.TabIndex = 0;
            this.FittingPic.TabStop = false;
            // 
            // dgvGridFinalOutPut
            // 
            this.dgvGridFinalOutPut.AllowUserToAddRows = false;
            this.dgvGridFinalOutPut.AllowUserToDeleteRows = false;
            this.dgvGridFinalOutPut.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvGridFinalOutPut.Location = new System.Drawing.Point(1247, 4);
            this.dgvGridFinalOutPut.Name = "dgvGridFinalOutPut";
            this.dgvGridFinalOutPut.Size = new System.Drawing.Size(274, 567);
            this.dgvGridFinalOutPut.TabIndex = 2002;
            // 
            // btnFinalOP
            // 
            this.btnFinalOP.Location = new System.Drawing.Point(98, 2);
            this.btnFinalOP.Name = "btnFinalOP";
            this.btnFinalOP.Size = new System.Drawing.Size(135, 23);
            this.btnFinalOP.TabIndex = 2005;
            this.btnFinalOP.Text = "ShowFinalOutput";
            this.btnFinalOP.UseVisualStyleBackColor = true;
            this.btnFinalOP.Click += new System.EventHandler(this.btnFinalOP_Click);
            // 
            // Calculate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1242, 577);
            this.ControlBox = false;
            this.Controls.Add(this.dgvGridFinalOutPut);
            this.Controls.Add(this.panel14);
            this.Controls.Add(this.panel12);
            this.Controls.Add(this.panel9);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel3);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Calculate";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Calculate";
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMolGrid)).EndInit();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.panel14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.FittingPic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGridFinalOutPut)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ComboBox cmbType;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbFittings;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lblSubcode;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label LBLoP;
        private System.Windows.Forms.TextBox txtMASS_FLOW_RATE;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox txtVISC;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtMOLEQ_WEIGHT;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtTEMP;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnCalculate;
        private System.Windows.Forms.Button btnNewCalculation;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtPST0;
        private System.Windows.Forms.Label lblPST0;
        private System.Windows.Forms.TextBox txtROUGHNESS;
        private System.Windows.Forms.Label lblROUGHNESS;
        private System.Windows.Forms.TextBox txtFITTING_QUANTITY;
        private System.Windows.Forms.Label lblFITTING_QUANTITY;
        private System.Windows.Forms.TextBox txtPIPE_DIA;
        private System.Windows.Forms.Label lblPIPE_DIA;
        private System.Windows.Forms.Button btnInsertNewForm;
        private System.Windows.Forms.ComboBox cmbMassFlMes;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cmbPipeDiaMes;
        private System.Windows.Forms.ComboBox cmbPST0Mes;
        private System.Windows.Forms.Label lblFittingLabel;
        private System.Windows.Forms.TextBox txtGAMMA;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtLENGTH_PIPE;
        private System.Windows.Forms.Label lblLENGTH_PIPE;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.PictureBox FittingPic;
        private System.Windows.Forms.TextBox txtRbyD;
        private System.Windows.Forms.Label lblRbyD;
        private System.Windows.Forms.TextBox txtTHETA;
        private System.Windows.Forms.Label lblTHETA;
        private System.Windows.Forms.ComboBox cmbRoughMes;
        private System.Windows.Forms.ComboBox cmbLENGTH_PIPE;
        private System.Windows.Forms.ComboBox cmbPIPE_DIA1;
        private System.Windows.Forms.TextBox txtPIPE_DIA1;
        private System.Windows.Forms.Label lblPIPE_DIA1;
        private System.Windows.Forms.ComboBox cmbMAJOR_AXIS;
        private System.Windows.Forms.TextBox txtMAJOR_AXIS;
        private System.Windows.Forms.Label lblMAJOR_AXIS;
        private System.Windows.Forms.ComboBox cmbMINOR_AXIS;
        private System.Windows.Forms.TextBox txtMINOR_AXIS;
        private System.Windows.Forms.Label lblMINOR_AXIS;
        private System.Windows.Forms.ComboBox cmbLENGTH_PIPE1;
        private System.Windows.Forms.TextBox txtLENGTH_PIPE1;
        private System.Windows.Forms.Label lblLENGTH_PIPE1;
        private System.Windows.Forms.ComboBox cmbSEAL_DIA;
        private System.Windows.Forms.TextBox txtSEAL_DIA;
        private System.Windows.Forms.Label lblSEAL_DIA;
        private System.Windows.Forms.ComboBox cmbBUCKET_DIA;
        private System.Windows.Forms.TextBox txtBUCKET_DIA;
        private System.Windows.Forms.Label lblBUCKET_DIA;
        private System.Windows.Forms.DataGridView dgvMolGrid;
        private System.Windows.Forms.DataGridView dgvGridFinalOutPut;
        private System.Windows.Forms.Button btnFinalOP;
    }
}