﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nirma.Properties;

using Excel = Microsoft.Office.Interop.Excel;

namespace Nirma
{
    public partial class Calculate : Form
    {
        double PI = 3.141592654;
        DataTable dttemp = new DataTable();
        bool isNewMeasure = true;

        public Calculate()
        {
            InitializeComponent();
            ResetLabels();
            dttemp.Columns.Add("Feetings");
            dttemp.Columns.Add("Type");
            dttemp.Columns.Add("PST1");
            dttemp.Columns.Add("DELP");
            //fillgrid();
            label8.Text = this.Text;
        }
         

        private void ShowOutPut(double PST1, double DELP) {
            LBLoP.Text = "Output : " + Environment.NewLine + "Total PST  : " + Convert.ToString(Math.Round(PST1, 3)) +
                        Environment.NewLine + "Total DELP : " + Convert.ToString(Math.Round(DELP, 3));

            string CurrentIndex = this.Text.Replace("Fitting ", "");
            string Fittings = Convert.ToString(cmbFittings.SelectedItem);
            string Type = Convert.ToString(cmbType.Text);

            CommonClass.objFitting[Convert.ToInt32(CurrentIndex) - 1] = Fittings;
            CommonClass.objType[Convert.ToInt32(CurrentIndex) - 1] = Type;
            CommonClass.objPST0[Convert.ToInt32(CurrentIndex) - 1] = Convert.ToString(Math.Round(PST1, 3));
            CommonClass.objDELP[Convert.ToInt32(CurrentIndex) - 1] = Convert.ToString(Math.Round(DELP, 3));

            try
            {
                Form Calculatefrm = new Calculate();
                Calculatefrm = (Form)(CommonClass.obj[Convert.ToInt32(CurrentIndex)]);
                Calculatefrm.Controls["panel12"].Controls["txtPST0"].Text = Convert.ToString(Math.Round(PST1, 3));
            }
            catch (Exception) { }
        }

        private void btnCalculate_Click(object sender, EventArgs e)
        {
            try
            {
                CalculateFunc();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Please Validate all the input values.");
                MessageBox.Show(ex.Message);
            }
        }

        private void ResetLabels()
        {
            dgvMolGrid.DataSource = null;

            cmbType.Text = "";
            FittingPic.Image = null;
            lblPIPE_DIA.Text = "Enter the value of Pipe Diameter : ";
            lblROUGHNESS.Text = "Enter the value of Roughness : ";
            lblPST0.Text = "Enter the value of Downstream Pressure : ";
            lblPIPE_DIA1.Text = "Upstream(Smaller) Pipe Diameter D1 :";

            lblFITTING_QUANTITY.Text = "Enter the Quantity :";
            lblLENGTH_PIPE.Text = "Enter the value of Length of Pipe :";            
            lblLENGTH_PIPE1.Text = "Enter the value of Pipe Length L :";

            lblBUCKET_DIA.Text = "Value of Inlet Pipe Diameter (dpipe) :";
            lblSEAL_DIA.Text = "Value of Water Seal Diameter (dseal) :";
             

            txtPIPE_DIA.Enabled = false;
            txtROUGHNESS.Enabled = false;
            txtPST0.Enabled = true;
            txtPIPE_DIA1.Enabled = false;
            txtRbyD.Enabled = false;
            txtMAJOR_AXIS.Enabled = false;
            txtFITTING_QUANTITY.Enabled = false;
            txtLENGTH_PIPE.Enabled = false;
            txtLENGTH_PIPE1.Enabled = false;
            txtSEAL_DIA.Enabled = false;
            txtBUCKET_DIA.Enabled = false;
            txtTHETA.Enabled = false;
            txtMINOR_AXIS.Enabled = false;
        }

        private void CalculateFunc()
        {
            string Fittings = Convert.ToString(cmbFittings.SelectedItem);

            if (Fittings.Trim() == "Orifice")
            {
                Orifice();
            }
            else if (Fittings.Trim() == "Flare Tip")
            {
                Flare_Tip();
            }
            else if (Fittings.Trim() == "Pipe Exit")
            {
                calculatePipeExit();
            }
            else if (Fittings.Trim() == "Velocity Seal")
            {
                Velocity_Seal();
            }
            else if (Fittings.Trim() == "Molecular Seal")
            {
                Molecular_Seal();
            }
            else if (Fittings.Trim() == "Straight Pipe")
            {
                Circular_Pipe_Pressure_Drop();
            }
            else if (Fittings.Trim() == "Water Seal Drum")
            {
                Water_Seal_Drum();
            }
            else if (Fittings.Trim() == "Sudden Contaction")
            {
                Sudden_Contaction();
            }
            else if (Fittings.Trim() == "Sudden Expansion")
            {
                Sudden_Expansion();
            }
            else if (Fittings.Trim() == "Gradual Contraction")
            {
                Gradual_Contaction();
            }
            else if (Fittings.Trim() == "Gradual Expansion")
            {
                Gradual_Expansion();
            }
            else if (Fittings.Trim() == "Elbow")
            {
                Elbow();
            }
            else if (Fittings.Trim() == "Mitre Bend")
            {
                Mitre_Bend();
            }
            else if (Fittings.Trim() == "Annular Section")
            {
                Annular_Section();
            }
            else if (Fittings.Trim() == "Rectangular Duct")
            {
                Rectangular_Duct();
            }
            else if (Fittings.Trim() == "Rectangular Elbow")
            {
                Rectangular_Elbow();
            }
            else if (Fittings.Trim() == "T_Thru")
            {
                T_Thru();
            }
            else if (Fittings.Trim() == "T Branch to Run Flow")
            {
                T_Branch_to_Run_Flow();
            }
            else if (Fittings.Trim() == "T Run to Branch Flow")
            {
                T_Run_to_Branch_Flow();
            }
            else if (Fittings.Trim() == "Gate valve")
            {
                Gate_Valve();
            }
            else if (Fittings.Trim() == "Ball Valve")
            {
                Ball_Valve();
            }
            else if (Fittings.Trim() == "Globe valve")
            {
                Globe_Valve();
            }
            else if (Fittings.Trim() == "Butterfly Valve")
            {
                Butterfly_Valve();
            }
            else if (Fittings.Trim() == "Check valve")
            {
                Check_Valve();
            }
            else if (Fittings.Trim() == "Angle valve")
            {
                Angle_Valve();
            }
            else if (Fittings.Trim() == "Coupling")
            {
                Coupling();
            }
            else if (Fittings.Trim() == "Diaphram valve")
            {
                Diaphram_Valve();
            }
            else if (Fittings.Trim() == "Foot valve")
            {
                Foot_Valve();
            }
            else if (Fittings.Trim() == "Plug Cock")
            {
                Plug_Cock();
            }
            else if (Fittings.Trim() == "Union")
            {
                Union();
            }
        }

        private void cmbType_SelectedIndexChanged(object sender, EventArgs e)
        {
            string Fittings = Convert.ToString(cmbType.Text);
            if (Fittings.Trim() == "Flare Tip Contant Bore")
            {
                lblPIPE_DIA1.Text = "Enter the value of Pipe Diameter D :";
                lblLENGTH_PIPE.Text = "value of Length of Flare Tip L :";
                FittingPic.Image = Resources.constant_bore;
                //LoadImage("constant bore.JPG");
                lblPIPE_DIA1.Enabled = false;
            }
            else if (Fittings.Trim() == "Flare Tip Standard Bore")
            {
                lblPIPE_DIA1.Enabled = true;
                lblPIPE_DIA.Text = "value of Refractory Diameter dr : ";
                lblPIPE_DIA1.Text = "Enter the value of Pipe Diameter D :";
                lblLENGTH_PIPE.Text = "The value of Refractory Length Lr :";
                FittingPic.Image = Resources.Standard_Bore;
                //LoadImage("Standard Bore.JPG");
            }
            else if (Fittings.Trim() == "Water Seal Drum Above 10 Inch")
            {
                FittingPic.Image = Resources.Above_10_Inch;
                //LoadImage("Above_10_Inch.jpg");
            }
            else if (Fittings.Trim() == "Water Seal Drum Upto 10 Inch")
            {
                FittingPic.Image = Resources.Upto_10_Inch;
                //LoadImage("Upto_10_Inch.jpg");
            }
        }

        private void LoadMolGrid() {
            //System.Data.OleDb.OleDbConnection MyConnection;
            //System.Data.DataSet DtSet;
            //System.Data.OleDb.OleDbDataAdapter MyCommand;
            //MyConnection = new System.Data.OleDb.OleDbConnection("provider=Microsoft.Jet.OLEDB.4.0;Data Source='c:\\Seal Size Input File.xls';Extended Properties=Excel 8.0;");
            //MyCommand = new System.Data.OleDb.OleDbDataAdapter("select * from [Sheet1$]", MyConnection);
            //MyCommand.TableMappings.Add("Table", "TestTable");
            //DtSet = new System.Data.DataSet();
            //MyCommand.Fill(DtSet);
            //MyConnection.Close();

            StringReader theReader = new StringReader("<NewDataSet>" +
                                                      "<TestTable>" +
                                                    "    <Seal_x0020_dia_x0020_inch>3</Seal_x0020_dia_x0020_inch>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D1>0.2191</Dimensions_x0020__x0028_m_x0029_D1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D2>0.1683</Dimensions_x0020__x0028_m_x0029_D2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D3>0.0889</Dimensions_x0020__x0028_m_x0029_D3>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L1>0.1</Dimensions_x0020__x0028_m_x0029_L1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L2>0.15</Dimensions_x0020__x0028_m_x0029_L2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L3>0.08</Dimensions_x0020__x0028_m_x0029_L3>" +
                                                    "  </TestTable>" +
                                                    "  <TestTable>" +
                                                    "    <Seal_x0020_dia_x0020_inch>4</Seal_x0020_dia_x0020_inch>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D1>0.273</Dimensions_x0020__x0028_m_x0029_D1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D2>0.2191</Dimensions_x0020__x0028_m_x0029_D2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D3>0.1143</Dimensions_x0020__x0028_m_x0029_D3>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L1>0.11</Dimensions_x0020__x0028_m_x0029_L1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L2>0.15</Dimensions_x0020__x0028_m_x0029_L2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L3>0.085</Dimensions_x0020__x0028_m_x0029_L3>" +
                                                    "  </TestTable>" +
                                                    "  <TestTable>" +
                                                    "    <Seal_x0020_dia_x0020_inch>6</Seal_x0020_dia_x0020_inch>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D1>0.3556</Dimensions_x0020__x0028_m_x0029_D1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D2>0.273</Dimensions_x0020__x0028_m_x0029_D2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D3>0.1683</Dimensions_x0020__x0028_m_x0029_D3>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L1>0.14</Dimensions_x0020__x0028_m_x0029_L1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L2>0.25</Dimensions_x0020__x0028_m_x0029_L2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L3>0.085</Dimensions_x0020__x0028_m_x0029_L3>" +
                                                    "  </TestTable>" +
                                                    "  <TestTable>" +
                                                    "    <Seal_x0020_dia_x0020_inch>8</Seal_x0020_dia_x0020_inch>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D1>0.457</Dimensions_x0020__x0028_m_x0029_D1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D2>0.3558</Dimensions_x0020__x0028_m_x0029_D2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D3>0.2191</Dimensions_x0020__x0028_m_x0029_D3>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L1>0.155</Dimensions_x0020__x0028_m_x0029_L1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L2>0.25</Dimensions_x0020__x0028_m_x0029_L2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L3>0.095</Dimensions_x0020__x0028_m_x0029_L3>" +
                                                    "  </TestTable>" +
                                                    "  <TestTable>" +
                                                    "    <Seal_x0020_dia_x0020_inch>10</Seal_x0020_dia_x0020_inch>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D1>0.6096</Dimensions_x0020__x0028_m_x0029_D1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D2>0.4576</Dimensions_x0020__x0028_m_x0029_D2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D3>0.273</Dimensions_x0020__x0028_m_x0029_D3>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L1>0.165</Dimensions_x0020__x0028_m_x0029_L1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L2>0.25</Dimensions_x0020__x0028_m_x0029_L2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L3>0.16</Dimensions_x0020__x0028_m_x0029_L3>" +
                                                    "  </TestTable>" +
                                                    "  <TestTable>" +
                                                    "    <Seal_x0020_dia_x0020_inch>12</Seal_x0020_dia_x0020_inch>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D1>0.712</Dimensions_x0020__x0028_m_x0029_D1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D2>0.5588</Dimensions_x0020__x0028_m_x0029_D2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D3>0.32380000000000003</Dimensions_x0020__x0028_m_x0029_D3>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L1>0.205</Dimensions_x0020__x0028_m_x0029_L1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L2>0.25</Dimensions_x0020__x0028_m_x0029_L2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L3>0.18</Dimensions_x0020__x0028_m_x0029_L3>" +
                                                    "  </TestTable>" +
                                                    "  <TestTable>" +
                                                    "    <Seal_x0020_dia_x0020_inch>14</Seal_x0020_dia_x0020_inch>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D1>0.812</Dimensions_x0020__x0028_m_x0029_D1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D2>0.6096</Dimensions_x0020__x0028_m_x0029_D2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D3>0.3556</Dimensions_x0020__x0028_m_x0029_D3>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L1>0.23</Dimensions_x0020__x0028_m_x0029_L1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L2>0.25</Dimensions_x0020__x0028_m_x0029_L2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L3>0.2</Dimensions_x0020__x0028_m_x0029_L3>" +
                                                    "  </TestTable>" +
                                                    "  <TestTable>" +
                                                    "    <Seal_x0020_dia_x0020_inch>16</Seal_x0020_dia_x0020_inch>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D1>0.9144</Dimensions_x0020__x0028_m_x0029_D1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D2>0.7112</Dimensions_x0020__x0028_m_x0029_D2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D3>0.4064</Dimensions_x0020__x0028_m_x0029_D3>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L1>0.28</Dimensions_x0020__x0028_m_x0029_L1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L2>0.25</Dimensions_x0020__x0028_m_x0029_L2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L3>0.235</Dimensions_x0020__x0028_m_x0029_L3>" +
                                                    "  </TestTable>" +
                                                    "  <TestTable>" +
                                                    "    <Seal_x0020_dia_x0020_inch>18</Seal_x0020_dia_x0020_inch>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D1>1.042</Dimensions_x0020__x0028_m_x0029_D1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D2>0.8128</Dimensions_x0020__x0028_m_x0029_D2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D3>0.4576</Dimensions_x0020__x0028_m_x0029_D3>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L1>0.305</Dimensions_x0020__x0028_m_x0029_L1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L2>0.25</Dimensions_x0020__x0028_m_x0029_L2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L3>0.285</Dimensions_x0020__x0028_m_x0029_L3>" +
                                                    "  </TestTable>" +
                                                    "  <TestTable>" +
                                                    "    <Seal_x0020_dia_x0020_inch>20</Seal_x0020_dia_x0020_inch>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D1>1.14</Dimensions_x0020__x0028_m_x0029_D1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D2>0.8636</Dimensions_x0020__x0028_m_x0029_D2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D3>0.508</Dimensions_x0020__x0028_m_x0029_D3>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L1>0.33</Dimensions_x0020__x0028_m_x0029_L1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L2>0.25</Dimensions_x0020__x0028_m_x0029_L2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L3>0.31</Dimensions_x0020__x0028_m_x0029_L3>" +
                                                    "  </TestTable>" +
                                                    "  <TestTable>" +
                                                    "    <Seal_x0020_dia_x0020_inch>22</Seal_x0020_dia_x0020_inch>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D1>1.255</Dimensions_x0020__x0028_m_x0029_D1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D2>0.95</Dimensions_x0020__x0028_m_x0029_D2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D3>0.559</Dimensions_x0020__x0028_m_x0029_D3>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L1>0.37</Dimensions_x0020__x0028_m_x0029_L1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L2>0.275</Dimensions_x0020__x0028_m_x0029_L2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L3>0.34</Dimensions_x0020__x0028_m_x0029_L3>" +
                                                    "  </TestTable>" +
                                                    "  <TestTable>" +
                                                    "    <Seal_x0020_dia_x0020_inch>24</Seal_x0020_dia_x0020_inch>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D1>1.372</Dimensions_x0020__x0028_m_x0029_D1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D2>1.0668</Dimensions_x0020__x0028_m_x0029_D2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D3>0.6096</Dimensions_x0020__x0028_m_x0029_D3>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L1>0.41</Dimensions_x0020__x0028_m_x0029_L1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L2>0.305</Dimensions_x0020__x0028_m_x0029_L2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L3>0.37</Dimensions_x0020__x0028_m_x0029_L3>" +
                                                    "  </TestTable>" +
                                                    "  <TestTable>" +
                                                    "    <Seal_x0020_dia_x0020_inch>26</Seal_x0020_dia_x0020_inch>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D1>1.43</Dimensions_x0020__x0028_m_x0029_D1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D2>1.12</Dimensions_x0020__x0028_m_x0029_D2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D3>0.6604</Dimensions_x0020__x0028_m_x0029_D3>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L1>0.41</Dimensions_x0020__x0028_m_x0029_L1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L2>0.305</Dimensions_x0020__x0028_m_x0029_L2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L3>0.4</Dimensions_x0020__x0028_m_x0029_L3>" +
                                                    "  </TestTable>" +
                                                    "  <TestTable>" +
                                                    "    <Seal_x0020_dia_x0020_inch>28</Seal_x0020_dia_x0020_inch>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D1>1.6</Dimensions_x0020__x0028_m_x0029_D1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D2>1.245</Dimensions_x0020__x0028_m_x0029_D2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D3>0.7112</Dimensions_x0020__x0028_m_x0029_D3>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L1>0.46</Dimensions_x0020__x0028_m_x0029_L1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L2>0.305</Dimensions_x0020__x0028_m_x0029_L2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L3>0.41</Dimensions_x0020__x0028_m_x0029_L3>" +
                                                    "  </TestTable>" +
                                                    "  <TestTable>" +
                                                    "    <Seal_x0020_dia_x0020_inch>30</Seal_x0020_dia_x0020_inch>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D1>1.702</Dimensions_x0020__x0028_m_x0029_D1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D2>1.32</Dimensions_x0020__x0028_m_x0029_D2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D3>0.762</Dimensions_x0020__x0028_m_x0029_D3>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L1>0.51</Dimensions_x0020__x0028_m_x0029_L1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L2>0.305</Dimensions_x0020__x0028_m_x0029_L2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L3>0.455</Dimensions_x0020__x0028_m_x0029_L3>" +
                                                    "  </TestTable>" +
                                                    "  <TestTable>" +
                                                    "    <Seal_x0020_dia_x0020_inch>32</Seal_x0020_dia_x0020_inch>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D1>1.85</Dimensions_x0020__x0028_m_x0029_D1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D2>1.4</Dimensions_x0020__x0028_m_x0029_D2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D3>0.813</Dimensions_x0020__x0028_m_x0029_D3>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L1>0.55</Dimensions_x0020__x0028_m_x0029_L1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L2>0.305</Dimensions_x0020__x0028_m_x0029_L2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L3>0.485</Dimensions_x0020__x0028_m_x0029_L3>" +
                                                    "  </TestTable>" +
                                                    "  <TestTable>" +
                                                    "    <Seal_x0020_dia_x0020_inch>34</Seal_x0020_dia_x0020_inch>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D1>1.91</Dimensions_x0020__x0028_m_x0029_D1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D2>1.455</Dimensions_x0020__x0028_m_x0029_D2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D3>0.85</Dimensions_x0020__x0028_m_x0029_D3>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L1>0.58</Dimensions_x0020__x0028_m_x0029_L1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L2>0.295</Dimensions_x0020__x0028_m_x0029_L2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L3>0.52</Dimensions_x0020__x0028_m_x0029_L3>" +
                                                    "  </TestTable>" +
                                                    "  <TestTable>" +
                                                    "    <Seal_x0020_dia_x0020_inch>36</Seal_x0020_dia_x0020_inch>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D1>2.032</Dimensions_x0020__x0028_m_x0029_D1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D2>1.574</Dimensions_x0020__x0028_m_x0029_D2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D3>0.9144</Dimensions_x0020__x0028_m_x0029_D3>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L1>0.61</Dimensions_x0020__x0028_m_x0029_L1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L2>0.305</Dimensions_x0020__x0028_m_x0029_L2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L3>0.56</Dimensions_x0020__x0028_m_x0029_L3>" +
                                                    "  </TestTable>" +
                                                    "  <TestTable>" +
                                                    "    <Seal_x0020_dia_x0020_inch>40</Seal_x0020_dia_x0020_inch>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D1>2.26</Dimensions_x0020__x0028_m_x0029_D1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D2>1.75</Dimensions_x0020__x0028_m_x0029_D2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D3>1.016</Dimensions_x0020__x0028_m_x0029_D3>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L1>0.68</Dimensions_x0020__x0028_m_x0029_L1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L2>0.35</Dimensions_x0020__x0028_m_x0029_L2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L3>0.6</Dimensions_x0020__x0028_m_x0029_L3>" +
                                                    "  </TestTable>" +
                                                    "  <TestTable>" +
                                                    "    <Seal_x0020_dia_x0020_inch>42</Seal_x0020_dia_x0020_inch>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D1>2.362</Dimensions_x0020__x0028_m_x0029_D1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D2>1.828</Dimensions_x0020__x0028_m_x0029_D2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D3>1.066</Dimensions_x0020__x0028_m_x0029_D3>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L1>0.71</Dimensions_x0020__x0028_m_x0029_L1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L2>0.38</Dimensions_x0020__x0028_m_x0029_L2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L3>0.615</Dimensions_x0020__x0028_m_x0029_L3>" +
                                                    "  </TestTable>" +
                                                    "  <TestTable>" +
                                                    "    <Seal_x0020_dia_x0020_inch>44</Seal_x0020_dia_x0020_inch>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D1>2.475</Dimensions_x0020__x0028_m_x0029_D1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D2>1.92</Dimensions_x0020__x0028_m_x0029_D2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D3>1.117</Dimensions_x0020__x0028_m_x0029_D3>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L1>0.8</Dimensions_x0020__x0028_m_x0029_L1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L2>0.5</Dimensions_x0020__x0028_m_x0029_L2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L3>0.7</Dimensions_x0020__x0028_m_x0029_L3>" +
                                                    "  </TestTable>" +
                                                    "  <TestTable>" +
                                                    "    <Seal_x0020_dia_x0020_inch>48</Seal_x0020_dia_x0020_inch>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D1>2.725</Dimensions_x0020__x0028_m_x0029_D1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D2>2.111</Dimensions_x0020__x0028_m_x0029_D2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D3>1.219</Dimensions_x0020__x0028_m_x0029_D3>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L1>0.85</Dimensions_x0020__x0028_m_x0029_L1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L2>0.6</Dimensions_x0020__x0028_m_x0029_L2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L3>0.75</Dimensions_x0020__x0028_m_x0029_L3>" +
                                                    "  </TestTable>" +
                                                    "  <TestTable>" +
                                                    "    <Seal_x0020_dia_x0020_inch>50</Seal_x0020_dia_x0020_inch>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D1>2.84</Dimensions_x0020__x0028_m_x0029_D1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D2>2.2</Dimensions_x0020__x0028_m_x0029_D2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D3>1.27</Dimensions_x0020__x0028_m_x0029_D3>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L1>0.89</Dimensions_x0020__x0028_m_x0029_L1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L2>0.625</Dimensions_x0020__x0028_m_x0029_L2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L3>0.78</Dimensions_x0020__x0028_m_x0029_L3>" +
                                                    "  </TestTable>" +
                                                    "  <TestTable>" +
                                                    "    <Seal_x0020_dia_x0020_inch>52</Seal_x0020_dia_x0020_inch>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D1>2.955</Dimensions_x0020__x0028_m_x0029_D1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D2>2.29</Dimensions_x0020__x0028_m_x0029_D2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D3>1.321</Dimensions_x0020__x0028_m_x0029_D3>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L1>0.925</Dimensions_x0020__x0028_m_x0029_L1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L2>0.65</Dimensions_x0020__x0028_m_x0029_L2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L3>0.8</Dimensions_x0020__x0028_m_x0029_L3>" +
                                                    "  </TestTable>" +
                                                    "  <TestTable>" +
                                                    "    <Seal_x0020_dia_x0020_inch>54</Seal_x0020_dia_x0020_inch>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D1>3.048</Dimensions_x0020__x0028_m_x0029_D1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D2>2.355</Dimensions_x0020__x0028_m_x0029_D2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D3>1.371</Dimensions_x0020__x0028_m_x0029_D3>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L1>0.94</Dimensions_x0020__x0028_m_x0029_L1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L2>0.51</Dimensions_x0020__x0028_m_x0029_L2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L3>0.81</Dimensions_x0020__x0028_m_x0029_L3>" +
                                                    "  </TestTable>" +
                                                    "  <TestTable>" +
                                                    "    <Seal_x0020_dia_x0020_inch>56</Seal_x0020_dia_x0020_inch>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D1>3.162</Dimensions_x0020__x0028_m_x0029_D1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D2>2.445</Dimensions_x0020__x0028_m_x0029_D2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D3>1.422</Dimensions_x0020__x0028_m_x0029_D3>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L1>0.975</Dimensions_x0020__x0028_m_x0029_L1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L2>0.6</Dimensions_x0020__x0028_m_x0029_L2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L3>0.82</Dimensions_x0020__x0028_m_x0029_L3>" +
                                                    "  </TestTable>" +
                                                    "  <TestTable>" +
                                                    "    <Seal_x0020_dia_x0020_inch>58</Seal_x0020_dia_x0020_inch>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D1>3.275</Dimensions_x0020__x0028_m_x0029_D1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D2>2.55</Dimensions_x0020__x0028_m_x0029_D2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D3>1.473</Dimensions_x0020__x0028_m_x0029_D3>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L1>0.94</Dimensions_x0020__x0028_m_x0029_L1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L2>0.7</Dimensions_x0020__x0028_m_x0029_L2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L3>0.81</Dimensions_x0020__x0028_m_x0029_L3>" +
                                                    "  </TestTable>" +
                                                    "  <TestTable>" +
                                                    "    <Seal_x0020_dia_x0020_inch>60</Seal_x0020_dia_x0020_inch>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D1>3.5</Dimensions_x0020__x0028_m_x0029_D1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D2>2.755</Dimensions_x0020__x0028_m_x0029_D2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D3>1.524</Dimensions_x0020__x0028_m_x0029_D3>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L1>0.94</Dimensions_x0020__x0028_m_x0029_L1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L2>0.75</Dimensions_x0020__x0028_m_x0029_L2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L3>0.81</Dimensions_x0020__x0028_m_x0029_L3>" +
                                                    "  </TestTable>" +
                                                    "  <TestTable>" +
                                                    "    <Seal_x0020_dia_x0020_inch>62</Seal_x0020_dia_x0020_inch>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D1>3.62</Dimensions_x0020__x0028_m_x0029_D1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D2>2.85</Dimensions_x0020__x0028_m_x0029_D2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D3>1.575</Dimensions_x0020__x0028_m_x0029_D3>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L1>0.975</Dimensions_x0020__x0028_m_x0029_L1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L2>0.78</Dimensions_x0020__x0028_m_x0029_L2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L3>0.84</Dimensions_x0020__x0028_m_x0029_L3>" +
                                                    "  </TestTable>" +
                                                    "  <TestTable>" +
                                                    "    <Seal_x0020_dia_x0020_inch>64</Seal_x0020_dia_x0020_inch>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D1>3.7</Dimensions_x0020__x0028_m_x0029_D1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D2>2.95</Dimensions_x0020__x0028_m_x0029_D2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D3>1.614</Dimensions_x0020__x0028_m_x0029_D3>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L1>0.94</Dimensions_x0020__x0028_m_x0029_L1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L2>0.51</Dimensions_x0020__x0028_m_x0029_L2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L3>0.81</Dimensions_x0020__x0028_m_x0029_L3>" +
                                                    "  </TestTable>" +
                                                    "  <TestTable>" +
                                                    "    <Seal_x0020_dia_x0020_inch>66</Seal_x0020_dia_x0020_inch>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D1>3.85</Dimensions_x0020__x0028_m_x0029_D1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D2>3.1</Dimensions_x0020__x0028_m_x0029_D2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D3>1.664</Dimensions_x0020__x0028_m_x0029_D3>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L1>0.94</Dimensions_x0020__x0028_m_x0029_L1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L2>0.75</Dimensions_x0020__x0028_m_x0029_L2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L3>0.9</Dimensions_x0020__x0028_m_x0029_L3>" +
                                                    "  </TestTable>" +
                                                    "  <TestTable>" +
                                                    "    <Seal_x0020_dia_x0020_inch>68</Seal_x0020_dia_x0020_inch>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D1>3.89</Dimensions_x0020__x0028_m_x0029_D1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D2>3.15</Dimensions_x0020__x0028_m_x0029_D2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D3>1.727</Dimensions_x0020__x0028_m_x0029_D3>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L1>1.07</Dimensions_x0020__x0028_m_x0029_L1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L2>0.75</Dimensions_x0020__x0028_m_x0029_L2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L3>0.95</Dimensions_x0020__x0028_m_x0029_L3>" +
                                                    "  </TestTable>" +
                                                    "  <TestTable>" +
                                                    "    <Seal_x0020_dia_x0020_inch>72</Seal_x0020_dia_x0020_inch>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D1>4.1</Dimensions_x0020__x0028_m_x0029_D1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D2>3.2</Dimensions_x0020__x0028_m_x0029_D2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D3>1.828</Dimensions_x0020__x0028_m_x0029_D3>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L1>1.1</Dimensions_x0020__x0028_m_x0029_L1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L2>0.8</Dimensions_x0020__x0028_m_x0029_L2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L3>0.96</Dimensions_x0020__x0028_m_x0029_L3>" +
                                                    "  </TestTable>" +
                                                    "  <TestTable>" +
                                                    "    <Seal_x0020_dia_x0020_inch>76</Seal_x0020_dia_x0020_inch>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D1>4.3</Dimensions_x0020__x0028_m_x0029_D1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D2>3.34</Dimensions_x0020__x0028_m_x0029_D2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D3>1.93</Dimensions_x0020__x0028_m_x0029_D3>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L1>1.2</Dimensions_x0020__x0028_m_x0029_L1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L2>0.96</Dimensions_x0020__x0028_m_x0029_L2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L3>1.1</Dimensions_x0020__x0028_m_x0029_L3>" +
                                                    "  </TestTable>" +
                                                    "  <TestTable>" +
                                                    "    <Seal_x0020_dia_x0020_inch>80</Seal_x0020_dia_x0020_inch>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D1>4.53</Dimensions_x0020__x0028_m_x0029_D1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D2>3.52</Dimensions_x0020__x0028_m_x0029_D2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D3>2.032</Dimensions_x0020__x0028_m_x0029_D3>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L1>1.3</Dimensions_x0020__x0028_m_x0029_L1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L2>1.01</Dimensions_x0020__x0028_m_x0029_L2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L3>1.16</Dimensions_x0020__x0028_m_x0029_L3>" +
                                                    "  </TestTable>" +
                                                    "  <TestTable>" +
                                                    "    <Seal_x0020_dia_x0020_inch>84</Seal_x0020_dia_x0020_inch>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D1>4.76</Dimensions_x0020__x0028_m_x0029_D1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D2>3.7</Dimensions_x0020__x0028_m_x0029_D2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D3>2.134</Dimensions_x0020__x0028_m_x0029_D3>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L1>1.35</Dimensions_x0020__x0028_m_x0029_L1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L2>1.06</Dimensions_x0020__x0028_m_x0029_L2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L3>1.22</Dimensions_x0020__x0028_m_x0029_L3>" +
                                                    "  </TestTable>" +
                                                    "  <TestTable>" +
                                                    "    <Seal_x0020_dia_x0020_inch>86</Seal_x0020_dia_x0020_inch>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D1>4.875</Dimensions_x0020__x0028_m_x0029_D1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D2>3.78</Dimensions_x0020__x0028_m_x0029_D2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_D3>2.185</Dimensions_x0020__x0028_m_x0029_D3>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L1>1.36</Dimensions_x0020__x0028_m_x0029_L1>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L2>1.1</Dimensions_x0020__x0028_m_x0029_L2>" +
                                                    "    <Dimensions_x0020__x0028_m_x0029_L3>1.25</Dimensions_x0020__x0028_m_x0029_L3>" +
                                                    "  </TestTable>" +
                                                    "</NewDataSet>");
            DataSet theDataSet = new DataSet();
            theDataSet.ReadXml(theReader); 
            dgvMolGrid.DataSource = theDataSet.Tables[0];
        }

        private void cmbFittings_SelectedIndexChanged(object sender, EventArgs e)
        {
            ResetLabels();
            string Fittings = Convert.ToString(cmbFittings.SelectedItem);
            lblFittingLabel.Text = ": " + Fittings;
            DataTable dt = new DataTable();
            dt.Columns.Add("ID");
            dt.Columns.Add("Type");
            if (Fittings.Trim() == "Orifice")
            {
                txtPIPE_DIA.Enabled = true;
                txtPIPE_DIA1.Enabled = true;

                lblPIPE_DIA1.Text = "value of Orifice Diameter D2 :";
                FittingPic.Image = Resources._1;
                //LoadImage("1.JPG");
            }
            else if (Fittings.Trim() == "Flare Tip")
            {
                txtPIPE_DIA.Enabled = true;
                txtPIPE_DIA1.Enabled = true;
                txtROUGHNESS.Enabled = true;
                txtLENGTH_PIPE.Enabled = true;
                txtLENGTH_PIPE1.Enabled = true;

                DataRow dr = dt.NewRow();
                dr["ID"] = 1;
                dr["Type"] = "Flare Tip Contant Bore";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["ID"] = 2;
                dr["Type"] = "Flare Tip Standard Bore";
                dt.Rows.Add(dr);
            }
            else if (Fittings.Trim() == "Pipe Exit")
            {
                txtPIPE_DIA.Enabled = true;

                FittingPic.Image = Resources._4;
                //LoadImage("4.JPG");
            }
            else if (Fittings.Trim() == "Molecular Seal")
            {
                txtROUGHNESS.Enabled = true;

                FittingPic.Image = Resources._6;
                LoadMolGrid();
                //LoadImage("4.JPG");
            }
            else if (Fittings.Trim() == "Velocity Seal")
            {
                txtPIPE_DIA.Enabled = true;
                txtROUGHNESS.Enabled = true;

                FittingPic.Image = Resources._5;
                //LoadImage("5.JPG");
                DataRow dr = dt.NewRow();
                dr["ID"] = 1;
                dr["Type"] = "Seal Dia = 50% of Pipe Dia";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["ID"] = 2;
                dr["Type"] = "Seal Dia = 75% of Pipe Dia";
                dt.Rows.Add(dr);
            }
            else if (Fittings.Trim() == "Straight Pipe")
            {
                txtPIPE_DIA.Enabled = true;
                txtLENGTH_PIPE.Enabled = true;
                txtROUGHNESS.Enabled = true;

                FittingPic.Image = Resources._7;
                //LoadImage("7.JPG");
            }
            else if (Fittings.Trim() == "Water Seal Drum")
            {
                txtPIPE_DIA.Enabled = true;
                txtPIPE_DIA1.Enabled = true;
                txtSEAL_DIA.Enabled = true;
                txtBUCKET_DIA.Enabled = true;
                txtROUGHNESS.Enabled = true;
                lblBUCKET_DIA.Text = "Enter Bucket Diameter (dbucket) :";
                lblSEAL_DIA.Text = "Enter Water Seal Diameter (dseal) :";
                lblPIPE_DIA.Text = "Enter Outlet Pipe Diameter D :";
                lblPIPE_DIA1.Text = "Enter Inlet Pipe Diameter (dpipe) :";
                DataRow dr = dt.NewRow();
                dr["ID"] = 1;
                dr["Type"] = "Water Seal Drum Above 10 Inch";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["ID"] = 2;
                dr["Type"] = "Water Seal Drum Upto 10 Inch";
                dt.Rows.Add(dr);
            }
            else if (Fittings.Trim() == "Sudden Contaction")
            {
                txtPIPE_DIA.Enabled = true;
                txtPIPE_DIA1.Enabled = true;

                lblPIPE_DIA1.Text = "Upstream(Larger) Pipe Diameter D1:";
                FittingPic.Image = Resources._10;
                //LoadImage("10.JPG");
            }
            else if (Fittings.Trim() == "Sudden Expansion")
            {
                txtPIPE_DIA.Enabled = true;
                txtPIPE_DIA1.Enabled = true;

                FittingPic.Image = Resources._11;
                //LoadImage("11.JPG");
            }
            else if (Fittings.Trim() == "Gradual Contraction")
            {
                txtPIPE_DIA.Enabled = true;
                txtPIPE_DIA1.Enabled = true;
                txtLENGTH_PIPE.Enabled = true;
                txtROUGHNESS.Enabled = true;
                txtPIPE_DIA1.Enabled = true;

                lblPIPE_DIA1.Text = "Upstream(Larger) Pipe Diameter D1:";
                FittingPic.Image = Resources._12;
                //LoadImage("12.JPG");
            }
            else if (Fittings.Trim() == "Gradual Expansion")
            {
                txtPIPE_DIA.Enabled = true;
                txtPIPE_DIA1.Enabled = true;
                txtLENGTH_PIPE.Enabled = true;
                txtROUGHNESS.Enabled = true;
                txtPIPE_DIA1.Enabled = true;

                FittingPic.Image = Resources._13;
                //LoadImage("13.JPG");
            }
            else if (Fittings.Trim() == "Elbow")
            {
                txtPIPE_DIA.Enabled = true;
                txtFITTING_QUANTITY.Enabled = true;
                txtROUGHNESS.Enabled = true;
                txtRbyD.Enabled = true;
                txtTHETA.Enabled = true;

                FittingPic.Image = Resources._14;
                //LoadImage("14.JPG");
            }
            else if (Fittings.Trim() == "Mitre Bend")
            {
                txtMAJOR_AXIS.Enabled = true;
                txtMINOR_AXIS.Enabled = true;
                txtFITTING_QUANTITY.Enabled = true;
                txtROUGHNESS.Enabled = true;

                FittingPic.Image = Resources._15;
                //LoadImage("15.JPG");
                DataRow dr = dt.NewRow();
                dr["ID"] = 1;
                dr["Type"] = "Mitre Bend Theta = 0 degree";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["ID"] = 2;
                dr["Type"] = "Mitre Bend Theta = 15 degree";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["ID"] = 3;
                dr["Type"] = "Mitre Bend Theta = 30 degree";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["ID"] = 4;
                dr["Type"] = "Mitre Bend Theta = 45 degree";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["ID"] = 5;
                dr["Type"] = "Mitre Bend Theta = 60 degree";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["ID"] = 6;
                dr["Type"] = "Mitre Bend Theta = 75 degree";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["ID"] = 7;
                dr["Type"] = "Mitre Bend Theta = 90 degree";
                dt.Rows.Add(dr);
            }
            else if (Fittings.Trim() == "Annular Section")
            {
                txtPIPE_DIA.Enabled = true;
                txtPIPE_DIA1.Enabled = true;
                txtROUGHNESS.Enabled = true;
                txtLENGTH_PIPE.Enabled = true;

                lblPIPE_DIA1.Text = " value of Outer Pipe Diameter D:";
                FittingPic.Image = Resources._16;
                //LoadImage("16.JPG");
            }
            else if (Fittings.Trim() == "Rectangular Duct")
            {
                txtMAJOR_AXIS.Enabled = true;
                txtMINOR_AXIS.Enabled = true;
                txtROUGHNESS.Enabled = true;
                txtLENGTH_PIPE.Enabled = true;

                FittingPic.Image = Resources._17;
                //LoadImage("17.JPG");
            }
            else if (Fittings.Trim() == "Rectangular Elbow")
            {
                txtMAJOR_AXIS.Enabled = true;
                txtMINOR_AXIS.Enabled = true;
                txtRbyD.Enabled = true;
                txtTHETA.Enabled = true;

                FittingPic.Image = Resources._18;
                //LoadImage("18.JPG");
            }
            else if (Fittings.Trim() == "T Thru")
            {
                txtPIPE_DIA.Enabled = true;
                txtFITTING_QUANTITY.Enabled = true;
                txtROUGHNESS.Enabled = true;

                FittingPic.Image = Resources._19;
                //LoadImage("19.JPG");
            }
            else if (Fittings.Trim() == "T Branch to Run Flow")
            {
                txtPIPE_DIA.Enabled = true;
                txtFITTING_QUANTITY.Enabled = true;
                txtROUGHNESS.Enabled = true;

                FittingPic.Image = Resources._20;
                //LoadImage("20.JPG");
                DataRow dr = dt.NewRow();
                dr["ID"] = 1;
                dr["Type"] = "T Branching Flow Theta = 0 degree";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["ID"] = 2;
                dr["Type"] = "T Branching Flow Theta = 15 degree";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["ID"] = 3;
                dr["Type"] = "T Branching Flow Theta = 30 degree";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["ID"] = 4;
                dr["Type"] = "T Branching Flow Theta = 45 degree";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["ID"] = 5;
                dr["Type"] = "T Branching Flow Theta = 60 degree";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["ID"] = 6;
                dr["Type"] = "T Branching Flow Theta = 75 degree";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["ID"] = 7;
                dr["Type"] = "T Branching Flow Theta = 90 degree";
                dt.Rows.Add(dr);
            }
            else if (Fittings.Trim() == "T Run to Branch Flow")
            {
                txtPIPE_DIA.Enabled = true;
                txtFITTING_QUANTITY.Enabled = true;
                txtROUGHNESS.Enabled = true;

                FittingPic.Image = Resources._21;
                //LoadImage("21.JPG");
                DataRow dr = dt.NewRow();
                dr["ID"] = 1;
                dr["Type"] = "T Branching Flow Theta = 0 degree";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["ID"] = 2;
                dr["Type"] = "T Branching Flow Theta = 15 degree";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["ID"] = 3;
                dr["Type"] = "T Branching Flow Theta = 30 degree";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["ID"] = 4;
                dr["Type"] = "T Branching Flow Theta = 45 degree";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["ID"] = 5;
                dr["Type"] = "T Branching Flow Theta = 60 degree";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["ID"] = 6;
                dr["Type"] = "T Branching Flow Theta = 75 degree";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["ID"] = 7;
                dr["Type"] = "T Branching Flow Theta = 90 degree";
                dt.Rows.Add(dr);
            }
            else if (Fittings.Trim() == "Gate valve")
            {
                txtPIPE_DIA.Enabled = true;
                txtFITTING_QUANTITY.Enabled = true;
                txtROUGHNESS.Enabled = true;

                FittingPic.Image = Resources._22;
                //LoadImage("22.JPG");
                DataRow dr = dt.NewRow();
                dr["ID"] = 1;
                dr["Type"] = "Gate Valve 1by2 Open";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["ID"] = 2;
                dr["Type"] = "Gate Valve 1by4 Open";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["ID"] = 3;
                dr["Type"] = "Gate Valve 3by4 Open";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["ID"] = 4;
                dr["Type"] = "Gate Valve Fully Open";
                dt.Rows.Add(dr);
            }
            else if (Fittings.Trim() == "Ball Valve")
            {
                txtPIPE_DIA.Enabled = true;
                txtFITTING_QUANTITY.Enabled = true;
                txtROUGHNESS.Enabled = true;

                FittingPic.Image = Resources._23;
                //LoadImage("23.JPG");
            }
            else if (Fittings.Trim() == "Globe valve")
            {
                txtPIPE_DIA.Enabled = true;
                txtFITTING_QUANTITY.Enabled = true;
                txtROUGHNESS.Enabled = true;

                FittingPic.Image = Resources._24;
                //LoadImage("24.JPG");
                DataRow dr = dt.NewRow();
                dr["ID"] = 1;
                dr["Type"] = "Globe Valve 1by2 Open";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["ID"] = 2;
                dr["Type"] = "Globe Valve Fully Open";
                dt.Rows.Add(dr);
            }
            else if (Fittings.Trim() == "Butterfly Valve")
            {
                txtPIPE_DIA.Enabled = true;
                txtFITTING_QUANTITY.Enabled = true;
                txtROUGHNESS.Enabled = true;

                FittingPic.Image = Resources._25;
                //LoadImage("25.JPG");
                DataRow dr = dt.NewRow();
                dr["ID"] = 1;
                dr["Type"] = "Butterfly Valve theta = 5 degree";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["ID"] = 2;
                dr["Type"] = "Butterfly Valve theta = 10 degree";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["ID"] = 3;
                dr["Type"] = "Butterfly Valve theta = 20 degree";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["ID"] = 4;
                dr["Type"] = "Butterfly Valve theta = 40 degree";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["ID"] = 5;
                dr["Type"] = "Butterfly Valve theta = 60 degree";
                dt.Rows.Add(dr);
            }
            else if (Fittings.Trim() == "Check valve")
            {
                txtPIPE_DIA.Enabled = true;
                txtFITTING_QUANTITY.Enabled = true;
                txtROUGHNESS.Enabled = true;

                FittingPic.Image = Resources._26;
                //LoadImage("26.JPG");
                DataRow dr = dt.NewRow();
                dr["ID"] = 1;
                dr["Type"] = "Check Valve Ball";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["ID"] = 2;
                dr["Type"] = "Check Valve Disc";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["ID"] = 3;
                dr["Type"] = "Check Valve Swing";
                dt.Rows.Add(dr);
            }
            else if (Fittings.Trim() == "Angle valve")
            {
                txtPIPE_DIA.Enabled = true;
                txtFITTING_QUANTITY.Enabled = true;
                txtROUGHNESS.Enabled = true;

                FittingPic.Image = Resources.E1_Angle_valve;
                //LoadImage("E1_Angle valve.JPG");
            }
            else if (Fittings.Trim() == "Diaphram valve")
            {
                txtPIPE_DIA.Enabled = true;
                txtFITTING_QUANTITY.Enabled = true;
                txtROUGHNESS.Enabled = true;

                DataRow dr = dt.NewRow();
                dr["ID"] = 1;
                dr["Type"] = "Diaphram Valve 1by2 Open";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["ID"] = 2;
                dr["Type"] = "Diaphram Valve 1by4 Open";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["ID"] = 3;
                dr["Type"] = "Diaphram Valve 3by4 Open";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["ID"] = 4;
                dr["Type"] = "Diaphram Valve Fully Open";
                dt.Rows.Add(dr);
            }
            else if (Fittings.Trim() == "Plug Cock")
            {
                txtPIPE_DIA.Enabled = true;
                txtFITTING_QUANTITY.Enabled = true;
                txtROUGHNESS.Enabled = true;

                FittingPic.Image = Resources.E5_Plug_cock;
                //LoadImage("E5_Plug cock.JPG");
                DataRow dr = dt.NewRow();
                dr["ID"] = 1;
                dr["Type"] = "Plug Cock theta = 5 degree";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["ID"] = 2;
                dr["Type"] = "Plug Cock theta = 10 degree";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["ID"] = 3;
                dr["Type"] = "Plug Cock theta = 20 degree";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["ID"] = 4;
                dr["Type"] = "Plug Cock theta = 40 degree";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["ID"] = 4;
                dr["Type"] = "Plug Cock theta = 60 degree";
                dt.Rows.Add(dr);
            }
            else if (Fittings.Trim() == "Coupling")
            {
                txtPIPE_DIA.Enabled = true;
                txtFITTING_QUANTITY.Enabled = true;
                txtROUGHNESS.Enabled = true;
            } 
            else if (Fittings.Trim() == "Foot valve")
            {
                txtPIPE_DIA.Enabled = true;
                txtFITTING_QUANTITY.Enabled = true;
                txtROUGHNESS.Enabled = true;
            } 
            else if (Fittings.Trim() == "Union")
            {
                txtPIPE_DIA.Enabled = true;
                txtFITTING_QUANTITY.Enabled = true;
                txtROUGHNESS.Enabled = true;
            }

            cmbType.DataSource = dt;
            cmbType.DisplayMember = "Type";
            cmbType.ValueMember = "ID";
            cmbType.SelectedIndex = dt.Rows.Count - 1;
            btnCalculate.Enabled = true;
             
        }

       

        private void ResetLocations()
        {
            for (int i = 0; i < CommonClass.obj.Count; i++)
            {
                if (i != 0)
                {
                    Calculate Calculatefrm = (Calculate)CommonClass.obj[i];
                    Point location = ((Calculate)CommonClass.obj[Convert.ToInt32(i) - 1]).Location;
                    Calculatefrm.Location = new Point(0, location.Y + 616);
                }
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            string CurrentIndex = this.Text.Replace("Fitting ", "");
            Point location = ((Calculate)CommonClass.obj[Convert.ToInt32(CurrentIndex) - 1]).Location;
            CommonClass.obj.Remove(CommonClass.obj[Convert.ToInt32(CurrentIndex) - 1]);
            for (int i = 0; i < CommonClass.obj.Count; i++)
            {
                if (i >= Convert.ToInt32(CurrentIndex) - 1)
                {
                    Calculate Calculatefrm = (Calculate)CommonClass.obj[i];
                    Calculatefrm.Text = "Fitting " + (i + 1).ToString();
                    Calculatefrm.Controls["panel2"].Controls["label8"].Text = Calculatefrm.Text;
                    Point location1 = Calculatefrm.Location;
                    Calculatefrm.Location = location;
                    location = location1;
                }
            }
            this.Dispose();
        }

        private void panel8_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            //dttemp.Rows.Clear();
        }

        private void btnNewCalculation_Click(object sender, EventArgs e)
        {
            //isNewMeasure = true;
            Form Calculatefrm = new Calculate();
            Calculatefrm.MdiParent = this.MdiParent;
            Calculatefrm.Text = "Fitting " + (CommonClass.obj.Count + 1).ToString();
            Calculatefrm.Controls["panel2"].Controls["label8"].Text = Calculatefrm.Text;
            Calculatefrm.Show();
            CommonClass.obj.Add(Calculatefrm);
            CommonClass.objFitting.Add("");
            CommonClass.objType.Add("");
            CommonClass.objPST0.Add("");
            CommonClass.objDELP.Add("");
             
            try
            {
                CalculateFunc();
                Calculatefrm.Controls["panel12"].Controls["txtPST0"].Text = CommonClass.objPST0[CommonClass.obj.Count - 1].ToString();
                Calculatefrm.Controls["panel4"].Controls["txtMASS_FLOW_RATE"].Text = txtMASS_FLOW_RATE.Text;
                Calculatefrm.Controls["panel4"].Controls["txtTEMP"].Text = txtTEMP.Text;
                Calculatefrm.Controls["panel4"].Controls["txtMOLEQ_WEIGHT"].Text = txtMOLEQ_WEIGHT.Text;
                Calculatefrm.Controls["panel4"].Controls["txtVISC"].Text = txtVISC.Text;
                Calculatefrm.Controls["panel4"].Controls["txtGAMMA"].Text = txtGAMMA.Text;
            }
            catch (Exception){}
            ResetLocations();
        }

        private void btnInsertNewForm_Click(object sender, EventArgs e)
        { 
            string CurrentIndex = this.Text.Replace("Fitting ", "");
            Form Calculatefrm = new Calculate();
            Calculatefrm.MdiParent = this.MdiParent;
            Calculatefrm.Text = "Fitting " + (Convert.ToInt32(CurrentIndex) + 1).ToString();
            Calculatefrm.Controls["panel2"].Controls["label8"].Text = Calculatefrm.Text;
            Calculatefrm.Show();
            CommonClass.obj.Insert(Convert.ToInt32(CurrentIndex), Calculatefrm);
            CommonClass.objFitting.Insert(Convert.ToInt32(CurrentIndex), "");
            CommonClass.objType.Insert(Convert.ToInt32(CurrentIndex), "");
            CommonClass.objPST0.Insert(Convert.ToInt32(CurrentIndex), "");
            CommonClass.objDELP.Insert(Convert.ToInt32(CurrentIndex), "");
            
            for (int i = 0; i < CommonClass.obj.Count; i++)
            {
                if (i > Convert.ToInt32(CurrentIndex))
                {
                    Form clFrm = new Calculate();
                    clFrm = (Calculate)CommonClass.obj[i];
                    clFrm.Text = "Fitting " + (Convert.ToInt32(i) + 1).ToString();
                    clFrm.Controls["panel2"].Controls["label8"].Text = clFrm.Text;
                    
                    try
                    {
                        Calculatefrm.Controls["panel12"].Controls["txtPST0"].Text = CommonClass.objPST0[Convert.ToInt32(CurrentIndex) - 1].ToString();
                        Calculatefrm.Controls["panel4"].Controls["txtMASS_FLOW_RATE"].Text = txtMASS_FLOW_RATE.Text;
                        Calculatefrm.Controls["panel4"].Controls["txtTEMP"].Text = txtTEMP.Text;
                        Calculatefrm.Controls["panel4"].Controls["txtMOLEQ_WEIGHT"].Text = txtMOLEQ_WEIGHT.Text;
                        Calculatefrm.Controls["panel4"].Controls["txtVISC"].Text = txtVISC.Text;
                        Calculatefrm.Controls["panel4"].Controls["txtGAMMA"].Text = txtGAMMA.Text;
                    }
                    catch (Exception) { }
                }
            }
            ResetLocations();
        }

        private double ConvertMASS_FLOW_RATEAsPerMeasure(double MASS_FLOW_RATE)
        {
            string MassFlMes = Convert.ToString(cmbMassFlMes.SelectedItem);
            if (MassFlMes == "in Tons/s")
            {
                MASS_FLOW_RATE = MASS_FLOW_RATE * 1000;
            }
            return MASS_FLOW_RATE;
        }

        private double ConvertMtoMMPerMeasure(double Val, ComboBox cmb)
        {
            string Mes = Convert.ToString(cmb.SelectedItem);
            if (Mes == "in mm")
            {
                Val = Val * 1000;
            }
            return Val;
        }

        private double ConvertPST0AsPerMeasure(double Val, ComboBox cmb)
        {
            string Mes = Convert.ToString(cmb.SelectedItem);
            if (Mes == "in bar")
            {
                Val = Val * 1000;
            }
            else if (Mes == "in kgf/cm2")
            {
                Val = Val * 10000;
            }

            return Val;
        }


        private void Elbow()
        {
            double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
            double VEL0, A0;
            double VISC;
            double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
            double ZETA, ZETA90, LENGTH_PIPE;
            double P_FRIC, C0, C1, C2;
            double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
            double FITTING_QUANTITY, RbyD, THETA, LBYD, K;
            // Input - Globle Variables: There should be button for the following globle inputs

            //printf("Enter the value of Mass flow rate in kg/s: ");
            //scanf("%lf", &MASS_FLOW_RATE);
            //printf("Enter the value of Temperature in Kelvin: ");
            //scanf("%lf", &TEMP);
            //printf("Enter the value of Molecular Weight in kg/kmol: ");
            //scanf("%lf", &MOLEQ_WEIGHT);
            //printf("Enter the value of ratio of Secific heat Gamma: ");
            //scanf("%lf", &GAMMA);

            MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
            MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
            TEMP = Convert.ToDouble(txtTEMP.Text);
            MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
            GAMMA = Convert.ToDouble(txtGAMMA.Text);

            // Viscosity will be calculated based on Temperature and Molecular Weight

            VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
            // However, User can edit the Viscosity 
            //printf("Enter the value of Viscoaity in Pas: ");
            //scanf("%lf", &VISC);

            VISC = Convert.ToDouble(txtVISC.Text);

            //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

            //printf("Enter the value of Pipe Diameter D in m: ");
            //scanf("%lf", &PIPE_DIA);

            PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);

            //printf("Enter the Quantity: ");
            //scanf("%lf", &FITTING_QUANTITY);

            FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);

            //printf("Enter the value of Roughness in m: ");
            //scanf("%lf", &ROUGHNESS);

            ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

            PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
            ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);


            //printf("Enter the value of R by D: ");
            //scanf("%lf", &RbyD);
            //printf("Enter the value of Angle of Bend W in Degree: ");
            //scanf("%lf", &THETA);

            RbyD = Convert.ToDouble(txtRbyD.Text);
            THETA = Convert.ToDouble(txtTHETA.Text);
            //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

            //printf("Enter the value of Upsteam Pressure in kPa: ");
            //scanf("%lf", &PST0);

            PST0 = Convert.ToDouble(txtPST0.Text);

            PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

            // All input ends here
            //ofstream file6("Output_Data.txt");

            UNIVERSAL_GAS_CONST = 8314;
            A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
            VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
            SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
            MACH = VEL0 / SOUND_VEL;

            if (MACH >= 0.7)
            {
                MessageBox.Show("Flow is compressible please Edit the input parameters");
                return;
            }
            RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
            NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
            if (RE <= 2300)
            {
                a1 = Math.Pow((RE / 2712), 8.4);
                a = (1.0 / (1 + a1));
                a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                b1 = Math.Pow(a1, 1.8);
                b = (1.0 / (1 + b1));
                a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
            }
            else
            {
                a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                a3 = Math.Log(RE / (1.816 * a2));
                b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                FRIC_FACT = Math.Pow((-2 * b1), -2);
            }
            LBYD = 51.51 - 66.914 * Math.Log(RbyD + 1) + 32.338 * Math.Log(RbyD + 1) * Math.Log(RbyD + 1) - 3.473 * Math.Log(RbyD + 1) * Math.Log(RbyD + 1) * Math.Log(RbyD + 1);
            ZETA90 = FRIC_FACT * LBYD;
            ZETA = (((THETA / 90) - 1) * ((0.25 * PI * FRIC_FACT * RbyD) + (0.5 * ZETA90))) + ZETA90;
            P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

            C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
            C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
            C2 = MASS_FLOW_RATE / A0;
            PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
            DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
            DELP = (PST1 - PST0 * 1000);
            PST1 = PST1 / 1000;

            ShowOutPut(PST1,DELP);
        }

        private void Gradual_Expansion()
        {
            double PIPE_DIA, PIPE_DIA1, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
            double VEL0, A0, A1, VEL1;
            double VISC;
            double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
            double ZETA, LENGTH_PIPE;
            double P_FRIC, C0, C1, C2;
            double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
            double D_EQ;
            // Input - Globle Variables: There should be button for the following globle inputs

            //printf("Enter the value of Mass flow rate in kg/s: ");
            //scanf("%lf", &MASS_FLOW_RATE);
            //printf("Enter the value of Temperature in Kelvin: ");
            //scanf("%lf", &TEMP);
            //printf("Enter the value of Molecular Weight in kg/kmol: ");
            //scanf("%lf", &MOLEQ_WEIGHT);
            //printf("Enter the value of ratio of Secific heat Gamma: ");
            //scanf("%lf", &GAMMA);

            MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
            MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
            TEMP = Convert.ToDouble(txtTEMP.Text);
            MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
            GAMMA = Convert.ToDouble(txtGAMMA.Text);

            // Viscosity will be calculated based on Temperature and Molecular Weight

            VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
            // However, User can edit the Viscosity 
            //printf("Enter the value of Viscoaity in Pas: ");
            //scanf("%lf", &VISC);

            VISC = Convert.ToDouble(txtVISC.Text);

            //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

            //printf("Enter the value of Downstream (Larger) Pipe Diameter D2 in m: ");
            //scanf("%lf", &PIPE_DIA);

            PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);

            //printf("Enter the value of Upstream (Smaller) Pipe Diameter D1 in m: ");
            //scanf("%lf", &PIPE_DIA1);

            PIPE_DIA1 = Convert.ToDouble(txtPIPE_DIA1.Text);

            //printf("Enter the value of Length of Expansion L in m: ");
            //scanf("%lf", &LENGTH_PIPE);

            LENGTH_PIPE = Convert.ToDouble(txtLENGTH_PIPE.Text);

            //printf("Enter the value of Roughness in m: ");
            //scanf("%lf", &ROUGHNESS);

            ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

            PIPE_DIA1 = ConvertMtoMMPerMeasure(PIPE_DIA1, cmbPIPE_DIA1);
            PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
            ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);

            //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

            //printf("Enter the value of Downsream Pressure in kPa: ");
            //scanf("%lf", &PST0);

            PST0 = Convert.ToDouble(txtPST0.Text);
            PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);
            // All input ends here

            UNIVERSAL_GAS_CONST = 8314;
            A0 = (PI * PIPE_DIA * PIPE_DIA / 4); A1 = (PI * PIPE_DIA1 * PIPE_DIA1 / 4);
            DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
            VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
            SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
            MACH = VEL0 / SOUND_VEL;

            if (MACH >= 0.7)
            {
                MessageBox.Show("Flow is compressible please Edit the input parameters");
                return;
            }
            RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
            NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
            if (RE <= 2300)
            {
                a1 = Math.Pow((RE / 2712), 8.4);
                a = (1.0 / (1 + a1));
                a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                b1 = Math.Pow(a1, 1.8);
                b = (1.0 / (1 + b1));
                a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
            }
            else
            {
                a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                a3 = Math.Log(RE / (1.816 * a2));
                b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                FRIC_FACT = Math.Pow((-2 * b1), -2);
            }
            D_EQ = 3 * PIPE_DIA1 * PIPE_DIA1 * PIPE_DIA1 / (PIPE_DIA * PIPE_DIA + PIPE_DIA1 * PIPE_DIA + PIPE_DIA1 * PIPE_DIA1);
            //ZETA = FRIC_FACT*LENGTH_PIPE/PIPE_DIA;
            ZETA = FRIC_FACT * LENGTH_PIPE / D_EQ;
            //ZETA = 0.5*(1-(A0/A1));
            P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0;

            C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
            C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
            C2 = MASS_FLOW_RATE / A1;
            PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
            DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
            VEL1 = (MASS_FLOW_RATE / (A1 * DEN1));
            DELP = (PST1 - PST0 * 1000);
            PST1 = PST1 / 1000;

            ShowOutPut(PST1, DELP);
        }

        private void Gradual_Contaction()
        {
            double PIPE_DIA, PIPE_DIA1, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
            double VEL0, A0, A1, VEL1;
            double VISC;
            double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
            double ZETA, LENGTH_PIPE;
            double P_FRIC, C0, C1, C2;
            double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
            double D_EQ;
            // Input - Globle Variables: There should be button for the following globle inputs

            //printf("Enter the value of Mass flow rate in kg/s: ");
            //scanf("%lf", &MASS_FLOW_RATE);
            //printf("Enter the value of Temperature in Kelvin: ");
            //scanf("%lf", &TEMP);
            //printf("Enter the value of Molecular Weight in kg/kmol: ");
            //scanf("%lf", &MOLEQ_WEIGHT);
            //printf("Enter the value of ratio of Secific heat Gamma: ");
            //scanf("%lf", &GAMMA);

            MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
            MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
            TEMP = Convert.ToDouble(txtTEMP.Text);
            MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
            GAMMA = Convert.ToDouble(txtGAMMA.Text);

            // Viscosity will be calculated based on Temperature and Molecular Weight

            VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
            // However, User can edit the Viscosity 
            //printf("Enter the value of Viscoaity in Pas: ");
            //scanf("%lf", &VISC);

            VISC = Convert.ToDouble(txtVISC.Text);

            //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

            //printf("Enter the value of Downstream (Smaller) Pipe Diameter D2 in m: ");
            //scanf("%lf", &PIPE_DIA);

            PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);

            //printf("Enter the value of Upstream (Larger) Pipe Diameter D1 in m: ");
            //scanf("%lf", &PIPE_DIA1);

            PIPE_DIA1 = Convert.ToDouble(txtPIPE_DIA1.Text);

            //printf("Enter the value of Length of the contraction L in m: ");
            //scanf("%lf", &LENGTH_PIPE);

            LENGTH_PIPE = Convert.ToDouble(txtLENGTH_PIPE.Text);

            //printf("Enter the value of Roughness in m: ");
            //scanf("%lf", &ROUGHNESS);

            ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

            PIPE_DIA1 = ConvertMtoMMPerMeasure(PIPE_DIA1, cmbPIPE_DIA1);
            PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
            ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);

            //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

            //printf("Enter the value of Downsream Pressure in kPa: ");
            //scanf("%lf", &PST0);

            PST0 = Convert.ToDouble(txtPST0.Text);
            PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

            // All input ends here

            UNIVERSAL_GAS_CONST = 8314;
            A0 = (PI * PIPE_DIA * PIPE_DIA / 4); A1 = (PI * PIPE_DIA1 * PIPE_DIA1 / 4);
            DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
            VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
            SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
            MACH = VEL0 / SOUND_VEL;

            if (MACH >= 0.7)
            {
                MessageBox.Show("Flow is compressible please Edit the input parameters");
                return;
            }
            RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
            NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
            if (RE <= 2300)
            {
                a1 = Math.Pow((RE / 2712), 8.4);
                a = (1.0 / (1 + a1));
                a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                b1 = Math.Pow(a1, 1.8);
                b = (1.0 / (1 + b1));
                a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
            }
            else
            {
                a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                a3 = Math.Log(RE / (1.816 * a2));
                b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                FRIC_FACT = Math.Pow((-2 * b1), -2);
            }
            D_EQ = 3 * PIPE_DIA1 * PIPE_DIA1 * PIPE_DIA1 / (PIPE_DIA * PIPE_DIA + PIPE_DIA1 * PIPE_DIA + PIPE_DIA1 * PIPE_DIA1);
            //ZETA = FRIC_FACT*LENGTH_PIPE/PIPE_DIA;
            ZETA = FRIC_FACT * LENGTH_PIPE / D_EQ;
            //ZETA = 0.5*(1-(A0/A1));
            P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0;

            C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
            C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
            C2 = MASS_FLOW_RATE / A1;
            PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
            DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
            VEL1 = (MASS_FLOW_RATE / (A1 * DEN1));
            DELP = (PST1 - PST0 * 1000);
            PST1 = PST1 / 1000;

            ShowOutPut(PST1, DELP);
        }

        private void Sudden_Expansion()
        {
            double PIPE_DIA, PIPE_DIA1, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
            double VEL0, A0, A1, VEL1;
            double VISC;
            double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
            double ZETA, LENGTH_PIPE;
            double P_FRIC, C0, C1, C2;
            double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
            // Input - Globle Variables: There should be button for the following globle inputs

            //printf("Enter the value of Mass flow rate in kg/s: ");
            //scanf("%lf", &MASS_FLOW_RATE);
            //printf("Enter the value of Temperature in Kelvin: ");
            //scanf("%lf", &TEMP);
            //printf("Enter the value of Molecular Weight in kg/kmol: ");
            //scanf("%lf", &MOLEQ_WEIGHT);
            //printf("Enter the value of ratio of Secific heat Gamma: ");
            //scanf("%lf", &GAMMA);

            MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
            MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
            TEMP = Convert.ToDouble(txtTEMP.Text);
            MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
            GAMMA = Convert.ToDouble(txtGAMMA.Text);

            // Viscosity will be calculated based on Temperature and Molecular Weight

            VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
            // However, User can edit the Viscosity 
            //printf("Enter the value of Viscoaity in Pas: ");
            //scanf("%lf", &VISC);

            VISC = Convert.ToDouble(txtVISC.Text);

            //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

            //printf("Enter the value of Downstream (Larger) Pipe Diameter D2 in m: ");
            //scanf("%lf", &PIPE_DIA);

            PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);

            //printf("Enter the value of Upstream (Smaller) Pipe Diameter D1 in m: ");
            //scanf("%lf", &PIPE_DIA1);

            PIPE_DIA1 = Convert.ToDouble(txtPIPE_DIA1.Text);


            PIPE_DIA1 = ConvertMtoMMPerMeasure(PIPE_DIA1, cmbPIPE_DIA1);
            PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);

            //printf("Enter the value of Length of Pipe in m: ");
            //scanf("%lf", &LENGTH_PIPE);
            //printf("Enter the value of Roughness in m: ");
            //scanf("%lf", &ROUGHNESS);

            //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

            //printf("Enter the value of Downsream Pressure in kPa: ");
            //scanf("%lf", &PST0);

            PST0 = Convert.ToDouble(txtPST0.Text);

            PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

            // All input ends here
            //ofstream file6("Output_Data.txt");
            UNIVERSAL_GAS_CONST = 8314;
            A0 = (PI * PIPE_DIA * PIPE_DIA / 4); A1 = (PI * PIPE_DIA1 * PIPE_DIA1 / 4);
            DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
            VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
            SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
            MACH = VEL0 / SOUND_VEL;

            if (MACH >= 0.7)
            {
                MessageBox.Show("Flow is compressible please Edit the input parameters");
                return;
            }
            /*RE = (DEN0*VEL0*PIPE_DIA)/VISC;
            NONDIM_ROUGHNESS = ROUGHNESS/PIPE_DIA;
              if(RE<=2300)
              {
              a1 = Math.Pow((RE/2712),8.4); 
              a = (1.0/(1+a1));
              a1 = ((RE/150)*NONDIM_ROUGHNESS);
              b1 = Math.Pow(a1,1.8);
              b = (1.0/(1+b1));
              a1 = Math.Pow((64/RE),a); a2 = 0.75*(Math.Log(RE/5.37)); a3 = 0.88*(Math.Log(6.82/NONDIM_ROUGHNESS));
              FRIC_FACT = (a1)*(Math.Pow(a2,(2*b*(a-1))))*(Math.Pow(a3,(2*(1-b)*(a-1))));
              }
              else
              {
                  a1 = Math.Log(1+1.1*RE); a2 = Math.Log(1.1*RE/a1);
                  a3 = Math.Log(RE/(1.816*a2));
                  b1 = (1.0/2.303)*Math.Log((2.18*a3/RE)+ (NONDIM_ROUGHNESS/3.71));
                  FRIC_FACT = Math.Pow((-2*b1),-2);
              }*/
            //ZETA = FRIC_FACT*LENGTH_PIPE/PIPE_DIA;
            ZETA = (1 - (A1 / A0)) * (1 - (A1 / A0)) / ((A1 / A0) * (A1 / A0));

            P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0;

            C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
            C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
            C2 = MASS_FLOW_RATE / A1;
            PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
            DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
            VEL1 = (MASS_FLOW_RATE / (A1 * DEN1));
            DELP = (PST1 - PST0 * 1000);
            PST1 = PST1 / 1000;

            ShowOutPut(PST1, DELP);
            //file6 << "Friction Pressure Drop" << "\t" << P_FRIC << endl;
            //file6 << "Pressure Drop in Pascal" << "\t" << DELP << endl;
        }

        private void Sudden_Contaction()
        {
            double PIPE_DIA, PIPE_DIA1, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
            double VEL0, A0, A1, VEL1;
            double VISC;
            double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
            double ZETA, LENGTH_PIPE;
            double P_FRIC, C0, C1, C2;
            double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
            // Input - Globle Variables: There should be button for the following globle inputs

            //printf("Enter the value of Mass flow rate in kg/s: ");
            //scanf("%lf", &MASS_FLOW_RATE);
            //printf("Enter the value of Temperature in Kelvin: ");
            //scanf("%lf", &TEMP);
            //printf("Enter the value of Molecular Weight in kg/kmol: ");
            //scanf("%lf", &MOLEQ_WEIGHT);
            //printf("Enter the value of ratio of Secific heat Gamma: ");
            //scanf("%lf", &GAMMA);

            MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
            MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
            TEMP = Convert.ToDouble(txtTEMP.Text);
            MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
            GAMMA = Convert.ToDouble(txtGAMMA.Text);

            // Viscosity will be calculated based on Temperature and Molecular Weight

            VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
            // However, User can edit the Viscosity 
            //printf("Enter the value of Viscoaity in Pas: ");
            //scanf("%lf", &VISC);

            VISC = Convert.ToDouble(txtVISC.Text);

            //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

            //printf("Enter the value of Downstream (Smaller) Pipe Diameter D2 in m: ");
            //scanf("%lf", &PIPE_DIA);

            PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);

            //printf("Enter the value of Upstream (Larger) Pipe Diameter D1 in m: ");
            //scanf("%lf", &PIPE_DIA1);

            PIPE_DIA1 = Convert.ToDouble(txtPIPE_DIA1.Text);

            PIPE_DIA1 = ConvertMtoMMPerMeasure(PIPE_DIA1, cmbPIPE_DIA1);
            PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);

            //printf("Enter the value of Length of Pipe in m: ");
            //scanf("%lf", &LENGTH_PIPE);
            //printf("Enter the value of Roughness in m: ");
            //scanf("%lf", &ROUGHNESS);

            //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

            //printf("Enter the value of Downsream Pressure in kPa: ");
            //scanf("%lf", &PST0);

            PST0 = Convert.ToDouble(txtPST0.Text);
            PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

            // All input ends here
            // ofstream file6("Output_Data.txt");
            UNIVERSAL_GAS_CONST = 8314;
            A0 = (PI * PIPE_DIA * PIPE_DIA / 4); A1 = (PI * PIPE_DIA1 * PIPE_DIA1 / 4);
            DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
            VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
            SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
            MACH = VEL0 / SOUND_VEL;

            if (MACH >= 0.7)
            {
                MessageBox.Show("Flow is compressible please Edit the input parameters");
                return;
            }
            /*RE = (DEN0*VEL0*PIPE_DIA)/VISC;
            NONDIM_ROUGHNESS = ROUGHNESS/PIPE_DIA;
              if(RE<=2300)
              {
              a1 = Math.Pow((RE/2712),8.4); 
              a = (1.0/(1+a1));
              a1 = ((RE/150)*NONDIM_ROUGHNESS);
              b1 = Math.Pow(a1,1.8);
              b = (1.0/(1+b1));
              a1 = Math.Pow((64/RE),a); a2 = 0.75*(Math.Log(RE/5.37)); a3 = 0.88*(Math.Log(6.82/NONDIM_ROUGHNESS));
              FRIC_FACT = (a1)*(Math.Pow(a2,(2*b*(a-1))))*(Math.Pow(a3,(2*(1-b)*(a-1))));
              }
              else
              {
                  a1 = Math.Log(1+1.1*RE); a2 = Math.Log(1.1*RE/a1);
                  a3 = Math.Log(RE/(1.816*a2));
                  b1 = (1.0/2.303)*Math.Log((2.18*a3/RE)+ (NONDIM_ROUGHNESS/3.71));
                  FRIC_FACT = Math.Pow((-2*b1),-2);
              }*/
            //ZETA = FRIC_FACT*LENGTH_PIPE/PIPE_DIA;
            ZETA = 0.5 * (1 - (A0 / A1));
            P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0;

            C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
            C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
            C2 = MASS_FLOW_RATE / A1;
            PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
            DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
            VEL1 = (MASS_FLOW_RATE / (A1 * DEN1));
            DELP = (PST1 - PST0 * 1000);
            PST1 = PST1 / 1000;

            ShowOutPut(PST1, DELP);
        }

        private void Circular_Pipe_Pressure_Drop()
        {
            double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
            double VEL0, A0;
            double VISC;
            double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
            double ZETA, LENGTH_PIPE;
            double P_FRIC, C0, C1, C2;
            double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
            // Input - Globle Variables: There should be button for the following globle inputs

            //printf("Enter the value of Mass flow rate in kg/s: ");
            //scanf("%lf", &MASS_FLOW_RATE);
            //printf("Enter the value of Temperature in Kelvin: ");
            //scanf("%lf", &TEMP);
            //printf("Enter the value of Molecular Weight in kg/kmol: ");
            //scanf("%lf", &MOLEQ_WEIGHT);
            //printf("Enter the value of ratio of Secific heat Gamma: ");
            //scanf("%lf", &GAMMA);

            MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
            MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
            TEMP = Convert.ToDouble(txtTEMP.Text);
            MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
            GAMMA = Convert.ToDouble(txtGAMMA.Text);

            // Viscosity will be calculated based on Temperature and Molecular Weight

            VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
            // However, User can edit the Viscosity 
            //printf("Enter the value of Viscoaity in Pas: ");
            //scanf("%lf", &VISC);

            VISC = Convert.ToDouble(txtVISC.Text);

            //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

            //printf("Enter the value of Pipe Diameter D in m: ");
            //scanf("%lf", &PIPE_DIA);
            //printf("Enter the value of Length of Pipe L in m: ");
            //scanf("%lf", &LENGTH_PIPE);
            //printf("Enter the value of Roughness in m: ");
            //scanf("%lf", &ROUGHNESS);

            PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);
            LENGTH_PIPE = Convert.ToDouble(txtLENGTH_PIPE.Text);
            ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);


            PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
            LENGTH_PIPE = ConvertMtoMMPerMeasure(LENGTH_PIPE, cmbLENGTH_PIPE);
            ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);

            //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

            //printf("Enter the value of Downsream Pressure in kPa: ");
            //scanf("%lf", &PST0);

            PST0 = Convert.ToDouble(txtPST0.Text);
            PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

            // All input ends here

            UNIVERSAL_GAS_CONST = 8314;
            A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
            VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
            SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
            MACH = VEL0 / SOUND_VEL;

            if (MACH >= 0.7)
            {
                MessageBox.Show("Flow is compressible please Edit the input parameters");
                return;
            }

            RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
            NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
            if (RE <= 2300)
            {
                a1 = Math.Pow((RE / 2712), 8.4);
                a = (1.0 / (1 + a1));
                a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                b1 = Math.Pow(a1, 1.8);
                b = (1.0 / (1 + b1));
                a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
            }
            else
            {
                a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                a3 = Math.Log(RE / (1.816 * a2));
                b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                FRIC_FACT = Math.Pow((-2 * b1), -2);
            }
            ZETA = FRIC_FACT * LENGTH_PIPE / PIPE_DIA;
            P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0;

            C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
            C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
            C2 = MASS_FLOW_RATE / A0;
            PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
            DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
            DELP = (PST1 - PST0 * 1000);
            PST1 = PST1 / 1000;

            ShowOutPut(PST1, DELP);
        }

        private void calculatePipeExit()
        {
            double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
            double VEL0, VEL1, A0;
            double VISC;
            double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
            double ZETA, LENGTH_PIPE;
            double P_FRIC, C0, C1, C2;
            double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
            double FITTING_QUANTITY = 1, LBYD, K = 1.0;
            // Input - Globle Variables: There should be button for the following globle inputs

            //printf("Enter the value of Mass flow rate in kg/s: ");
            //scanf("%lf", &MASS_FLOW_RATE);
            //printf("Enter the value of Temperature in Kelvin: ");
            //scanf("%lf", &TEMP);
            //printf("Enter the value of Molecular Weight in kg/kmol: ");
            //scanf("%lf", &MOLEQ_WEIGHT);
            //printf("Enter the value of ratio of Secific heat Gamma: ");
            //scanf("%lf", &GAMMA);

            MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
            MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
            TEMP = Convert.ToDouble(txtTEMP.Text);
            MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
            GAMMA = Convert.ToDouble(txtGAMMA.Text);

            // Viscosity will be calculated based on Temperature and Molecular Weight

            VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
            // However, User can edit the Viscosity 
            //printf("Enter the value of Viscoaity in Pas: ");
            //scanf("%lf", &VISC);

            VISC = Convert.ToDouble(txtVISC.Text);

            //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

            //printf("Enter the value of Pipe Diameter D in m: ");
            //scanf("%lf", &PIPE_DIA);

            PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);

            PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);

            //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

            //printf("Enter the value of Downstream Pressure in kPa: ");
            //scanf("%lf", &PST0);

            PST0 = Convert.ToDouble(txtPST0.Text);

            PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

            // All input ends here

            UNIVERSAL_GAS_CONST = 8314;
            A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
            VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));

            SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
            MACH = VEL0 / SOUND_VEL;

            if (MACH >= 0.7)
            {
                MessageBox.Show("Flow is compressible please Edit the input parameters");
                return;
            }

            P_FRIC = 0.5 * DEN0 * VEL0 * VEL0 * K * FITTING_QUANTITY;

            C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
            C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
            C2 = MASS_FLOW_RATE / A0;
            PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
            DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
            DELP = (PST1 - PST0 * 1000);
            VEL1 = VEL0;
            PST1 = PST1 / 1000;

            ShowOutPut(PST1, DELP);
            //Following Vales are reqired to be print in File
            // return (PST1,DELP,DEN1); 
        }

        private void Mitre_Bend()
        {
            string cmbTypeT = Convert.ToString(cmbType.Text);
            if (cmbTypeT == "Mitre Bend Theta = 0 degree")
            {
                double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
                double VEL0, A0;
                double VISC;
                double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
                double ZETA, LENGTH_PIPE;
                double P_FRIC, C0, C1, C2;
                double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
                double FITTING_QUANTITY, LBYD = 2, K;
                double MAJOR_AXIS, MINOR_AXIS;
                // Input - Globle Variables: There should be button for the following globle inputs

                //printf("Enter the value of Mass flow rate in kg/s: ");
                //scanf("%lf", &MASS_FLOW_RATE);
                //printf("Enter the value of Temperature in Kelvin: ");
                //scanf("%lf", &TEMP);
                //printf("Enter the value of Molecular Weight in kg/kmol: ");
                //scanf("%lf", &MOLEQ_WEIGHT);
                //printf("Enter the value of ratio of Secific heat Gamma: ");
                //scanf("%lf", &GAMMA);

                MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
                MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
                TEMP = Convert.ToDouble(txtTEMP.Text);
                MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
                GAMMA = Convert.ToDouble(txtGAMMA.Text);

                // Viscosity will be calculated based on Temperature and Molecular Weight

                VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
                // However, User can edit the Viscosity 
                //printf("Enter the value of Viscoaity in Pas: ");
                //scanf("%lf", &VISC);

                VISC = Convert.ToDouble(txtVISC.Text);

                //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs
                //printf("Enter the value of Width of the Square Duct B in m: ");
                //scanf("%lf", &MAJOR_AXIS);
                //printf("Enter the value of Hight of the Square Duct H in m: ");
                //scanf("%lf", &MINOR_AXIS);

                MAJOR_AXIS = Convert.ToDouble(txtMAJOR_AXIS.Text);
                MINOR_AXIS = Convert.ToDouble(txtMINOR_AXIS.Text);
                //printf("Enter the value of Pipe Diameter in m: ");
                //scanf("%lf", &PIPE_DIA);
                //printf("Enter the Quantity: ");
                //scanf("%lf", &FITTING_QUANTITY);
                //printf("Enter the value of Roughness in m: ");
                //scanf("%lf", &ROUGHNESS);

                FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);
                ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

                ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);
                MAJOR_AXIS = ConvertMtoMMPerMeasure(MAJOR_AXIS, cmbMAJOR_AXIS);
                MINOR_AXIS = ConvertMtoMMPerMeasure(MINOR_AXIS, cmbMINOR_AXIS);
                //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

                //printf("Enter the value of Upsteam Pressure in kPa: ");
                //scanf("%lf", &PST0);

                PST0 = Convert.ToDouble(txtPST0.Text);
                PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

                // All input ends here
                // ofstream file6("Output_Data.txt");
                UNIVERSAL_GAS_CONST = 8314; PIPE_DIA = 2 * MAJOR_AXIS * MINOR_AXIS / (MAJOR_AXIS + MINOR_AXIS);
                A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
                SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
                MACH = VEL0 / SOUND_VEL;

                if (MACH >= 0.7)
                {
                    MessageBox.Show("Flow is compressible please Edit the input parameters");
                    return;
                }
                RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
                if (RE <= 2300)
                {
                    a1 = Math.Pow((RE / 2712), 8.4);
                    a = (1.0 / (1 + a1));
                    a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                    b1 = Math.Pow(a1, 1.8);
                    b = (1.0 / (1 + b1));
                    a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                    FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
                }
                else
                {
                    a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                    a3 = Math.Log(RE / (1.816 * a2));
                    b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                    FRIC_FACT = Math.Pow((-2 * b1), -2);
                }
                ZETA = FRIC_FACT * LBYD;
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

                C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A0;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                DELP = (PST1 - PST0 * 1000);
                PST1 = PST1 / 1000;

                ShowOutPut(PST1, DELP);
            }
            else if (cmbTypeT == "Mitre Bend Theta = 15 degree")
            {
                double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
                double VEL0, A0;
                double VISC;
                double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
                double ZETA, LENGTH_PIPE;
                double P_FRIC, C0, C1, C2;
                double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
                double FITTING_QUANTITY, LBYD = 4, K;
                double MAJOR_AXIS, MINOR_AXIS;
                // Input - Globle Variables: There should be button for the following globle inputs

                //printf("Enter the value of Mass flow rate in kg/s: ");
                //scanf("%lf", &MASS_FLOW_RATE);
                //printf("Enter the value of Temperature in Kelvin: ");
                //scanf("%lf", &TEMP);
                //printf("Enter the value of Molecular Weight in kg/kmol: ");
                //scanf("%lf", &MOLEQ_WEIGHT);
                //printf("Enter the value of ratio of Secific heat Gamma: ");
                //scanf("%lf", &GAMMA);

                MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
                MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
                TEMP = Convert.ToDouble(txtTEMP.Text);
                MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
                GAMMA = Convert.ToDouble(txtGAMMA.Text);

                // Viscosity will be calculated based on Temperature and Molecular Weight

                VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
                // However, User can edit the Viscosity 
                //printf("Enter the value of Viscoaity in Pas: ");
                //scanf("%lf", &VISC);

                VISC = Convert.ToDouble(txtVISC.Text);

                //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

                //printf("Enter the value of Width of the Square Duct B in m: ");
                //scanf("%lf", &MAJOR_AXIS);
                //printf("Enter the value of Hight of the Square Duct H in m: ");
                //scanf("%lf", &MINOR_AXIS);

                MAJOR_AXIS = Convert.ToDouble(txtMAJOR_AXIS.Text);
                MINOR_AXIS = Convert.ToDouble(txtMINOR_AXIS.Text);

                //printf("Enter the value of Pipe Diameter in m: ");
                //scanf("%lf", &PIPE_DIA);
                //printf("Enter the Quantity: ");
                //scanf("%lf", &FITTING_QUANTITY);
                //printf("Enter the value of Roughness in m: ");
                //scanf("%lf", &ROUGHNESS);

                FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);
                ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

                ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);
                MAJOR_AXIS = ConvertMtoMMPerMeasure(MAJOR_AXIS, cmbMAJOR_AXIS);
                MINOR_AXIS = ConvertMtoMMPerMeasure(MINOR_AXIS, cmbMINOR_AXIS);
                //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

                //printf("Enter the value of Upsteam Pressure in kPa: ");
                //scanf("%lf", &PST0);

                PST0 = Convert.ToDouble(txtPST0.Text);
                PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

                // All input ends here
                // ofstream file6("Output_Data.txt");
                UNIVERSAL_GAS_CONST = 8314; PIPE_DIA = 2 * MAJOR_AXIS * MINOR_AXIS / (MAJOR_AXIS + MINOR_AXIS);
                A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
                SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
                MACH = VEL0 / SOUND_VEL;

                if (MACH >= 0.7)
                {
                    MessageBox.Show("Flow is compressible please Edit the input parameters");
                    return;
                }
                RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
                if (RE <= 2300)
                {
                    a1 = Math.Pow((RE / 2712), 8.4);
                    a = (1.0 / (1 + a1));
                    a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                    b1 = Math.Pow(a1, 1.8);
                    b = (1.0 / (1 + b1));
                    a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                    FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
                }
                else
                {
                    a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                    a3 = Math.Log(RE / (1.816 * a2));
                    b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                    FRIC_FACT = Math.Pow((-2 * b1), -2);
                }
                ZETA = FRIC_FACT * LBYD;
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

                C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A0;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                DELP = (PST1 - PST0 * 1000);
                PST1 = PST1 / 1000;

                ShowOutPut(PST1, DELP);
            }
            else if (cmbTypeT == "Mitre Bend Theta = 30 degree")
            {
                double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
                double VEL0, A0;
                double VISC;
                double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
                double ZETA, LENGTH_PIPE;
                double P_FRIC, C0, C1, C2;
                double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
                double FITTING_QUANTITY, LBYD = 8, K;
                double MAJOR_AXIS, MINOR_AXIS;
                // Input - Globle Variables: There should be button for the following globle inputs

                //printf("Enter the value of Mass flow rate in kg/s: ");
                //scanf("%lf", &MASS_FLOW_RATE);
                //printf("Enter the value of Temperature in Kelvin: ");
                //scanf("%lf", &TEMP);
                //printf("Enter the value of Molecular Weight in kg/kmol: ");
                //scanf("%lf", &MOLEQ_WEIGHT);
                //printf("Enter the value of ratio of Secific heat Gamma: ");
                //scanf("%lf", &GAMMA);

                MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
                MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
                TEMP = Convert.ToDouble(txtTEMP.Text);
                MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
                GAMMA = Convert.ToDouble(txtGAMMA.Text);

                // Viscosity will be calculated based on Temperature and Molecular Weight

                VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
                // However, User can edit the Viscosity 
                //printf("Enter the value of Viscoaity in Pas: ");
                //scanf("%lf", &VISC);

                VISC = Convert.ToDouble(txtVISC.Text);

                //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

                //printf("Enter the value of Width of the Square Duct B in m: ");
                //scanf("%lf", &MAJOR_AXIS);
                //printf("Enter the value of Hight of the Square Duct H in m: ");
                //scanf("%lf", &MINOR_AXIS);

                MAJOR_AXIS = Convert.ToDouble(txtMAJOR_AXIS.Text);
                MINOR_AXIS = Convert.ToDouble(txtMINOR_AXIS.Text);

                //printf("Enter the value of Pipe Diameter in m: ");
                //scanf("%lf", &PIPE_DIA);
                //printf("Enter the Quantity: ");
                //scanf("%lf", &FITTING_QUANTITY);
                //printf("Enter the value of Roughness in m: ");
                //scanf("%lf", &ROUGHNESS);

                FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);
                ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

                ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);

                //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

                //printf("Enter the value of Upsteam Pressure in kPa: ");
                //scanf("%lf", &PST0);

                PST0 = Convert.ToDouble(txtPST0.Text);
                PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

                // All input ends here
                // ofstream file6("Output_Data.txt");
                UNIVERSAL_GAS_CONST = 8314; PIPE_DIA = 2 * MAJOR_AXIS * MINOR_AXIS / (MAJOR_AXIS + MINOR_AXIS);
                A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
                SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
                MACH = VEL0 / SOUND_VEL;

                if (MACH >= 0.7)
                {
                    MessageBox.Show("Flow is compressible please Edit the input parameters");
                    return;
                }
                RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
                if (RE <= 2300)
                {
                    a1 = Math.Pow((RE / 2712), 8.4);
                    a = (1.0 / (1 + a1));
                    a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                    b1 = Math.Pow(a1, 1.8);
                    b = (1.0 / (1 + b1));
                    a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                    FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
                }
                else
                {
                    a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                    a3 = Math.Log(RE / (1.816 * a2));
                    b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                    FRIC_FACT = Math.Pow((-2 * b1), -2);
                }
                ZETA = FRIC_FACT * LBYD;
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

                C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A0;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                DELP = (PST1 - PST0 * 1000);
                PST1 = PST1 / 1000;

                ShowOutPut(PST1, DELP);
            }
            else if (cmbTypeT == "Mitre Bend Theta = 45 degree")
            {
                double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
                double VEL0, A0;
                double VISC;
                double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
                double ZETA, LENGTH_PIPE;
                double P_FRIC, C0, C1, C2;
                double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
                double FITTING_QUANTITY, LBYD = 15, K;
                double MAJOR_AXIS, MINOR_AXIS;
                // Input - Globle Variables: There should be button for the following globle inputs

                //printf("Enter the value of Mass flow rate in kg/s: ");
                //scanf("%lf", &MASS_FLOW_RATE);
                //printf("Enter the value of Temperature in Kelvin: ");
                //scanf("%lf", &TEMP);
                //printf("Enter the value of Molecular Weight in kg/kmol: ");
                //scanf("%lf", &MOLEQ_WEIGHT);
                //printf("Enter the value of ratio of Secific heat Gamma: ");
                //scanf("%lf", &GAMMA);

                MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
                MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
                TEMP = Convert.ToDouble(txtTEMP.Text);
                MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
                GAMMA = Convert.ToDouble(txtGAMMA.Text);


                // Viscosity will be calculated based on Temperature and Molecular Weight

                VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
                // However, User can edit the Viscosity 
                //printf("Enter the value of Viscoaity in Pas: ");
                //scanf("%lf", &VISC);

                VISC = Convert.ToDouble(txtVISC.Text);

                //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

                //printf("Enter the value of Width of the Square Duct B in m: ");
                //scanf("%lf", &MAJOR_AXIS);
                //printf("Enter the value of Hight of the Square Duct H in m: ");
                //scanf("%lf", &MINOR_AXIS);

                MAJOR_AXIS = Convert.ToDouble(txtMAJOR_AXIS.Text);
                MINOR_AXIS = Convert.ToDouble(txtMINOR_AXIS.Text);

                //printf("Enter the value of Pipe Diameter in m: ");
                //scanf("%lf", &PIPE_DIA);
                //printf("Enter the Quantity: ");
                //scanf("%lf", &FITTING_QUANTITY);
                //printf("Enter the value of Roughness in m: ");
                //scanf("%lf", &ROUGHNESS);

                FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);
                ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

                ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);
                MAJOR_AXIS = ConvertMtoMMPerMeasure(MAJOR_AXIS, cmbMAJOR_AXIS);
                MINOR_AXIS = ConvertMtoMMPerMeasure(MINOR_AXIS, cmbMINOR_AXIS);

                //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

                //printf("Enter the value of Upsteam Pressure in kPa: ");
                //scanf("%lf", &PST0);

                PST0 = Convert.ToDouble(txtPST0.Text);
                PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

                // All input ends here
                // ofstream file6("Output_Data.txt");
                UNIVERSAL_GAS_CONST = 8314; PIPE_DIA = 2 * MAJOR_AXIS * MINOR_AXIS / (MAJOR_AXIS + MINOR_AXIS);
                A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
                SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
                MACH = VEL0 / SOUND_VEL;

                if (MACH >= 0.7)
                {
                    MessageBox.Show("Flow is compressible please Edit the input parameters");
                    return;
                }
                RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
                if (RE <= 2300)
                {
                    a1 = Math.Pow((RE / 2712), 8.4);
                    a = (1.0 / (1 + a1));
                    a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                    b1 = Math.Pow(a1, 1.8);
                    b = (1.0 / (1 + b1));
                    a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                    FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
                }
                else
                {
                    a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                    a3 = Math.Log(RE / (1.816 * a2));
                    b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                    FRIC_FACT = Math.Pow((-2 * b1), -2);
                }
                ZETA = FRIC_FACT * LBYD;
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

                C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A0;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                DELP = (PST1 - PST0 * 1000);
                PST1 = PST1 / 1000;

                ShowOutPut(PST1, DELP);
            }
            else if (cmbTypeT == "Mitre Bend Theta = 60 degree")
            {
                double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
                double VEL0, A0;
                double VISC;
                double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
                double ZETA, LENGTH_PIPE;
                double P_FRIC, C0, C1, C2;
                double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
                double FITTING_QUANTITY, LBYD = 25, K;
                double MAJOR_AXIS, MINOR_AXIS;
                // Input - Globle Variables: There should be button for the following globle inputs

                //printf("Enter the value of Mass flow rate in kg/s: ");
                //scanf("%lf", &MASS_FLOW_RATE);
                //printf("Enter the value of Temperature in Kelvin: ");
                //scanf("%lf", &TEMP);
                //printf("Enter the value of Molecular Weight in kg/kmol: ");
                //scanf("%lf", &MOLEQ_WEIGHT);
                //printf("Enter the value of ratio of Secific heat Gamma: ");
                //scanf("%lf", &GAMMA);

                MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
                MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
                TEMP = Convert.ToDouble(txtTEMP.Text);
                MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
                GAMMA = Convert.ToDouble(txtGAMMA.Text);

                // Viscosity will be calculated based on Temperature and Molecular Weight

                VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
                // However, User can edit the Viscosity 
                //printf("Enter the value of Viscoaity in Pas: ");
                //scanf("%lf", &VISC);

                VISC = Convert.ToDouble(txtVISC.Text);

                //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

                //printf("Enter the value of Width of the Square Duct B in m: ");
                //scanf("%lf", &MAJOR_AXIS);
                //printf("Enter the value of Hight of the Square Duct H in m: ");
                //scanf("%lf", &MINOR_AXIS);


                MAJOR_AXIS = Convert.ToDouble(txtMAJOR_AXIS.Text);
                MINOR_AXIS = Convert.ToDouble(txtMINOR_AXIS.Text);
                //printf("Enter the value of Pipe Diameter in m: ");
                //scanf("%lf", &PIPE_DIA);
                //printf("Enter the Quantity: ");
                //scanf("%lf", &FITTING_QUANTITY);
                //printf("Enter the value of Roughness in m: ");
                //scanf("%lf", &ROUGHNESS);

                FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);
                ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);


                ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);
                MAJOR_AXIS = ConvertMtoMMPerMeasure(MAJOR_AXIS, cmbMAJOR_AXIS);
                MINOR_AXIS = ConvertMtoMMPerMeasure(MINOR_AXIS, cmbMINOR_AXIS);

                //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

                //printf("Enter the value of Upsteam Pressure in kPa: ");
                //scanf("%lf", &PST0);

                PST0 = Convert.ToDouble(txtPST0.Text);
                PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

                // All input ends here
                // ofstream file6("Output_Data.txt");
                UNIVERSAL_GAS_CONST = 8314; PIPE_DIA = 2 * MAJOR_AXIS * MINOR_AXIS / (MAJOR_AXIS + MINOR_AXIS);
                A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
                SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
                MACH = VEL0 / SOUND_VEL;

                if (MACH >= 0.7)
                {
                    MessageBox.Show("Flow is compressible please Edit the input parameters");
                    return;
                }
                RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
                if (RE <= 2300)
                {
                    a1 = Math.Pow((RE / 2712), 8.4);
                    a = (1.0 / (1 + a1));
                    a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                    b1 = Math.Pow(a1, 1.8);
                    b = (1.0 / (1 + b1));
                    a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                    FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
                }
                else
                {
                    a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                    a3 = Math.Log(RE / (1.816 * a2));
                    b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                    FRIC_FACT = Math.Pow((-2 * b1), -2);
                }
                ZETA = FRIC_FACT * LBYD;
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

                C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A0;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                DELP = (PST1 - PST0 * 1000);
                PST1 = PST1 / 1000;

                ShowOutPut(PST1, DELP);
            }
            else if (cmbTypeT == "Mitre Bend Theta = 75 degree")
            {
                double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
                double VEL0, A0;
                double VISC;
                double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
                double ZETA, LENGTH_PIPE;
                double P_FRIC, C0, C1, C2;
                double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
                double FITTING_QUANTITY, LBYD = 40, K;
                double MAJOR_AXIS, MINOR_AXIS;
                // Input - Globle Variables: There should be button for the following globle inputs

                //printf("Enter the value of Mass flow rate in kg/s: ");
                //scanf("%lf", &MASS_FLOW_RATE);
                //printf("Enter the value of Temperature in Kelvin: ");
                //scanf("%lf", &TEMP);
                //printf("Enter the value of Molecular Weight in kg/kmol: ");
                //scanf("%lf", &MOLEQ_WEIGHT);
                //printf("Enter the value of ratio of Secific heat Gamma: ");
                //scanf("%lf", &GAMMA);

                MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
                MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
                TEMP = Convert.ToDouble(txtTEMP.Text);
                MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
                GAMMA = Convert.ToDouble(txtGAMMA.Text);

                // Viscosity will be calculated based on Temperature and Molecular Weight

                VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
                // However, User can edit the Viscosity 
                //printf("Enter the value of Viscoaity in Pas: ");
                //scanf("%lf", &VISC);

                VISC = Convert.ToDouble(txtVISC.Text);

                //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

                //printf("Enter the value of Width of the Square Duct B in m: ");
                //scanf("%lf", &MAJOR_AXIS);
                //printf("Enter the value of Hight of the Square Duct H in m: ");
                //scanf("%lf", &MINOR_AXIS);

                MAJOR_AXIS = Convert.ToDouble(txtMAJOR_AXIS.Text);
                MINOR_AXIS = Convert.ToDouble(txtMINOR_AXIS.Text);

                //printf("Enter the value of Pipe Diameter in m: ");
                //scanf("%lf", &PIPE_DIA);
                //printf("Enter the Quantity: ");
                //scanf("%lf", &FITTING_QUANTITY);
                //printf("Enter the value of Roughness in m: ");
                //scanf("%lf", &ROUGHNESS);

                FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);
                ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);


                ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);
                MAJOR_AXIS = ConvertMtoMMPerMeasure(MAJOR_AXIS, cmbMAJOR_AXIS);
                MINOR_AXIS = ConvertMtoMMPerMeasure(MINOR_AXIS, cmbMINOR_AXIS);

                //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

                //printf("Enter the value of Upsteam Pressure in kPa: ");
                //scanf("%lf", &PST0);

                PST0 = Convert.ToDouble(txtPST0.Text);
                PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

                // All input ends here
                // ofstream file6("Output_Data.txt");
                UNIVERSAL_GAS_CONST = 8314; PIPE_DIA = 2 * MAJOR_AXIS * MINOR_AXIS / (MAJOR_AXIS + MINOR_AXIS);
                A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
                SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
                MACH = VEL0 / SOUND_VEL;

                if (MACH >= 0.7)
                {
                    MessageBox.Show("Flow is compressible please Edit the input parameters");
                    return;
                }
                RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
                if (RE <= 2300)
                {
                    a1 = Math.Pow((RE / 2712), 8.4);
                    a = (1.0 / (1 + a1));
                    a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                    b1 = Math.Pow(a1, 1.8);
                    b = (1.0 / (1 + b1));
                    a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                    FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
                }
                else
                {
                    a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                    a3 = Math.Log(RE / (1.816 * a2));
                    b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                    FRIC_FACT = Math.Pow((-2 * b1), -2);
                }
                ZETA = FRIC_FACT * LBYD;
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

                C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A0;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                DELP = (PST1 - PST0 * 1000);
                PST1 = PST1 / 1000;

                ShowOutPut(PST1, DELP);

            }
            else if (cmbTypeT == "Mitre Bend Theta = 90 degree")
            {
                double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
                double VEL0, A0;
                double VISC;
                double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
                double ZETA, LENGTH_PIPE;
                double P_FRIC, C0, C1, C2;
                double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
                double FITTING_QUANTITY, LBYD = 60, K;
                double MAJOR_AXIS, MINOR_AXIS;
                // Input - Globle Variables: There should be button for the following globle inputs

                //printf("Enter the value of Mass flow rate in kg/s: ");
                //scanf("%lf", &MASS_FLOW_RATE);
                //printf("Enter the value of Temperature in Kelvin: ");
                //scanf("%lf", &TEMP);
                //printf("Enter the value of Molecular Weight in kg/kmol: ");
                //scanf("%lf", &MOLEQ_WEIGHT);
                //printf("Enter the value of ratio of Secific heat Gamma: ");
                //scanf("%lf", &GAMMA);

                MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
                MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
                TEMP = Convert.ToDouble(txtTEMP.Text);
                MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
                GAMMA = Convert.ToDouble(txtGAMMA.Text);

                // Viscosity will be calculated based on Temperature and Molecular Weight

                VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
                // However, User can edit the Viscosity 
                //printf("Enter the value of Viscoaity in Pas: ");
                //scanf("%lf", &VISC);

                VISC = Convert.ToDouble(txtVISC.Text);

                //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

                //printf("Enter the value of Width of the Square Duct B in m: ");
                //scanf("%lf", &MAJOR_AXIS);
                //printf("Enter the value of Hight of the Square Duct H in m: ");
                //scanf("%lf", &MINOR_AXIS);

                MAJOR_AXIS = Convert.ToDouble(txtMAJOR_AXIS.Text);
                MINOR_AXIS = Convert.ToDouble(txtMINOR_AXIS.Text);

                //printf("Enter the value of Pipe Diameter in m: ");
                //scanf("%lf", &PIPE_DIA);
                //printf("Enter the Quantity: ");
                //scanf("%lf", &FITTING_QUANTITY);
                //printf("Enter the value of Roughness in m: ");
                //scanf("%lf", &ROUGHNESS);

                FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);
                ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

                ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);
                MAJOR_AXIS = ConvertMtoMMPerMeasure(MAJOR_AXIS, cmbMAJOR_AXIS);
                MINOR_AXIS = ConvertMtoMMPerMeasure(MINOR_AXIS, cmbMINOR_AXIS);

                //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

                //printf("Enter the value of Upsteam Pressure in kPa: ");
                //scanf("%lf", &PST0);

                PST0 = Convert.ToDouble(txtPST0.Text);
                PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

                // All input ends here
                // ofstream file6("Output_Data.txt");
                UNIVERSAL_GAS_CONST = 8314; PIPE_DIA = 2 * MAJOR_AXIS * MINOR_AXIS / (MAJOR_AXIS + MINOR_AXIS);
                A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
                SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
                MACH = VEL0 / SOUND_VEL;

                if (MACH >= 0.7)
                {
                    MessageBox.Show("Flow is compressible please Edit the input parameters");
                    return;
                }
                RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
                if (RE <= 2300)
                {
                    a1 = Math.Pow((RE / 2712), 8.4);
                    a = (1.0 / (1 + a1));
                    a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                    b1 = Math.Pow(a1, 1.8);
                    b = (1.0 / (1 + b1));
                    a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                    FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
                }
                else
                {
                    a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                    a3 = Math.Log(RE / (1.816 * a2));
                    b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                    FRIC_FACT = Math.Pow((-2 * b1), -2);
                }
                ZETA = FRIC_FACT * LBYD;
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

                C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A0;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                DELP = (PST1 - PST0 * 1000);
                PST1 = PST1 / 1000;

                ShowOutPut(PST1, DELP);
            }
        }

        private void Annular_Section()
        {
            double PIPE_DIA, PIPE_DIA1, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
            double VEL0, A0, A1, VEL1, DELA;
            double VISC;
            double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
            double ZETA, LENGTH_PIPE;
            double P_FRIC, C0, C1, C2;
            double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
            double D_EQ;
            // Input - Globle Variables: There should be button for the following globle inputs

            //printf("Enter the value of Mass flow rate in kg/s: ");
            //scanf("%lf", &MASS_FLOW_RATE);
            //printf("Enter the value of Temperature in Kelvin: ");
            //scanf("%lf", &TEMP);
            //printf("Enter the value of Molecular Weight in kg/kmol: ");
            //scanf("%lf", &MOLEQ_WEIGHT);
            //printf("Enter the value of ratio of Secific heat Gamma: ");
            //scanf("%lf", &GAMMA);

            MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
            MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
            TEMP = Convert.ToDouble(txtTEMP.Text);
            MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
            GAMMA = Convert.ToDouble(txtGAMMA.Text);

            // Viscosity will be calculated based on Temperature and Molecular Weight

            VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
            // However, User can edit the Viscosity 
            //printf("Enter the value of Viscoaity in Pas: ");
            //scanf("%lf", &VISC);

            VISC = Convert.ToDouble(txtVISC.Text);

            //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

            //printf("Enter the value of inner Pipe Diameter Di in m: ");
            //scanf("%lf", &PIPE_DIA);

            PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);
            PIPE_DIA1 = Convert.ToDouble(txtPIPE_DIA1.Text);

            //printf("Enter the value of Outer Pipe Diameter D in m: ");
            //scanf("%lf", &PIPE_DIA1);

            PIPE_DIA1 = ConvertMtoMMPerMeasure(PIPE_DIA1, cmbPIPE_DIA1);
            PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);

            //printf("Enter the value of Length of Pipe L in m: ");
            //scanf("%lf", &LENGTH_PIPE);
            //printf("Enter the value of Roughness in m: ");
            //scanf("%lf", &ROUGHNESS);

            LENGTH_PIPE = Convert.ToDouble(txtLENGTH_PIPE.Text);
            ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

            ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);
            //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

            //printf("Enter the value of Downsream Pressure in kPa: ");
            //scanf("%lf", &PST0);

            PST0 = Convert.ToDouble(txtPST0.Text);
            PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

            // All input ends here

            UNIVERSAL_GAS_CONST = 8314;
            A0 = (PI * PIPE_DIA * PIPE_DIA / 4); A1 = (PI * PIPE_DIA1 * PIPE_DIA1 / 4);
            DELA = A1 - A0;
            D_EQ = 4 * DELA / (PI * (PIPE_DIA1 + PIPE_DIA));

            DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
            VEL0 = (MASS_FLOW_RATE / (DELA * DEN0));
            SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
            MACH = VEL0 / SOUND_VEL;

            if (MACH >= 0.7)
            {
                MessageBox.Show("Flow is compressible please Edit the input parameters");
                return;
            }
            RE = (DEN0 * VEL0 * D_EQ) / VISC;
            NONDIM_ROUGHNESS = ROUGHNESS / D_EQ;
            if (RE <= 2300)
            {
                a1 = Math.Pow((RE / 2712), 8.4);
                a = (1.0 / (1 + a1));
                a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                b1 = Math.Pow(a1, 1.8);
                b = (1.0 / (1 + b1));
                a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
            }
            else
            {
                a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                a3 = Math.Log(RE / (1.816 * a2));
                b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                FRIC_FACT = Math.Pow((-2 * b1), -2);
            }
            //ZETA = FRIC_FACT*LENGTH_PIPE/PIPE_DIA;
            ZETA = FRIC_FACT * LENGTH_PIPE / D_EQ;
            //ZETA = 0.5*(1-(A0/A1));
            P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0;

            C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
            C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
            C2 = MASS_FLOW_RATE / DELA;
            PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
            DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
            VEL1 = (MASS_FLOW_RATE / (A1 * DEN1));
            DELP = (PST1 - PST0 * 1000);
            PST1 = PST1 / 1000;

            ShowOutPut(PST1, DELP);
        }

        private void Rectangular_Duct()
        {
            double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
            double VEL0, A0;
            double VISC;
            double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
            double ZETA, LENGTH_PIPE;
            double P_FRIC, C0, C1, C2;
            double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
            double MAJOR_AXIS, MINOR_AXIS; // Major and Minor Axis of Non-Circular Duct
                                           // Input - Globle Variables: There should be button for the following globle inputs

            //printf("Enter the value of Mass flow rate in kg/s: ");
            //scanf("%lf", &MASS_FLOW_RATE);
            //printf("Enter the value of Temperature in Kelvin: ");
            //scanf("%lf", &TEMP);
            //printf("Enter the value of Molecular Weight in kg/kmol: ");
            //scanf("%lf", &MOLEQ_WEIGHT);
            //printf("Enter the value of ratio of Secific heat Gamma: ");
            //scanf("%lf", &GAMMA);

            MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
            MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
            TEMP = Convert.ToDouble(txtTEMP.Text);
            MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
            GAMMA = Convert.ToDouble(txtGAMMA.Text);

            // Viscosity will be calculated based on Temperature and Molecular Weight

            VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
            // However, User can edit the Viscosity 
            //printf("Enter the value of Viscoaity in Pas: ");
            //scanf("%lf", &VISC);

            VISC = Convert.ToDouble(txtVISC.Text);

            //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

            //printf("Enter the value of Pipe Diameter in m: ");
            //scanf("%lf", &PIPE_DIA);
            //printf("Enter the value of Width of the Square Duct B in m: ");
            //scanf("%lf", &MAJOR_AXIS);
            //printf("Enter the value of Hight of the Square Duct H in m: ");
            //scanf("%lf", &MINOR_AXIS);

            MAJOR_AXIS = Convert.ToDouble(txtMAJOR_AXIS.Text);
            MINOR_AXIS = Convert.ToDouble(txtMINOR_AXIS.Text);
            //printf("Enter the value of Length of Pipe L in m: ");
            //scanf("%lf", &LENGTH_PIPE);
            //printf("Enter the value of Roughness in m: ");
            //scanf("%lf", &ROUGHNESS);

            ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);
            LENGTH_PIPE = Convert.ToDouble(txtLENGTH_PIPE.Text);


            ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);
            MAJOR_AXIS = ConvertMtoMMPerMeasure(MAJOR_AXIS, cmbMAJOR_AXIS);
            MINOR_AXIS = ConvertMtoMMPerMeasure(MINOR_AXIS, cmbMINOR_AXIS);

            //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

            //printf("Enter the value of Downsream Pressure in kPa: ");
            //scanf("%lf", &PST0);

            PST0 = Convert.ToDouble(txtPST0.Text);
            PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

            // All input ends here

            PIPE_DIA = 2 * MAJOR_AXIS * MINOR_AXIS / (MAJOR_AXIS + MINOR_AXIS);
            UNIVERSAL_GAS_CONST = 8314;
            //A0 = (PI*PIPE_DIA*PIPE_DIA/4); 
            A0 = MAJOR_AXIS * MINOR_AXIS;
            DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
            VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
            SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
            MACH = VEL0 / SOUND_VEL;

            if (MACH >= 0.7)
            {
                MessageBox.Show("Flow is compressible please Edit the input parameters");
                return;
            }
            RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
            NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
            if (RE <= 2300)
            {
                a1 = Math.Pow((RE / 2712), 8.4);
                a = (1.0 / (1 + a1));
                a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                b1 = Math.Pow(a1, 1.8);
                b = (1.0 / (1 + b1));
                a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
            }
            else
            {
                a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                a3 = Math.Log(RE / (1.816 * a2));
                b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                FRIC_FACT = Math.Pow((-2 * b1), -2);
            }
            ZETA = FRIC_FACT * LENGTH_PIPE / PIPE_DIA;
            P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0;

            C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
            C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
            C2 = MASS_FLOW_RATE / A0;
            PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
            DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
            DELP = (PST1 - PST0 * 1000);
            PST1 = PST1 / 1000;

            ShowOutPut(PST1, DELP);
        }

        private void Rectangular_Elbow()
        {
            double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
            double VEL0, A0;
            double VISC;
            double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
            double ZETA, ZETA90, LENGTH_PIPE;
            double P_FRIC, C0, C1, C2;
            double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
            double FITTING_QUANTITY, RbyD, THETA, LBYD, K;
            double MAJOR_AXIS, MINOR_AXIS;
            // Input - Globle Variables: There should be button for the following globle inputs

            //printf("Enter the value of Mass flow rate in kg/s: ");
            //scanf("%lf", &MASS_FLOW_RATE);
            //printf("Enter the value of Temperature in Kelvin: ");
            //scanf("%lf", &TEMP);
            //printf("Enter the value of Molecular Weight in kg/kmol: ");
            //scanf("%lf", &MOLEQ_WEIGHT);
            //printf("Enter the value of ratio of Secific heat Gamma: ");
            //scanf("%lf", &GAMMA);

            MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
            MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
            TEMP = Convert.ToDouble(txtTEMP.Text);
            MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
            GAMMA = Convert.ToDouble(txtGAMMA.Text);

            // Viscosity will be calculated based on Temperature and Molecular Weight

            VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
            // However, User can edit the Viscosity 
            //printf("Enter the value of Viscoaity in Pas: ");
            //scanf("%lf", &VISC);

            VISC = Convert.ToDouble(txtVISC.Text);

            //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

            //printf("Enter the value of Pipe Diameter in m: ");
            //scanf("%lf", &PIPE_DIA);
            //printf("Enter the value of Width of the Square Duct B in m: ");
            //scanf("%lf", &MAJOR_AXIS);
            //printf("Enter the value of Hight of the Square Duct H in m: ");
            //scanf("%lf", &MINOR_AXIS);

            MAJOR_AXIS = Convert.ToDouble(txtMAJOR_AXIS.Text);
            MINOR_AXIS = Convert.ToDouble(txtMINOR_AXIS.Text);

            //printf("Enter the Quantity: ");
            //scanf("%lf", &FITTING_QUANTITY);
            //printf("Enter the value of Roughness in m: ");
            //scanf("%lf", &ROUGHNESS);
            //printf("Enter the value of R by D: ");
            //scanf("%lf", &RbyD);
            //printf("Enter the value of Angle of Bend W in Degree: ");
            //scanf("%lf", &THETA);

            RbyD = Convert.ToDouble(txtRbyD.Text);
            THETA = Convert.ToDouble(txtTHETA.Text);

            FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);
            ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

            ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);
            MAJOR_AXIS = ConvertMtoMMPerMeasure(MAJOR_AXIS, cmbMAJOR_AXIS);
            MINOR_AXIS = ConvertMtoMMPerMeasure(MINOR_AXIS, cmbMINOR_AXIS);
            //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

            //printf("Enter the value of Upsteam Pressure in kPa: ");
            //scanf("%lf", &PST0);

            PST0 = Convert.ToDouble(txtPST0.Text);
            PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

            // All input ends here
            //ofstream file6("Output_Data.txt");
            PIPE_DIA = 2 * MAJOR_AXIS * MINOR_AXIS / (MAJOR_AXIS + MINOR_AXIS);
            UNIVERSAL_GAS_CONST = 8314;
            A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
            VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
            SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
            MACH = VEL0 / SOUND_VEL;

            if (MACH >= 0.7)
            {
                MessageBox.Show("Flow is compressible please Edit the input parameters");
                return;
            }
            RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
            NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
            if (RE <= 2300)
            {
                a1 = Math.Pow((RE / 2712), 8.4);
                a = (1.0 / (1 + a1));
                a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                b1 = Math.Pow(a1, 1.8);
                b = (1.0 / (1 + b1));
                a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
            }
            else
            {
                a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                a3 = Math.Log(RE / (1.816 * a2));
                b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                FRIC_FACT = Math.Pow((-2 * b1), -2);
            }
            LBYD = 51.51 - 66.914 * Math.Log(RbyD + 1) + 32.338 * Math.Log(RbyD + 1) * Math.Log(RbyD + 1) - 3.473 * Math.Log(RbyD + 1) * Math.Log(RbyD + 1) * Math.Log(RbyD + 1);
            ZETA90 = FRIC_FACT * LBYD;
            ZETA = (((THETA / 90) - 1) * ((0.25 * PI * FRIC_FACT * RbyD) + (0.5 * ZETA90))) + ZETA90;
            P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

            C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
            C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
            C2 = MASS_FLOW_RATE / A0;
            PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
            DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
            DELP = (PST1 - PST0 * 1000);
            PST1 = PST1 / 1000;

            ShowOutPut(PST1, DELP);
        }

        private void T_Thru()
        {
            double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
            double VEL0, A0;
            double VISC;
            double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
            double ZETA, LENGTH_PIPE;
            double P_FRIC, C0, C1, C2;
            double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
            double FITTING_QUANTITY, LBYD = 20, K;
            // Input - Globle Variables: There should be button for the following globle inputs

            //printf("Enter the value of Mass flow rate in kg/s: ");
            //scanf("%lf", &MASS_FLOW_RATE);
            //printf("Enter the value of Temperature in Kelvin: ");
            //scanf("%lf", &TEMP);
            //printf("Enter the value of Molecular Weight in kg/kmol: ");
            //scanf("%lf", &MOLEQ_WEIGHT);
            //printf("Enter the value of ratio of Secific heat Gamma: ");
            //scanf("%lf", &GAMMA);

            MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
            MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
            TEMP = Convert.ToDouble(txtTEMP.Text);
            MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
            GAMMA = Convert.ToDouble(txtGAMMA.Text);

            // Viscosity will be calculated based on Temperature and Molecular Weight

            VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
            // However, User can edit the Viscosity 
            //printf("Enter the value of Viscoaity in Pas: ");
            //scanf("%lf", &VISC);

            VISC = Convert.ToDouble(txtVISC.Text);


            //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

            //printf("Enter the value of Pipe Diameter D in m: ");
            //scanf("%lf", &PIPE_DIA);
            //printf("Enter the Quantity: ");
            //scanf("%lf", &FITTING_QUANTITY);
            //printf("Enter the value of Roughness in m: ");
            //scanf("%lf", &ROUGHNESS);

            PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);
            FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);
            ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);


            PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
            ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);

            //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

            //printf("Enter the value of Upsteam Pressure in kPa: ");
            //scanf("%lf", &PST0);

            PST0 = Convert.ToDouble(txtPST0.Text);
            PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

            // All input ends here
            // ofstream file6("Output_Data.txt");
            UNIVERSAL_GAS_CONST = 8314;
            A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
            VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
            SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
            MACH = VEL0 / SOUND_VEL;

            if (MACH >= 0.7)
            {
                MessageBox.Show("Flow is compressible please Edit the input parameters");
                return;
            }
            RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
            NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
            if (RE <= 2300)
            {
                a1 = Math.Pow((RE / 2712), 8.4);
                a = (1.0 / (1 + a1));
                a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                b1 = Math.Pow(a1, 1.8);
                b = (1.0 / (1 + b1));
                a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
            }
            else
            {
                a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                a3 = Math.Log(RE / (1.816 * a2));
                b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                FRIC_FACT = Math.Pow((-2 * b1), -2);
            }
            ZETA = FRIC_FACT * LBYD;
            P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

            C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
            C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
            C2 = MASS_FLOW_RATE / A0;
            PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
            DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
            DELP = (PST1 - PST0 * 1000);
            PST1 = PST1 / 1000;

            ShowOutPut(PST1, DELP);
        }

        private void Orifice()
        {
            double PIPE_DIA, PIPE_DIA1, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
            double VEL0, A0, A1, VEL1;
            double VISC;
            double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
            double ZETA, LENGTH_PIPE;
            double P_FRIC, C0, C1, C2;
            double a1, a2, a3, b1;
            double BETA, CDinf, CD, Y, GAMMA, SOUND_VEL, MACH;
            // Input - Globle Variables: There should be button for the following globle inputs

            MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
            TEMP = Convert.ToDouble(txtTEMP.Text);
            MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
            GAMMA = Convert.ToDouble(txtGAMMA.Text);
            // Viscosity will be calculated based on Temperature and Molecular Weight

            VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
            // However, User can edit the Viscosity 
            VISC = Convert.ToDouble(txtVISC.Text);

            //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

            //printf("Enter the value of Pipe Diameter D1 in m: ");
            PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);
            PIPE_DIA1 = Convert.ToDouble(txtPIPE_DIA1.Text);

            //printf("Enter the value of Orifice Diameter D2 in m: ");
            //scanf("%lf", &PIPE_DIA1);

            //printf("Enter the value of Length of Pipe in m: ");
            //scanf("%lf", &LENGTH_PIPE);
            //printf("Enter the value of Roughness in m: ");
            //scanf("%lf", &ROUGHNESS);

            PIPE_DIA1 = ConvertMtoMMPerMeasure(PIPE_DIA1, cmbPIPE_DIA1);
            PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);

            //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

            //printf("Enter the value of Downsream Pressure in kPa: ");
            PST0 = Convert.ToDouble(txtPST0.Text);
            PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

            // All input ends here
            //ofstream file6("Output_Data.txt");
            UNIVERSAL_GAS_CONST = 8314;
            A0 = (PI * PIPE_DIA * PIPE_DIA / 4); A1 = (PI * PIPE_DIA1 * PIPE_DIA1 / 4);
            DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
            //DEN0 = 1.2;
            VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));

            SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
            MACH = VEL0 / SOUND_VEL;

            if (MACH >= 0.7)
            {
                MessageBox.Show("Flow is compressible please Edit the input parameters");
                return;
            }

            RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
            BETA = PIPE_DIA1 / PIPE_DIA;
            //CDinf = 0.5959+0.0312*Math.Pow(BETA,2.1)-0.184*Math.Pow(BETA,6)+(0.039*Math.Pow(BETA,4)/(1-Math.Pow(BETA,4)))-0.0158*Math.Pow(BETA,3);
            //b = 91.71*Math.Pow(BETA,2.5);
            //CD = CDinf + b/Math.Pow(RE,0.75);
            CD = 0.5961 + 0.0261 * Math.Pow(BETA, 2) - 0.216 * Math.Pow(BETA, 8) + 0.000521 * Math.Pow((10e6 * BETA / RE), 0.7) + (0.0188 + 0.0063 * Math.Pow((19000 * BETA / RE), 0.8)) * Math.Pow(BETA, 3.5) * Math.Pow((10e6 / RE), 0.3);
            CD = CD / Math.Sqrt(1 - Math.Pow(BETA, 4));
            a = Math.Pow((2.0 / (GAMMA + 1)), (GAMMA / (GAMMA - 1)));
            //Y = 1-(0.5/GAMMA)*(0.41+0.35*Math.Pow(BETA,4));
            Y = 1 - (1 - Math.Pow(a, (1.0 / GAMMA))) * (0.351 + 0.256 * Math.Pow(BETA, 4) + 0.93 * Math.Pow(BETA, 8));
            //Y = 1.0;
            DELP = (MASS_FLOW_RATE / (Y * CD * A1)) * (MASS_FLOW_RATE / (Y * CD * A1)) / (2 * DEN0);
            DELP = DELP * (1 - Math.Pow(BETA, 1.9));

            PST1 = DELP + PST0 * 1000;
            PST1 = PST1 / 1000;

            ShowOutPut(PST1, DELP);
        }

        private void Flare_Tip()
        {
            string cmbTypeT = Convert.ToString(cmbType.Text);
            if (cmbTypeT == "Flare Tip Contant Bore")
            {
                double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
                double VEL0, A0;
                double VISC, PST0_DOWNSTREAM;
                double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
                double ZETA, LENGTH_PIPE, VEL1;
                double P_FRIC, C0, C1, C2;
                double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
                double FITTING_QUANTITY = 1, LBYD, K = 1.0;
                // Input - Globle Variables: There should be button for the following globle inputs

                MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
                MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
                TEMP = Convert.ToDouble(txtTEMP.Text);
                MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
                GAMMA = Convert.ToDouble(txtGAMMA.Text);

                // Viscosity will be calculated based on Temperature and Molecular Weight

                VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
                // However, User can edit the Viscosity 
                //printf("Enter the value of Viscoaity in Pas: ");
                VISC = Convert.ToDouble(txtVISC.Text);

                //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

                //printf("Enter the value of Pipe Diameter D in m: ");
                PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);

                //printf("Enter the value of Length of Flare Tip L in m: ");
                LENGTH_PIPE = Convert.ToDouble(txtLENGTH_PIPE.Text);

                //printf("Enter the value of Roughness in m: ");
                ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);
                //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

                PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
                ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);
                LENGTH_PIPE = ConvertMtoMMPerMeasure(LENGTH_PIPE, cmbLENGTH_PIPE);

                //printf("Enter the value of Downstream Pressure in kPa: ");
                PST0 = Convert.ToDouble(txtPST0.Text);
                PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

                // All input ends here
                PST0 = PST0 * 1000;
                PST0_DOWNSTREAM = PST0;
                UNIVERSAL_GAS_CONST = 8314;
                A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));

                SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
                MACH = VEL0 / SOUND_VEL;

                if (MACH >= 0.7)
                {
                    MessageBox.Show("Flow is compressible please Edit the input parameters");
                    return;
                }
                //Pipe Exit STarts Here
                P_FRIC = 0.5 * DEN0 * VEL0 * VEL0 * K * FITTING_QUANTITY;

                C0 = PST0 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A0;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL1 = (MASS_FLOW_RATE / (A0 * DEN1));
                DELP = (PST1 - PST0);

                //Pipe Exit Ends Here

                //Straight Pipe Start Here
                VEL0 = VEL1; DEN0 = DEN1; PST0 = PST1;
                //DELP = (PST1 - PST0*1000);
                RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
                if (RE <= 2300)
                {
                    a1 = Math.Pow((RE / 2712), 8.4);
                    a = (1.0 / (1 + a1));
                    a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                    b1 = Math.Pow(a1, 1.8);
                    b = (1.0 / (1 + b1));
                    a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                    FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
                }
                else
                {
                    a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                    a3 = Math.Log(RE / (1.816 * a2));
                    b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                    FRIC_FACT = Math.Pow((-2 * b1), -2);
                }
                ZETA = FRIC_FACT * LENGTH_PIPE / PIPE_DIA;
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0;

                C0 = PST0 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A0;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL1 = (MASS_FLOW_RATE / (A0 * DEN1));
                //Straight Pipe End Here
                DELP = (PST1 - PST0_DOWNSTREAM);
                PST1 = PST1 / 1000;

                ShowOutPut(PST1, DELP);
            }
            else if (cmbTypeT == "Flare Tip Standard Bore")
            {
                double PIPE_DIA, PIPE_DIA1, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
                double VEL0, A0, A1;
                double VISC, PST0_DOWNSTREAM;
                double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
                double ZETA, LENGTH_PIPE, LENGTH_PIPE1, VEL1;
                double P_FRIC, C0, C1, C2;
                double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
                double FITTING_QUANTITY = 1, LBYD, K = 1.0;
                // Input - Globle Variables: There should be button for the following globle inputs

                //printf("Enter the value of Mass flow rate in kg/s: ");
                //scanf("%lf", &MASS_FLOW_RATE);
                //printf("Enter the value of Temperature in Kelvin: ");
                //scanf("%lf", &TEMP);
                //printf("Enter the value of Molecular Weight in kg/kmol: ");
                //scanf("%lf", &MOLEQ_WEIGHT);
                //printf("Enter the value of ratio of Secific heat Gamma: ");
                //scanf("%lf", &GAMMA);

                MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
                MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
                TEMP = Convert.ToDouble(txtTEMP.Text);
                MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
                GAMMA = Convert.ToDouble(txtGAMMA.Text);

                // Viscosity will be calculated based on Temperature and Molecular Weight

                VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
                // However, User can edit the Viscosity 
                //printf("Enter the value of Viscoaity in Pas: ");
                //scanf("%lf", &VISC);

                VISC = Convert.ToDouble(txtVISC.Text);

                //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs
                //printf("Enter the value of Refractory Diameter dr in m: ");
                //scanf("%lf", &PIPE_DIA);

                //printf("Enter the value of Pipe Diameter D in m: ");
                //scanf("%lf", &PIPE_DIA1);

                PIPE_DIA1 = Convert.ToDouble(txtPIPE_DIA1.Text);

                //printf("Enter the value of Refractory Length Lr in m: ");
                //scanf("%lf", &LENGTH_PIPE);

                //printf("Enter the value of Pipe Length L in m: ");
                //scanf("%lf", &LENGTH_PIPE1);

                LENGTH_PIPE1 = Convert.ToDouble(txtLENGTH_PIPE1.Text);

                //printf("Enter the value of Roughness in m: ");
                //scanf("%lf", &ROUGHNESS);

                PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);
                LENGTH_PIPE = Convert.ToDouble(txtLENGTH_PIPE.Text);
                ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

                PIPE_DIA1 = ConvertMtoMMPerMeasure(PIPE_DIA1, cmbPIPE_DIA1);
                PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
                ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);
                LENGTH_PIPE = ConvertMtoMMPerMeasure(LENGTH_PIPE, cmbLENGTH_PIPE);
                LENGTH_PIPE1 = ConvertMtoMMPerMeasure(LENGTH_PIPE1, cmbLENGTH_PIPE1);

                //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

                //printf("Enter the value of Downstream Pressure in kPa: ");
                //scanf("%lf", &PST0);

                PST0 = Convert.ToDouble(txtPST0.Text);
                PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

                // All input ends here
                PST0 = PST0 * 1000;
                PST0_DOWNSTREAM = PST0;
                UNIVERSAL_GAS_CONST = 8314;
                A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL0 = (MASS_FLOW_RATE / (A0 * DEN0)); A1 = (PI * PIPE_DIA1 * PIPE_DIA1 / 4);

                SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
                MACH = VEL0 / SOUND_VEL;

                if (MACH >= 0.7)
                {
                    MessageBox.Show("Flow is compressible please Edit the input parameters");
                    return;
                }
                //Pipe Exit STarts Here
                P_FRIC = 0.5 * DEN0 * VEL0 * VEL0 * K * FITTING_QUANTITY;

                C0 = PST0 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A0;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL1 = (MASS_FLOW_RATE / (A0 * DEN1));
                //DELP = (PST1 - PST0*1000);

                //Pipe Exit Ends Here

                //Straight Pipe Start Here
                VEL0 = VEL1; DEN0 = DEN1; PST0 = PST1;
                //DELP = (PST1 - PST0*1000);
                RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
                if (RE <= 2300)
                {
                    a1 = Math.Pow((RE / 2712), 8.4);
                    a = (1.0 / (1 + a1));
                    a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                    b1 = Math.Pow(a1, 1.8);
                    b = (1.0 / (1 + b1));
                    a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                    FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
                }
                else
                {
                    a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                    a3 = Math.Log(RE / (1.816 * a2));
                    b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                    FRIC_FACT = Math.Pow((-2 * b1), -2);
                }
                ZETA = FRIC_FACT * LENGTH_PIPE / PIPE_DIA;
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0;

                C0 = PST0 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A0;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL1 = (MASS_FLOW_RATE / (A0 * DEN1));
                //Straight Pipe End Here

                // Sudden Contraction STart here
                VEL0 = VEL1; DEN0 = DEN1; PST0 = PST1;
                ZETA = 0.5 * (1 - (A0 / A1));
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0;

                C0 = PST0 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A1;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL1 = (MASS_FLOW_RATE / (A1 * DEN1));
                //DELP = (PST1 - PST0*1000);
                // Sudden Contraction End here

                //Straight Pipe Start Here
                VEL0 = VEL1; DEN0 = DEN1; PST0 = PST1;
                //DELP = (PST1 - PST0*1000);
                RE = (DEN0 * VEL0 * PIPE_DIA1) / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA1;
                if (RE <= 2300)
                {
                    a1 = Math.Pow((RE / 2712), 8.4);
                    a = (1.0 / (1 + a1));
                    a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                    b1 = Math.Pow(a1, 1.8);
                    b = (1.0 / (1 + b1));
                    a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                    FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
                }
                else
                {
                    a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                    a3 = Math.Log(RE / (1.816 * a2));
                    b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                    FRIC_FACT = Math.Pow((-2 * b1), -2);
                }
                ZETA = FRIC_FACT * LENGTH_PIPE1 / PIPE_DIA1;
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0;

                C0 = PST0 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A1;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL1 = (MASS_FLOW_RATE / (A1 * DEN1));
                //Straight Pipe End Here

                DELP = (PST1 - PST0_DOWNSTREAM);
                PST1 = PST1 / 1000;

                ShowOutPut(PST1, DELP);
            }
        }

        private void Velocity_Seal()
        {
            string cmbTypeT = Convert.ToString(cmbType.Text);
            if (cmbTypeT == "Seal Dia = 50% of Pipe Dia")
            {
                double PIPE_DIA, PIPE_DIA1, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
                double VEL0, A0, A1, VEL1;
                double VISC, PST0_DOWNSTREAM;
                double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
                double ZETA, LENGTH_PIPE, LENGTH_CONT, D_EQ;
                double P_FRIC, C0, C1, C2;
                double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
                // Input - Globle Variables: There should be button for the following globle inputs

                //printf("Enter the value of Mass flow rate in kg/s: ");
                //scanf("%lf", &MASS_FLOW_RATE);
                //printf("Enter the value of Temperature in Kelvin: ");
                //scanf("%lf", &TEMP);
                //printf("Enter the value of Molecular Weight in kg/kmol: ");
                //scanf("%lf", &MOLEQ_WEIGHT);
                //printf("Enter the value of ratio of Secific heat Gamma: ");
                //scanf("%lf", &GAMMA);

                MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
                MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
                TEMP = Convert.ToDouble(txtTEMP.Text);
                MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
                GAMMA = Convert.ToDouble(txtGAMMA.Text);

                // Viscosity will be calculated based on Temperature and Molecular Weight

                VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
                // However, User can edit the Viscosity 
                //printf("Enter the value of Viscoaity in Pas: ");
                //scanf("%lf", &VISC);

                VISC = Convert.ToDouble(txtVISC.Text);

                //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

                //printf("Enter the value of Pipe Diameter D in m: ");
                //scanf("%lf", &PIPE_DIA);

                PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);

                //printf("Enter the value of Upstream (Smaller) Pipe Diameter in m: ");
                //scanf("%lf", &PIPE_DIA1);
                //printf("Enter the value of Length of Pipe in m: ");
                //scanf("%lf", &LENGTH_PIPE);
                //printf("Enter the value of Roughness in m: ");
                //scanf("%lf", &ROUGHNESS);

                ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

                PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
                ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);

                //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

                //printf("Enter the value of Downsream Pressure in kPa: ");
                //scanf("%lf", &PST0);

                PST0 = Convert.ToDouble(txtPST0.Text);
                PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

                // All input ends here
                PST0 = PST0 * 1000;
                PST0_DOWNSTREAM = PST0;
                PIPE_DIA1 = Math.Sqrt(0.5 * PIPE_DIA * PIPE_DIA);
                LENGTH_PIPE = 1.4142 * PIPE_DIA1;
                LENGTH_CONT = 0.5 * 1.7320 * (PIPE_DIA - PIPE_DIA1);
                UNIVERSAL_GAS_CONST = 8314;
                A0 = (PI * PIPE_DIA * PIPE_DIA / 4); A1 = (PI * PIPE_DIA1 * PIPE_DIA1 / 4);
                DEN0 = (PST0 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
                SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
                MACH = VEL0 / SOUND_VEL;

                if (MACH >= 0.7)
                {
                    MessageBox.Show("Flow is compressible please Edit the input parameters");
                    return;
                }
                //Sudden Expansion Start Here
                ZETA = (1 - (A1 / A0)) * (1 - (A1 / A0)) / ((A1 / A0) * (A1 / A0));

                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0;

                C0 = PST0 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A1;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL1 = (MASS_FLOW_RATE / (A1 * DEN1));
                //Sudden Expansion End Here

                //Straight Pipe Start Here
                VEL0 = VEL1; DEN0 = DEN1; PST0 = PST1;
                //DELP = (PST1 - PST0*1000);
                RE = (DEN0 * VEL0 * PIPE_DIA1) / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA1;
                if (RE <= 2300)
                {
                    a1 = Math.Pow((RE / 2712), 8.4);
                    a = (1.0 / (1 + a1));
                    a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                    b1 = Math.Pow(a1, 1.8);
                    b = (1.0 / (1 + b1));
                    a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                    FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
                }
                else
                {
                    a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                    a3 = Math.Log(RE / (1.816 * a2));
                    b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                    FRIC_FACT = Math.Pow((-2 * b1), -2);
                }
                ZETA = FRIC_FACT * LENGTH_PIPE / PIPE_DIA1;
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0;

                C0 = PST0 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A1;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL1 = (MASS_FLOW_RATE / (A1 * DEN1));
                //Straight Pipe End Here

                //Gradaul Contraction Start Here
                VEL0 = VEL1; DEN0 = DEN1; PST0 = PST1;
                //DELP = (PST1 - PST0*1000);
                RE = (DEN0 * VEL0 * PIPE_DIA1) / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA1;
                if (RE <= 2300)
                {
                    a1 = Math.Pow((RE / 2712), 8.4);
                    a = (1.0 / (1 + a1));
                    a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                    b1 = Math.Pow(a1, 1.8);
                    b = (1.0 / (1 + b1));
                    a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                    FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
                }
                else
                {
                    a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                    a3 = Math.Log(RE / (1.816 * a2));
                    b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                    FRIC_FACT = Math.Pow((-2 * b1), -2);
                }

                D_EQ = 3 * PIPE_DIA1 * PIPE_DIA1 * PIPE_DIA1 / (PIPE_DIA * PIPE_DIA + PIPE_DIA1 * PIPE_DIA + PIPE_DIA1 * PIPE_DIA1);
                ZETA = FRIC_FACT * LENGTH_CONT / D_EQ;
                //ZETA = 0.5*(1-(A0/A1));
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0;

                C0 = PST0 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A0;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL1 = (MASS_FLOW_RATE / (A1 * DEN1));
                DELP = (PST1 - PST0_DOWNSTREAM);
                PST1 = PST1 / 1000;

                ShowOutPut(PST1, DELP);
            }
            else if (cmbTypeT == "Seal Dia = 75% of Pipe Dia")
            {
                double PIPE_DIA, PIPE_DIA1, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
                double VEL0, A0, A1, VEL1;
                double VISC, PST0_DOWNSTREAM;
                double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
                double ZETA, LENGTH_PIPE, LENGTH_CONT, D_EQ;
                double P_FRIC, C0, C1, C2;
                double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
                // Input - Globle Variables: There should be button for the following globle inputs

                //printf("Enter the value of Mass flow rate in kg/s: ");
                //scanf("%lf", &MASS_FLOW_RATE);
                //printf("Enter the value of Temperature in Kelvin: ");
                //scanf("%lf", &TEMP);
                //printf("Enter the value of Molecular Weight in kg/kmol: ");
                //scanf("%lf", &MOLEQ_WEIGHT);
                //printf("Enter the value of ratio of Secific heat Gamma: ");
                //scanf("%lf", &GAMMA);

                MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
                MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
                TEMP = Convert.ToDouble(txtTEMP.Text);
                MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
                GAMMA = Convert.ToDouble(txtGAMMA.Text);

                // Viscosity will be calculated based on Temperature and Molecular Weight

                VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
                // However, User can edit the Viscosity 
                //printf("Enter the value of Viscoaity in Pas: ");
                //scanf("%lf", &VISC);
                VISC = Convert.ToDouble(txtVISC.Text);

                //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

                //printf("Enter the value of Pipe Diameter D in m: ");
                //scanf("%lf", &PIPE_DIA);

                PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);

                //printf("Enter the value of Upstream (Smaller) Pipe Diameter in m: ");
                //scanf("%lf", &PIPE_DIA1);
                //printf("Enter the value of Length of Pipe in m: ");
                //scanf("%lf", &LENGTH_PIPE);
                //printf("Enter the value of Roughness in m: ");
                //scanf("%lf", &ROUGHNESS);

                ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

                PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
                ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);

                //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

                //printf("Enter the value of Downsream Pressure in kPa: ");
                //scanf("%lf", &PST0);

                PST0 = Convert.ToDouble(txtPST0.Text);
                PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

                // All input ends here
                PST0 = PST0 * 1000;
                PST0_DOWNSTREAM = PST0;

                PIPE_DIA1 = Math.Sqrt(0.75 * PIPE_DIA * PIPE_DIA);
                LENGTH_PIPE = 1.4142 * PIPE_DIA1;
                LENGTH_CONT = 0.5 * 1.7320 * (PIPE_DIA - PIPE_DIA1);
                UNIVERSAL_GAS_CONST = 8314;
                A0 = (PI * PIPE_DIA * PIPE_DIA / 4); A1 = (PI * PIPE_DIA1 * PIPE_DIA1 / 4);
                DEN0 = (PST0 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
                SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
                MACH = VEL0 / SOUND_VEL;

                if (MACH >= 0.7)
                {
                    MessageBox.Show("Flow is compressible please Edit the input parameters");
                    return;
                }
                //Sudden Expansion Start Here
                ZETA = (1 - (A1 / A0)) * (1 - (A1 / A0)) / ((A1 / A0) * (A1 / A0));

                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0;

                C0 = PST0 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A1;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL1 = (MASS_FLOW_RATE / (A1 * DEN1));
                //Sudden Expansion End Here

                //Straight Pipe Start Here
                VEL0 = VEL1; DEN0 = DEN1; PST0 = PST1;
                //DELP = (PST1 - PST0*1000);
                RE = (DEN0 * VEL0 * PIPE_DIA1) / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA1;
                if (RE <= 2300)
                {
                    a1 = Math.Pow((RE / 2712), 8.4);
                    a = (1.0 / (1 + a1));
                    a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                    b1 = Math.Pow(a1, 1.8);
                    b = (1.0 / (1 + b1));
                    a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                    FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
                }
                else
                {
                    a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                    a3 = Math.Log(RE / (1.816 * a2));
                    b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                    FRIC_FACT = Math.Pow((-2 * b1), -2);
                }
                ZETA = FRIC_FACT * LENGTH_PIPE / PIPE_DIA1;
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0;

                C0 = PST0 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A1;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL1 = (MASS_FLOW_RATE / (A1 * DEN1));
                //Straight Pipe End Here

                //Gradaul Contraction Start Here
                VEL0 = VEL1; DEN0 = DEN1; PST0 = PST1;
                //DELP = (PST1 - PST0*1000);
                RE = (DEN0 * VEL0 * PIPE_DIA1) / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA1;
                if (RE <= 2300)
                {
                    a1 = Math.Pow((RE / 2712), 8.4);
                    a = (1.0 / (1 + a1));
                    a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                    b1 = Math.Pow(a1, 1.8);
                    b = (1.0 / (1 + b1));
                    a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                    FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
                }
                else
                {
                    a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                    a3 = Math.Log(RE / (1.816 * a2));
                    b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                    FRIC_FACT = Math.Pow((-2 * b1), -2);
                }

                D_EQ = 3 * PIPE_DIA1 * PIPE_DIA1 * PIPE_DIA1 / (PIPE_DIA * PIPE_DIA + PIPE_DIA1 * PIPE_DIA + PIPE_DIA1 * PIPE_DIA1);
                ZETA = FRIC_FACT * LENGTH_CONT / D_EQ;
                //ZETA = 0.5*(1-(A0/A1));
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0;

                C0 = PST0 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A0;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL1 = (MASS_FLOW_RATE / (A1 * DEN1));
                DELP = (PST1 - PST0_DOWNSTREAM * 1000);
                PST1 = PST1 / 1000;

                ShowOutPut(PST1, DELP);
            }
        }

        private void Molecular_Seal()
        {
            double MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, DEN0, PST0_DOWNSTREAM, DEN1;
            double VEL0, VEL1, FLOW_AREA, FRIC_FACT, TOTAL_LENGTH, PST1;
            double L1, L2, L3, D1, D2, D3, ENTRY_LENGTH, EXIT_LENGTH, CONE_LENGTH, DOME_LEVEL, EQUI_LENGTH_SUDDEN_EXPANSION;
            double RE, EQUI_LENGTH_SUDDEN_COTRACTIO, EQUI_LENGTH_GRADUAL_CONTRACTION;
            double VISC, GAMMA, SOUND_VEL, MACH;
            double ROUGHNESS;
            double ZETA, P_FRIC_SECTION1, P_FRIC_SECTION2, P_FRIC_SECTION3, P_FRIC_SECTION4, P_FRIC_SECTION5, P_FRIC_SECTION6, TOTAL_P_FRIC;
            double C0, C1, C2, PIPE_DIA, PIPE_DIA1, D_EQ, A0, A1, DELA, DELP;
            double DELP1, DELP2, DELP3, DELP4, DELP5, DELP6, DELP7, DELP8, DELP9, DELP10, DELP11, DELP12, DELP13;
            double P_FRIC_SECTION7, P_FRIC_SECTION8, P_FRIC_SECTION9, P_FRIC_SECTION10, P_FRIC_SECTION11, P_FRIC_SECTION12, P_FRIC_SECTION13;

            // Input - Globle Variables: There should be button for the following globle inputs

            //printf("Enter the value of Mass flow rate in kg/s: ");
            //scanf("%lf", &MASS_FLOW_RATE);
            //printf("Enter the value of Temperature in Kelvin: ");
            //scanf("%lf", &TEMP);
            //printf("Enter the value of Molecular Weight in kg/kmol: ");
            //scanf("%lf", &MOLEQ_WEIGHT);
            //printf("Enter the value of ratio of Secific heat Gamma: ");
            //scanf("%lf", &GAMMA);

            MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
            MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
            TEMP = Convert.ToDouble(txtTEMP.Text);
            MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
            GAMMA = Convert.ToDouble(txtGAMMA.Text);


            // Viscosity will be calculated based on Temperature and Molecular Weight

            VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
            // However, User can edit the Viscosity 
            //printf("Enter the value of Viscoaity in Pas: ");
            //scanf("%lf", &VISC);
            VISC = Convert.ToDouble(txtVISC.Text);


            //Globle Variable ends here

            //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs
            //printf("Enter the value of Roughness in m: ");
            //scanf("%lf", &ROUGHNESS);
            ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);
            ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);
            D1 = 4.76; D2 = 3.7; D3 = 2.134;
            L1 = 1.35; L2 = 1.06; L3 = 1.22;

            if (dgvMolGrid.SelectedRows.Count != 1)
            {
                MessageBox.Show("Select one row from grid to calculate.");
                return;
            }
            else {
                int selectedrowindex = dgvMolGrid.SelectedCells[0].RowIndex;
                DataGridViewRow selectedRow = dgvMolGrid.Rows[selectedrowindex];
                D1 = Convert.ToDouble(selectedRow.Cells[1].Value);
                D2 = Convert.ToDouble(selectedRow.Cells[2].Value);
                D3 = Convert.ToDouble(selectedRow.Cells[3].Value);
                L1 = Convert.ToDouble(selectedRow.Cells[4].Value);
                L2 = Convert.ToDouble(selectedRow.Cells[5].Value);
                L3 = Convert.ToDouble(selectedRow.Cells[6].Value);
            }
            //Specific Variable ends here 

            //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

            //printf("Enter the value of Downsream Pressure in kPa: ");
            //scanf("%lf", &PST0);
            PST0 = Convert.ToDouble(txtPST0.Text);
            PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

            PST0 = PST0 * 1000;
            PST0_DOWNSTREAM = PST0;

            UNIVERSAL_GAS_CONST = 8314; ENTRY_LENGTH = 0.250; EXIT_LENGTH = 0.250; DOME_LEVEL = 0.915;
            // The code start here


            CONE_LENGTH = ((D1 - D3) / 1.15);
            //EQUI_LENGTH_SUDDEN_EXPANSION = 25;
            //EQUI_LENGTH_SUDDEN_COTRACTIO = 25;
            //EQUI_LENGTH_GRADUAL_CONTRACTION = 25;
            //ofstream file6("Output_Data.txt");
            DEN0 = (PST0 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));

            //CALCULATIONS FOR SECTION 1 (STraight Pipe) STARTS HERE
            FLOW_AREA = PI * D3 * D3 / 4; VEL0 = MASS_FLOW_RATE / (DEN0 * FLOW_AREA);
            SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
            MACH = VEL0 / SOUND_VEL;

            if (MACH >= 0.7)
            {
                MessageBox.Show("Flow is compressible please Edit the input parameters");
                return;
            }
            NONDIM_ROUGHNESS = ROUGHNESS / D3;
            RE = DEN0 * VEL0 * D3 / VISC;
            FRIC_FACT = CALC_FRIC_FACT(RE);
            //TOTAL_LENGTH = ((DOME_LEVEL+ENTRY_LENGTH+EXIT_LENGTH+CONE_LENGTH+L1)/D3)+EQUI_LENGTH_SUDDEN_EXPANSION;
            ZETA = FRIC_FACT * ENTRY_LENGTH / D3;
            P_FRIC_SECTION1 = 0.5 * ZETA * DEN0 * VEL0 * VEL0;
            C0 = PST0 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC_SECTION1;
            C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
            C2 = MASS_FLOW_RATE / FLOW_AREA;
            PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
            DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
            VEL1 = (MASS_FLOW_RATE / (FLOW_AREA * DEN1));
            DELP1 = (PST1 - PST0);
            //file6 << "P_Drop_SECTION1" << "\t" << DELP1 << endl;
            //CALCULATIONS FOR SECTION 1 ENDS HERE

            //CALCULATIONS FOR SECTION 2 (Gradual Contraction) STARTS HERE
            VEL0 = VEL1; DEN0 = DEN1; PST0 = PST1;
            //FLOW_AREA = PI*(D2*D2-D3*D3)/4; VEL0 = MASS_FLOW_RATE/(DEN0*FLOW_AREA);
            FLOW_AREA = PI * D1 * D1 / 4;
            NONDIM_ROUGHNESS = ROUGHNESS / D1;
            RE = DEN0 * VEL0 * D3 / VISC;
            FRIC_FACT = CALC_FRIC_FACT(RE);
            //TOTAL_LENGTH = ((DOME_LEVEL+L2)/(D2-D3))+EQUI_LENGTH_SUDDEN_EXPANSION;
            PIPE_DIA = D3; PIPE_DIA1 = D1;
            D_EQ = 3 * PIPE_DIA1 * PIPE_DIA1 * PIPE_DIA1 / (PIPE_DIA * PIPE_DIA + PIPE_DIA1 * PIPE_DIA + PIPE_DIA1 * PIPE_DIA1);
            ZETA = FRIC_FACT * CONE_LENGTH / D_EQ;
            P_FRIC_SECTION2 = 0.5 * ZETA * DEN0 * VEL0 * VEL0;

            C0 = PST0 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC_SECTION2;
            C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
            C2 = MASS_FLOW_RATE / FLOW_AREA;
            PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
            DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
            VEL1 = (MASS_FLOW_RATE / (FLOW_AREA * DEN1));
            DELP2 = (PST1 - PST0);
            //file6 << "P_Drop_SECTION2" << "\t" << DELP2 << endl;
            //CALCULATIONS FOR SECTION 2 ENDS HERE

            //CALCULATIONS FOR SECTION 3 (Straight Pipe) STARTS HERE
            VEL0 = VEL1; DEN0 = DEN1; PST0 = PST1;
            FLOW_AREA = PI * D1 * D1 / 4;
            NONDIM_ROUGHNESS = ROUGHNESS / D1;
            RE = DEN0 * VEL0 * D1 / VISC;
            FRIC_FACT = CALC_FRIC_FACT(RE);
            ZETA = FRIC_FACT * L3 / D1;
            P_FRIC_SECTION3 = 0.5 * ZETA * DEN0 * VEL0 * VEL0;

            C0 = PST0 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC_SECTION3;
            C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
            C2 = MASS_FLOW_RATE / FLOW_AREA;
            PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
            DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
            VEL1 = (MASS_FLOW_RATE / (FLOW_AREA * DEN1));
            DELP3 = (PST1 - PST0);
            //file6 << "P_Drop_SECTION3" << "\t" << DELP3 << endl;
            //CALCULATIONS FOR SECTION 3 ENDS HERE

            //CALCULATIONS FOR SECTION 4 (Sudden Expansion) STARTS HERE
            VEL0 = VEL1; DEN0 = DEN1; PST0 = PST1;
            PIPE_DIA = D1; PIPE_DIA1 = D1 - D2;
            A0 = (PI * D1 * D1 / 4); A1 = (PI * (D1 * D1 - D2 * D2) / 4);
            ZETA = (1 - (A1 / A0)) * (1 - (A1 / A0)) / ((A1 / A0) * (A1 / A0));
            P_FRIC_SECTION4 = 0.5 * ZETA * DEN0 * VEL0 * VEL0;
            C0 = PST0 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC_SECTION4;
            C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
            C2 = MASS_FLOW_RATE / A1;
            PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
            DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
            VEL1 = (MASS_FLOW_RATE / (A1 * DEN1));
            DELP4 = (PST1 - PST0);
            //DELP4 = Math.Sqrt((PST1 - PST0)*(PST1 - PST0));
            //file6 << "P_Drop_SECTION4" << "\t" << DELP4 << endl;
            //CALCULATIONS FOR SECTION 4 ENDS HERE


            //CALCULATIONS FOR SECTION 5 (Annular Section) STARTS HERE
            VEL0 = VEL1; DEN0 = DEN1; PST0 = PST1;
            PIPE_DIA = D2; PIPE_DIA1 = D1;
            A0 = (PI * PIPE_DIA * PIPE_DIA / 4); A1 = (PI * PIPE_DIA1 * PIPE_DIA1 / 4);
            DELA = A1 - A0;
            D_EQ = 4 * DELA / (PI * (PIPE_DIA1 + PIPE_DIA));
            FLOW_AREA = DELA; VEL0 = MASS_FLOW_RATE / (DEN0 * FLOW_AREA);
            RE = DEN0 * VEL0 * D_EQ / VISC;
            NONDIM_ROUGHNESS = ROUGHNESS / D_EQ;
            FRIC_FACT = CALC_FRIC_FACT(RE);
            //TOTAL_LENGTH = (L3/D1);
            ZETA = FRIC_FACT * (L2 + 0.915) / D_EQ;
            P_FRIC_SECTION5 = 0.5 * ZETA * DEN0 * VEL0 * VEL0;
            C0 = PST0 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC_SECTION5;
            C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
            C2 = MASS_FLOW_RATE / DELA;
            PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
            DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
            VEL1 = (MASS_FLOW_RATE / (DELA * DEN1));
            DELP5 = (PST1 - PST0);
            //file6 << "P_Drop_SECTION5" << "\t" << DELP5 << endl;
            //CALCULATIONS FOR SECTION 5 ENDS HERE


            //CALCULATIONS FOR SECTION 6 (Sudden Contraction) STARTS HERE
            VEL0 = VEL1; DEN0 = DEN1; PST0 = PST1;
            PIPE_DIA = D1 - D2; PIPE_DIA1 = D1 - D3;
            A0 = (PI * (D1 * D1 - D2 * D2) / 4); A1 = (PI * (D1 * D1 - D3 * D3) / 4);
            ZETA = 0.5 * (1 - (A0 / A1));
            P_FRIC_SECTION6 = 0.5 * ZETA * DEN0 * VEL0 * VEL0;

            C0 = PST0 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC_SECTION6;
            C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
            C2 = MASS_FLOW_RATE / A1;
            PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
            DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
            VEL1 = (MASS_FLOW_RATE / (A1 * DEN1));
            DELP6 = (PST1 - PST0);
            //file6 << "P_Drop_SECTION6" << "\t" << DELP6 << endl;
            //CALCULATIONS FOR SECTION 6 ENDS HERE

            //CALCULATIONS FOR SECTION 7 (Annular Section) STARTS HERE
            VEL0 = VEL1; DEN0 = DEN1; PST0 = PST1;
            PIPE_DIA = D3; PIPE_DIA1 = D1;
            A0 = (PI * PIPE_DIA * PIPE_DIA / 4); A1 = (PI * PIPE_DIA1 * PIPE_DIA1 / 4);
            DELA = A1 - A0;
            D_EQ = 4 * DELA / (PI * (PIPE_DIA1 + PIPE_DIA));
            FLOW_AREA = DELA; VEL0 = MASS_FLOW_RATE / (DEN0 * FLOW_AREA);
            RE = DEN0 * VEL0 * D_EQ / VISC;
            NONDIM_ROUGHNESS = ROUGHNESS / D_EQ;
            FRIC_FACT = CALC_FRIC_FACT(RE);
            //TOTAL_LENGTH = (L3/D1);
            ZETA = FRIC_FACT * L1 / D_EQ;
            P_FRIC_SECTION7 = 0.5 * ZETA * DEN0 * VEL0 * VEL0;
            C0 = PST0 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC_SECTION7;
            C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
            C2 = MASS_FLOW_RATE / DELA;
            PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
            DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
            VEL1 = (MASS_FLOW_RATE / (DELA * DEN1));
            DELP7 = (PST1 - PST0);
            //file6 << "P_Drop_SECTION7" << "\t" << DELP7 << endl;
            //CALCULATIONS FOR SECTION 7 ENDS HERE

            //CALCULATIONS FOR SECTION 8 (Sudden Expansion) STARTS HERE
            VEL0 = VEL1; DEN0 = DEN1; PST0 = PST1;
            PIPE_DIA = D1 - D3; PIPE_DIA1 = D2 - D3;
            //PIPE_DIA = D1-D2;
            A0 = (PI * (D1 * D1 - D3 * D3) / 4); A1 = (PI * (D2 * D2 - D3 * D3) / 4);
            //A0 = (PI*(D1*D1-D2*D2)/4);

            /*RE = DEN0*VEL0*PIPE_DIA/VISC;
            FRIC_FACT = CALC_FRIC_FACT(RE);
            ZETA = FRIC_FACT*50;*/

            ZETA = (1 - (A1 / A0)) * (1 - (A1 / A0)) / ((A1 / A0) * (A1 / A0));

            P_FRIC_SECTION8 = 0.5 * ZETA * DEN0 * VEL0 * VEL0;
            C0 = PST0 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC_SECTION8;
            C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
            C2 = MASS_FLOW_RATE / A1;
            PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
            DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
            VEL1 = (MASS_FLOW_RATE / (A1 * DEN1));
            DELP8 = (PST1 - PST0);
            //DELP8 = Math.Sqrt((PST1 - PST0)*(PST1 - PST0));
            //file6 << "P_Drop_SECTION8" << "\t" << DELP8 << endl;
            //CALCULATIONS FOR SECTION 8 ENDS HERE

            //CALCULATIONS FOR SECTION 9 (Annular Section) STARTS HERE
            VEL0 = VEL1; DEN0 = DEN1; PST0 = PST1;
            PIPE_DIA = D3; PIPE_DIA1 = D2;
            A0 = (PI * PIPE_DIA * PIPE_DIA / 4); A1 = (PI * PIPE_DIA1 * PIPE_DIA1 / 4);
            DELA = A1 - A0;
            D_EQ = 4 * DELA / (PI * (PIPE_DIA1 + PIPE_DIA));
            FLOW_AREA = DELA; VEL0 = MASS_FLOW_RATE / (DEN0 * FLOW_AREA);
            RE = DEN0 * VEL0 * D_EQ / VISC;
            NONDIM_ROUGHNESS = ROUGHNESS / D_EQ;
            FRIC_FACT = CALC_FRIC_FACT(RE);
            //TOTAL_LENGTH = (L3/D1);
            ZETA = FRIC_FACT * 0.915 / D_EQ;
            P_FRIC_SECTION9 = 0.5 * ZETA * DEN0 * VEL0 * VEL0;
            C0 = PST0 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC_SECTION9;
            C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
            C2 = MASS_FLOW_RATE / DELA;
            PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
            DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
            VEL1 = (MASS_FLOW_RATE / (DELA * DEN1));
            DELP9 = (PST1 - PST0);
            //file6 << "P_Drop_SECTION9" << "\t" << DELP9 << endl;
            //CALCULATIONS FOR SECTION 9 ENDS HERE

            //CALCULATIONS FOR SECTION 10 (Sudden Contraction) STARTS HERE
            VEL0 = VEL1; DEN0 = DEN1; PST0 = PST1;
            PIPE_DIA = D2 - D3; PIPE_DIA1 = D2;
            A0 = (PI * (D2 * D2 - D3 * D3) / 4); A1 = (PI * (D2 * D2) / 4);
            ZETA = 0.5 * (1 - (A0 / A1));
            P_FRIC_SECTION10 = 0.5 * ZETA * DEN0 * VEL0 * VEL0;

            C0 = PST0 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC_SECTION10;
            C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
            C2 = MASS_FLOW_RATE / A1;
            PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
            DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
            VEL1 = (MASS_FLOW_RATE / (A1 * DEN1));
            DELP10 = (PST1 - PST0);
            //file6 << "P_Drop_SECTION10" << "\t" << DELP10 << endl;
            //CALCULATIONS FOR SECTION 10 ENDS HERE

            //CALCULATIONS FOR SECTION 11 (Straight Pipe) STARTS HERE
            VEL0 = VEL1; DEN0 = DEN1; PST0 = PST1;
            FLOW_AREA = PI * D2 * D2 / 4;
            RE = DEN0 * VEL0 * D2 / VISC;
            NONDIM_ROUGHNESS = ROUGHNESS / D2;
            FRIC_FACT = CALC_FRIC_FACT(RE);
            ZETA = FRIC_FACT * L2 / D2;
            P_FRIC_SECTION11 = 0.5 * ZETA * DEN0 * VEL0 * VEL0;

            C0 = PST0 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC_SECTION11;
            C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
            C2 = MASS_FLOW_RATE / FLOW_AREA;
            PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
            DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
            VEL1 = (MASS_FLOW_RATE / (FLOW_AREA * DEN1));
            DELP11 = (PST1 - PST0);
            //file6 << "P_Drop_SECTION11" << "\t" << DELP11 << endl;
            //CALCULATIONS FOR SECTION 11 ENDS HERE

            //CALCULATIONS FOR SECTION 12 (Sudden Expansion) STARTS HERE
            VEL0 = VEL1; DEN0 = DEN1; PST0 = PST1;
            PIPE_DIA = D2; PIPE_DIA1 = D3;
            //PIPE_DIA = D3;
            A0 = (PI * (D2 * D2) / 4); A1 = (PI * (D3 * D3) / 4);
            //A0 = (PI*(D3*D3)/4);
            /*RE = DEN0*VEL0*PIPE_DIA/VISC;
             FRIC_FACT = CALC_FRIC_FACT(RE);
             ZETA = FRIC_FACT*50;*/

            ZETA = (1 - (A1 / A0)) * (1 - (A1 / A0)) / ((A1 / A0) * (A1 / A0));
            P_FRIC_SECTION12 = 0.5 * ZETA * DEN0 * VEL0 * VEL0;
            C0 = PST0 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC_SECTION12;
            C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
            C2 = MASS_FLOW_RATE / A1;
            PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
            DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
            VEL1 = (MASS_FLOW_RATE / (A1 * DEN1));
            DELP12 = (PST1 - PST0);
            //DELP12 = Math.Sqrt((PST1 - PST0)*(PST1 - PST0));
            //file6 << "P_Drop_SECTION12" << "\t" << DELP12 << endl;
            //CALCULATIONS FOR SECTION 12 ENDS HERE

            //CALCULATIONS FOR SECTION 13 (Straight Pipe) STARTS HERE
            VEL0 = VEL1; DEN0 = DEN1; PST0 = PST1;
            FLOW_AREA = PI * D3 * D3 / 4;
            RE = DEN0 * VEL0 * D3 / VISC;
            NONDIM_ROUGHNESS = ROUGHNESS / D3;
            FRIC_FACT = CALC_FRIC_FACT(RE);
            ZETA = FRIC_FACT * (0.915 + L1 + CONE_LENGTH + 0.250) / D3;
            P_FRIC_SECTION13 = 0.5 * ZETA * DEN0 * VEL0 * VEL0;

            C0 = PST0 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC_SECTION13;
            C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
            C2 = MASS_FLOW_RATE / FLOW_AREA;
            PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
            DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
            VEL1 = (MASS_FLOW_RATE / (FLOW_AREA * DEN1));
            DELP13 = (PST1 - PST0);
            //file6 << "P_Drop_SECTION13" << "\t" << DELP13 << endl;
            //CALCULATIONS FOR SECTION 13 ENDS HERE

            //TOTAL_P_FRIC = P_FRIC_SECTION1+P_FRIC_SECTION2+P_FRIC_SECTION3+P_FRIC_SECTION4+P_FRIC_SECTION5+P_FRIC_SECTION6+P_FRIC_SECTION7+P_FRIC_SECTION8+P_FRIC_SECTION9+P_FRIC_SECTION10+P_FRIC_SECTION11+P_FRIC_SECTION12+P_FRIC_SECTION13;
            TOTAL_P_FRIC = DELP1 + DELP2 + DELP3 + DELP4 + DELP5 + DELP6 + DELP7 + DELP8 + DELP9 + DELP10 + DELP11 + DELP12 + DELP13;
            DELP = PST1 - PST0_DOWNSTREAM;
            PST1 = PST1 / 1000;

            ShowOutPut(PST1, DELP);
            //file6 << "Total Pressre Drop" << "\t" << TOTAL_P_FRIC << endl;
            //file6 << "Total Pressre Drop_1" << "\t" << DELP << endl;
        }


        double NONDIM_ROUGHNESS;
        private double CALC_FRIC_FACT(double RE)
        {
            double a, b, a1, a2, a3, b1, FF;

            if (RE <= 2300)
            {
                a1 = Math.Pow((RE / 2712), 8.4);
                a = (1.0 / (1 + a1));
                a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                b1 = Math.Pow(a1, 1.8);
                b = (1.0 / (1 + b1));
                a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                FF = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
            }
            else
            {
                a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                a3 = Math.Log(RE / (1.816 * a2));
                b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                FF = Math.Pow((-2 * b1), -2);
            }

            return FF;
        }

        private void Water_Seal_Drum()
        {
            string cmbTypeT = Convert.ToString(cmbType.Text);
            if (cmbTypeT == "Water Seal Drum Above 10 Inch")
            {
                double PIPE_DIA, PIPE_DIA1, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
                double VEL0, A0, A1, VEL1;
                double SEAL_DIA, SEAL_LENGTH, BUCKET_DIA, WATER_DEPTH, CONE_LENGTH;
                double VISC, PST0_DOWNSTREAM;
                double a, b, ROUGHNESS;
                double ZETA, LENGTH_PIPE, LENGTH_CONT, D_EQ;
                double P_FRIC, C0, C1, C2;
                double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
                double P_FRIC_SECTION1, P_FRIC_SECTION2, P_FRIC_SECTION3, P_FRIC_SECTION4, P_FRIC_SECTION5, P_FRIC_SECTION6, TOTAL_P_FRIC, P_FRIC_SECTION7;
                double DELP1, DELP2, DELP3, DELP4, DELP5, DELP6, DELP7;

                // Input - Globle Variables: There should be button for the following globle inputs

                //printf("Enter the value of Mass flow rate in kg/s: ");
                //scanf("%lf", &MASS_FLOW_RATE);
                //printf("Enter the value of Temperature in Kelvin: ");
                //scanf("%lf", &TEMP);
                //printf("Enter the value of Molecular Weight in kg/kmol: ");
                //scanf("%lf", &MOLEQ_WEIGHT);
                //printf("Enter the value of ratio of Secific heat Gamma: ");
                //scanf("%lf", &GAMMA);

                MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
                MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
                TEMP = Convert.ToDouble(txtTEMP.Text);
                MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
                GAMMA = Convert.ToDouble(txtGAMMA.Text);

                // Viscosity will be calculated based on Temperature and Molecular Weight

                VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
                // However, User can edit the Viscosity 
                //printf("Enter the value of Viscoaity in Pas: ");
                //scanf("%lf", &VISC);

                VISC = Convert.ToDouble(txtVISC.Text);

                //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

                //printf("Enter the value of Outlet Pipe Diameter D in m: ");
                //scanf("%lf", &PIPE_DIA);

                PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);

                //printf("Enter the value of Water Seal Diameter (dseal) in m: ");
                //scanf("%lf", &SEAL_DIA);

                SEAL_DIA = Convert.ToDouble(txtSEAL_DIA.Text);

                //printf("Enter the value of  Bucket Diameter in m: ");
                //scanf("%lf", &BUCKET_DIA);
                //printf("Enter the value of Inlet Pipe Diameter (dpipe) in m: ");
                //scanf("%lf", &PIPE_DIA1);

                PIPE_DIA1 = Convert.ToDouble(txtPIPE_DIA1.Text);

                //printf("Enter the value of Water Seal Length in m: ");
                //scanf("%lf", &SEAL_LENGTH);
                //printf("Enter the value of Water DEPTH in m: ");
                //scanf("%lf", &WATER_DEPTH);

                //printf("Enter the value of Roughness in m: ");
                //scanf("%lf", &ROUGHNESS);

                ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

                PIPE_DIA1 = ConvertMtoMMPerMeasure(PIPE_DIA1, cmbPIPE_DIA1);
                PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
                SEAL_DIA = ConvertMtoMMPerMeasure(SEAL_DIA, cmbSEAL_DIA);
                ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);

                //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

                //printf("Enter the value of Downsream Pressure in kPa: ");
                //scanf("%lf", &PST0);

                PST0 = Convert.ToDouble(txtPST0.Text);
                PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

                // All input ends here
                //ofstream file6("Output_Data.txt");
                PST0 = PST0 * 1000;
                PST0_DOWNSTREAM = PST0;
                UNIVERSAL_GAS_CONST = 8314; CONE_LENGTH = ((SEAL_DIA - PIPE_DIA) / 1.15);

                A0 = (PI * PIPE_DIA * PIPE_DIA / 4); A1 = (PI * SEAL_DIA * SEAL_DIA / 4);
                DEN0 = (PST0 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
                SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
                MACH = VEL0 / SOUND_VEL;

                if (MACH >= 0.7)
                {
                    MessageBox.Show("Flow is compressible please Edit the input parameters");
                    return;
                }


                //CALCULATIONS FOR SECTION 1 (Gradual Contraction) STARTS HERE

                RE = DEN0 * VEL0 * PIPE_DIA / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
                FRIC_FACT = CALC_FRIC_FACT(RE);
                D_EQ = 3 * PIPE_DIA * PIPE_DIA * PIPE_DIA / (PIPE_DIA * PIPE_DIA + SEAL_DIA * PIPE_DIA + SEAL_DIA * SEAL_DIA);
                ZETA = FRIC_FACT * CONE_LENGTH / D_EQ;
                P_FRIC_SECTION1 = 0.5 * ZETA * DEN0 * VEL0 * VEL0;

                C0 = PST0 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC_SECTION1;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A1;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL1 = (MASS_FLOW_RATE / (A1 * DEN1));
                DELP1 = (PST1 - PST0);
                //file6<<"P_Drop_SECTION1"<<"\t"<<DELP1<<endl;
                //CALCULATIONS FOR SECTION 1 ENDS HERE

                //CALCULATIONS FOR SECTION 2 (Straight Pipe) STARTS HERE
                SEAL_LENGTH = 3.0;
                VEL0 = VEL1; DEN0 = DEN1; PST0 = PST1;
                RE = DEN0 * VEL0 * SEAL_DIA / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / SEAL_DIA;
                FRIC_FACT = CALC_FRIC_FACT(RE);
                ZETA = FRIC_FACT * SEAL_LENGTH / SEAL_DIA;
                P_FRIC_SECTION2 = 0.5 * ZETA * DEN0 * VEL0 * VEL0;

                C0 = PST0 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC_SECTION2;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A1;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL1 = (MASS_FLOW_RATE / (A1 * DEN1));
                DELP2 = (PST1 - PST0);
                //file6<<"P_Drop_SECTION2"<<"\t"<<DELP2<<endl;
                //CALCULATIONS FOR SECTION 2 ENDS HERE 

                //CALCULATIONS FOR SECTION 3 (Sudden Expansion) STARTS HERE
                VEL0 = VEL1; DEN0 = DEN1; PST0 = PST1;
                A0 = (PI * SEAL_DIA * SEAL_DIA / 4); A1 = (PI * (PIPE_DIA1 * PIPE_DIA1) / 4);
                ZETA = (1 - (A1 / A0)) * (1 - (A1 / A0)) / ((A1 / A0) * (A1 / A0));
                P_FRIC_SECTION3 = 0.5 * ZETA * DEN0 * VEL0 * VEL0;
                C0 = PST0 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC_SECTION3;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A1;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL1 = (MASS_FLOW_RATE / (A1 * DEN1));
                DELP3 = (PST1 - PST0);
                //file6<<"P_Drop_SECTION3"<<"\t"<<DELP3<<endl;
                //CALCULATIONS FOR SECTION 3 ENDS HERE

                //CALCULATIONS FOR SECTION 4 (Straight Pipe) STARTS HERE
                //WATER_DEPTH = 3;
                VEL0 = VEL1; DEN0 = DEN1; PST0 = PST1;
                RE = DEN0 * VEL0 * PIPE_DIA1 / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA1;
                FRIC_FACT = CALC_FRIC_FACT(RE);
                ZETA = FRIC_FACT * (SEAL_LENGTH) / PIPE_DIA1;
                P_FRIC_SECTION4 = 0.5 * ZETA * DEN0 * VEL0 * VEL0;

                C0 = PST0 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC_SECTION4;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A1;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL1 = (MASS_FLOW_RATE / (A1 * DEN1));
                DELP4 = (PST1 - PST0);
                //file6<<"P_Drop_SECTION4"<<"\t"<<DELP4<<endl;
                //CALCULATIONS FOR SECTION 4 ENDS HERE 

                //CALCULATIONS FOR SECTION 5 (90 Elbow) STARTS HERE
                VEL0 = VEL1; DEN0 = DEN1; PST0 = PST1;
                RE = DEN0 * VEL0 * PIPE_DIA1 / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA1;
                FRIC_FACT = CALC_FRIC_FACT(RE);
                ZETA = FRIC_FACT * 20;
                P_FRIC_SECTION5 = 0.5 * ZETA * DEN0 * VEL0 * VEL0;

                C0 = PST0 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC_SECTION5;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A1;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL1 = (MASS_FLOW_RATE / (A1 * DEN1));
                DELP5 = (PST1 - PST0);
                //file6<<"P_Drop_SECTION5"<<"\t"<<DELP5<<endl;
                //CALCULATIONS FOR SECTION 5 ENDS HERE 

                //CALCULATIONS FOR SECTION 6 (Straight Pipe) STARTS HERE
                VEL0 = VEL1; DEN0 = DEN1; PST0 = PST1;
                RE = DEN0 * VEL0 * PIPE_DIA1 / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA1;
                FRIC_FACT = CALC_FRIC_FACT(RE);
                ZETA = FRIC_FACT * (SEAL_DIA / 2) / PIPE_DIA1;
                P_FRIC_SECTION6 = 0.5 * ZETA * DEN0 * VEL0 * VEL0;

                C0 = PST0 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC_SECTION6;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A1;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL1 = (MASS_FLOW_RATE / (A1 * DEN1));
                DELP7 = (PST1 - PST0);
                //file6<<"P_Drop_SECTION6"<<"\t"<<DELP6<<endl;
                //CALCULATIONS FOR SECTION 6 ENDS HERE 

                //TOTAL_P_FRIC = P_FRIC_SECTION1+P_FRIC_SECTION2+P_FRIC_SECTION3+P_FRIC_SECTION4+P_FRIC_SECTION5+P_FRIC_SECTION6;
                TOTAL_P_FRIC = DELP1 + DELP2 + DELP3 + DELP4 + DELP5 + DELP7;
                DELP = PST1 - PST0_DOWNSTREAM;
                PST1 = PST1 / 1000;

                ShowOutPut(PST1, DELP);
                //file6 << "Total Pressre Drop" << "\t" << TOTAL_P_FRIC << endl;
                //file6 << "Total Pressre Drop_1" << "\t" << DELP << endl;
            }
            else if (cmbTypeT == "Water Seal Drum Upto 10 Inch")
            {
                double PIPE_DIA, PIPE_DIA1, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
                double VEL0, A0, A1, VEL1;
                double SEAL_DIA, SEAL_LENGTH, BUCKET_DIA, WATER_DEPTH, CONE_LENGTH;
                double VISC, PST0_DOWNSTREAM;
                double a, b, ROUGHNESS;
                double ZETA, LENGTH_PIPE, LENGTH_CONT, D_EQ;
                double P_FRIC, C0, C1, C2;
                double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
                double P_FRIC_SECTION1, P_FRIC_SECTION2, P_FRIC_SECTION3, P_FRIC_SECTION4, P_FRIC_SECTION5, P_FRIC_SECTION6, TOTAL_P_FRIC, P_FRIC_SECTION7;
                double DELP1, DELP2, DELP3, DELP4, DELP5, DELP6, DELP7;

                // Input - Globle Variables: There should be button for the following globle inputs

                //printf("Enter the value of Mass flow rate in kg/s: ");
                //scanf("%lf", &MASS_FLOW_RATE);
                //printf("Enter the value of Temperature in Kelvin: ");
                //scanf("%lf", &TEMP);
                //printf("Enter the value of Molecular Weight in kg/kmol: ");
                //scanf("%lf", &MOLEQ_WEIGHT);
                //printf("Enter the value of ratio of Secific heat Gamma: ");
                //scanf("%lf", &GAMMA);

                MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
                MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
                TEMP = Convert.ToDouble(txtTEMP.Text);
                MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
                GAMMA = Convert.ToDouble(txtGAMMA.Text);

                // Viscosity will be calculated based on Temperature and Molecular Weight

                VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
                // However, User can edit the Viscosity 
                //printf("Enter the value of Viscoaity in Pas: ");
                //scanf("%lf", &VISC);

                VISC = Convert.ToDouble(txtVISC.Text);

                //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

                //printf("Enter the value of Outlet Pipe Diameter D in m: ");
                //scanf("%lf", &PIPE_DIA);

                PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);

                //printf("Enter the value of Water Seal Diameter (dseal) in m: ");
                //scanf("%lf", &SEAL_DIA);
                //printf("Enter the value of Bucket Diameter (dbucket) in m: ");
                //scanf("%lf", &BUCKET_DIA);
                //printf("Enter the value of Inlet Pipe Diameter (dpipe) in m: ");
                //scanf("%lf", &PIPE_DIA1);

                SEAL_DIA = Convert.ToDouble(txtSEAL_DIA.Text);
                BUCKET_DIA = Convert.ToDouble(txtBUCKET_DIA.Text);
                PIPE_DIA1 = Convert.ToDouble(txtPIPE_DIA1.Text);

                //printf("Enter the value of Water Seal Length in m: ");
                //scanf("%lf", &SEAL_LENGTH);
                //printf("Enter the value of Water DEPTH in m: ");
                //scanf("%lf", &WATER_DEPTH);

                //printf("Enter the value of Roughness in m: ");
                //scanf("%lf", &ROUGHNESS);
                ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

                PIPE_DIA1 = ConvertMtoMMPerMeasure(PIPE_DIA1, cmbPIPE_DIA1);
                PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
                SEAL_DIA = ConvertMtoMMPerMeasure(SEAL_DIA, cmbSEAL_DIA);
                BUCKET_DIA = ConvertMtoMMPerMeasure(BUCKET_DIA, cmbBUCKET_DIA);
                ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);

                //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

                //printf("Enter the value of Downsream Pressure in kPa: ");
                //scanf("%lf", &PST0);

                PST0 = Convert.ToDouble(txtPST0.Text);
                PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

                // All input ends here
                //ofstream file6("Output_Data.txt");
                PST0 = PST0 * 1000;
                PST0_DOWNSTREAM = PST0;
                UNIVERSAL_GAS_CONST = 8314; CONE_LENGTH = ((SEAL_DIA - PIPE_DIA) / 1.15);

                A0 = (PI * PIPE_DIA * PIPE_DIA / 4); A1 = (PI * SEAL_DIA * SEAL_DIA / 4);
                DEN0 = (PST0 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
                SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
                MACH = VEL0 / SOUND_VEL;

                if (MACH >= 0.7)
                {
                    MessageBox.Show("Flow is compressible please Edit the input parameters");
                    return;
                }

                //CALCULATIONS FOR SECTION 1 (Gradual Contraction) STARTS HERE

                RE = DEN0 * VEL0 * PIPE_DIA / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
                FRIC_FACT = CALC_FRIC_FACT(RE);
                D_EQ = 3 * PIPE_DIA * PIPE_DIA * PIPE_DIA / (PIPE_DIA * PIPE_DIA + SEAL_DIA * PIPE_DIA + SEAL_DIA * SEAL_DIA);
                ZETA = FRIC_FACT * CONE_LENGTH / D_EQ;
                P_FRIC_SECTION1 = 0.5 * ZETA * DEN0 * VEL0 * VEL0;

                C0 = PST0 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC_SECTION1;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A1;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL1 = (MASS_FLOW_RATE / (A1 * DEN1));
                DELP1 = (PST1 - PST0);
                //file6<<"P_Drop_SECTION1"<<"\t"<<DELP1<<endl;
                //CALCULATIONS FOR SECTION 1 ENDS HERE

                //CALCULATIONS FOR SECTION 2 (Straight Pipe) STARTS HERE
                SEAL_LENGTH = 3.0;
                VEL0 = VEL1; DEN0 = DEN1; PST0 = PST1;
                RE = DEN0 * VEL0 * SEAL_DIA / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / SEAL_DIA;
                FRIC_FACT = CALC_FRIC_FACT(RE);
                ZETA = FRIC_FACT * SEAL_LENGTH / SEAL_DIA;
                P_FRIC_SECTION2 = 0.5 * ZETA * DEN0 * VEL0 * VEL0;

                C0 = PST0 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC_SECTION2;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A1;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL1 = (MASS_FLOW_RATE / (A1 * DEN1));
                DELP2 = (PST1 - PST0);
                //file6<<"P_Drop_SECTION2"<<"\t"<<DELP2<<endl;
                //CALCULATIONS FOR SECTION 2 ENDS HERE 

                //CALCULATIONS FOR SECTION 3 (Sudden Expansion) STARTS HERE
                VEL0 = VEL1; DEN0 = DEN1; PST0 = PST1;
                A0 = (PI * SEAL_DIA * SEAL_DIA / 4); A1 = (PI * (BUCKET_DIA * BUCKET_DIA) / 4);
                ZETA = (1 - (A1 / A0)) * (1 - (A1 / A0)) / ((A1 / A0) * (A1 / A0));
                P_FRIC_SECTION3 = 0.5 * ZETA * DEN0 * VEL0 * VEL0;
                C0 = PST0 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC_SECTION3;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A1;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL1 = (MASS_FLOW_RATE / (A1 * DEN1));
                DELP3 = (PST1 - PST0);
                //file6<<"P_Drop_SECTION3"<<"\t"<<DELP3<<endl;
                //CALCULATIONS FOR SECTION 3 ENDS HERE

                //CALCULATIONS FOR SECTION 4 (Sudden Expansion) STARTS HERE
                VEL0 = VEL1; DEN0 = DEN1; PST0 = PST1;
                A0 = (PI * BUCKET_DIA * BUCKET_DIA / 4); A1 = (PI * (PIPE_DIA1 * PIPE_DIA1) / 4);
                ZETA = (1 - (A1 / A0)) * (1 - (A1 / A0)) / ((A1 / A0) * (A1 / A0));
                P_FRIC_SECTION4 = 0.5 * ZETA * DEN0 * VEL0 * VEL0;
                C0 = PST0 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC_SECTION4;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A1;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL1 = (MASS_FLOW_RATE / (A1 * DEN1));
                DELP4 = (PST1 - PST0);
                //file6<<"P_Drop_SECTION4"<<"\t"<<DELP4<<endl;
                //CALCULATIONS FOR SECTION 4 ENDS HERE

                //CALCULATIONS FOR SECTION 5 (Straight Pipe) STARTS HERE
                WATER_DEPTH = 0.25;
                VEL0 = VEL1; DEN0 = DEN1; PST0 = PST1;
                RE = DEN0 * VEL0 * PIPE_DIA1 / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA1;
                FRIC_FACT = CALC_FRIC_FACT(RE);
                ZETA = FRIC_FACT * (SEAL_LENGTH + WATER_DEPTH) / PIPE_DIA1;
                P_FRIC_SECTION5 = 0.5 * ZETA * DEN0 * VEL0 * VEL0;

                C0 = PST0 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC_SECTION5;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A1;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL1 = (MASS_FLOW_RATE / (A1 * DEN1));
                DELP5 = (PST1 - PST0);
                //file6<<"P_Drop_SECTION5"<<"\t"<<DELP5<<endl;
                //CALCULATIONS FOR SECTION 5 ENDS HERE 

                //CALCULATIONS FOR SECTION 6 (90 Elbow) STARTS HERE
                VEL0 = VEL1; DEN0 = DEN1; PST0 = PST1;
                RE = DEN0 * VEL0 * PIPE_DIA1 / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA1;
                FRIC_FACT = CALC_FRIC_FACT(RE);
                ZETA = FRIC_FACT * 20;
                P_FRIC_SECTION6 = 0.5 * ZETA * DEN0 * VEL0 * VEL0;

                C0 = PST0 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC_SECTION6;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A1;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL1 = (MASS_FLOW_RATE / (A1 * DEN1));
                DELP6 = (PST1 - PST0);
                //file6<<"P_Drop_SECTION6"<<"\t"<<DELP6<<endl;
                //CALCULATIONS FOR SECTION 6 ENDS HERE 

                //CALCULATIONS FOR SECTION 7 (Straight Pipe) STARTS HERE
                VEL0 = VEL1; DEN0 = DEN1; PST0 = PST1;
                RE = DEN0 * VEL0 * PIPE_DIA1 / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA1;
                FRIC_FACT = CALC_FRIC_FACT(RE);
                ZETA = FRIC_FACT * (SEAL_DIA / 2) / PIPE_DIA1;
                P_FRIC_SECTION7 = 0.5 * ZETA * DEN0 * VEL0 * VEL0;

                C0 = PST0 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC_SECTION7;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A1;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL1 = (MASS_FLOW_RATE / (A1 * DEN1));
                DELP7 = (PST1 - PST0);
                //file6<<"P_Drop_SECTION7"<<"\t"<<DELP7<<endl;
                //CALCULATIONS FOR SECTION 7 ENDS HERE 

                //TOTAL_P_FRIC = P_FRIC_SECTION1+P_FRIC_SECTION2+P_FRIC_SECTION3+P_FRIC_SECTION4+P_FRIC_SECTION5+P_FRIC_SECTION6+P_FRIC_SECTION7;
                TOTAL_P_FRIC = DELP1 + DELP2 + DELP3 + DELP4 + DELP5 + DELP6 + DELP7;
                DELP = PST1 - PST0_DOWNSTREAM;
                PST1 = PST1 / 1000;

                ShowOutPut(PST1, DELP);
                //file6 << "Total Pressre Drop" << "\t" << TOTAL_P_FRIC << endl;
                //file6 << "Total Pressre Drop_1" << "\t" << DELP << endl;
            }
        }

        private void T_Branch_to_Run_Flow()
        {
            string cmbTypeT = Convert.ToString(cmbType.Text);
            if (cmbTypeT == "T Branching Flow Theta = 0 degree")
            {
                double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
                double VEL0, A0;
                double VISC;
                double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
                double ZETA, LENGTH_PIPE;
                double P_FRIC, C0, C1, C2;
                double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
                double FITTING_QUANTITY, LBYD = 2, K;
                // Input - Globle Variables: There should be button for the following globle inputs

                //printf("Enter the value of Mass flow rate in kg/s: ");
                //scanf("%lf", &MASS_FLOW_RATE);
                //printf("Enter the value of Temperature in Kelvin: ");
                //scanf("%lf", &TEMP);
                //printf("Enter the value of Molecular Weight in kg/kmol: ");
                //scanf("%lf", &MOLEQ_WEIGHT);
                //printf("Enter the value of ratio of Secific heat Gamma: ");
                //scanf("%lf", &GAMMA);

                MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
                MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
                TEMP = Convert.ToDouble(txtTEMP.Text);
                MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
                GAMMA = Convert.ToDouble(txtGAMMA.Text);


                // Viscosity will be calculated based on Temperature and Molecular Weight

                VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
                // However, User can edit the Viscosity 
                //printf("Enter the value of Viscoaity in Pas: ");
                //scanf("%lf", &VISC);

                VISC = Convert.ToDouble(txtVISC.Text);

                //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

                //printf("Enter the value of Pipe Diameter (Run Dia d0) in m: ");
                //scanf("%lf", &PIPE_DIA);
                //printf("Enter the Quantity: ");
                //scanf("%lf", &FITTING_QUANTITY);
                //printf("Enter the value of Roughness in m: ");
                //scanf("%lf", &ROUGHNESS);

                PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);
                FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);
                ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

                PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
                ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);

                //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

                //printf("Enter the value of Upsteam Pressure in kPa: ");
                //scanf("%lf", &PST0);

                PST0 = Convert.ToDouble(txtPST0.Text);
                PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

                // All input ends here
                // ofstream file6("Output_Data.txt");
                UNIVERSAL_GAS_CONST = 8314;
                A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
                SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
                MACH = VEL0 / SOUND_VEL;

                if (MACH >= 0.7)
                {
                    MessageBox.Show("Flow is compressible please Edit the input parameters");
                    return;
                }
                RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
                if (RE <= 2300)
                {
                    a1 = Math.Pow((RE / 2712), 8.4);
                    a = (1.0 / (1 + a1));
                    a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                    b1 = Math.Pow(a1, 1.8);
                    b = (1.0 / (1 + b1));
                    a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                    FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
                }
                else
                {
                    a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                    a3 = Math.Log(RE / (1.816 * a2));
                    b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                    FRIC_FACT = Math.Pow((-2 * b1), -2);
                }
                ZETA = FRIC_FACT * LBYD;
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

                C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A0;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                DELP = (PST1 - PST0 * 1000);
                PST1 = PST1 / 1000;

                ShowOutPut(PST1, DELP);
                //printf("Edit Mass Flow Rate in Next Fittings due to Branch Flow");
            }
            else if (cmbTypeT == "T Branching Flow Theta = 15 degree")
            {
                double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
                double VEL0, A0;
                double VISC;
                double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
                double ZETA, LENGTH_PIPE;
                double P_FRIC, C0, C1, C2;
                double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
                double FITTING_QUANTITY, LBYD = 4, K;
                // Input - Globle Variables: There should be button for the following globle inputs

                //printf("Enter the value of Mass flow rate in kg/s: ");
                //scanf("%lf", &MASS_FLOW_RATE);
                //printf("Enter the value of Temperature in Kelvin: ");
                //scanf("%lf", &TEMP);
                //printf("Enter the value of Molecular Weight in kg/kmol: ");
                //scanf("%lf", &MOLEQ_WEIGHT);
                //printf("Enter the value of ratio of Secific heat Gamma: ");
                //scanf("%lf", &GAMMA);

                MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
                MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
                TEMP = Convert.ToDouble(txtTEMP.Text);
                MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
                GAMMA = Convert.ToDouble(txtGAMMA.Text);


                // Viscosity will be calculated based on Temperature and Molecular Weight

                VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
                // However, User can edit the Viscosity 
                //printf("Enter the value of Viscoaity in Pas: ");
                //scanf("%lf", &VISC);

                VISC = Convert.ToDouble(txtVISC.Text);

                //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

                //printf("Enter the value of Pipe Diameter (Run Dia d0) in m: ");
                //scanf("%lf", &PIPE_DIA);
                //printf("Enter the Quantity: ");
                //scanf("%lf", &FITTING_QUANTITY);
                //printf("Enter the value of Roughness in m: ");
                //scanf("%lf", &ROUGHNESS);

                PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);
                FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);
                ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

                PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
                ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);
                //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

                //printf("Enter the value of Upsteam Pressure in kPa: ");
                //scanf("%lf", &PST0)

                PST0 = Convert.ToDouble(txtPST0.Text);
                PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

                // All input ends here
                // ofstream file6("Output_Data.txt");
                UNIVERSAL_GAS_CONST = 8314;
                A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
                SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
                MACH = VEL0 / SOUND_VEL;

                if (MACH >= 0.7)
                {
                    MessageBox.Show("Flow is compressible please Edit the input parameters");
                    return;
                }
                RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
                if (RE <= 2300)
                {
                    a1 = Math.Pow((RE / 2712), 8.4);
                    a = (1.0 / (1 + a1));
                    a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                    b1 = Math.Pow(a1, 1.8);
                    b = (1.0 / (1 + b1));
                    a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                    FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
                }
                else
                {
                    a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                    a3 = Math.Log(RE / (1.816 * a2));
                    b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                    FRIC_FACT = Math.Pow((-2 * b1), -2);
                }
                ZETA = FRIC_FACT * LBYD;
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

                C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A0;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                DELP = (PST1 - PST0 * 1000);
                PST1 = PST1 / 1000;

                ShowOutPut(PST1, DELP);
                //printf("Edit Mass Flow Rate in Next Fittings due to Branch Flow");
            }
            else if (cmbTypeT == "T Branching Flow Theta = 30 degree")
            {
                double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
                double VEL0, A0;
                double VISC;
                double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
                double ZETA, LENGTH_PIPE;
                double P_FRIC, C0, C1, C2;
                double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
                double FITTING_QUANTITY, LBYD = 8, K;
                // Input - Globle Variables: There should be button for the following globle inputs

                //printf("Enter the value of Mass flow rate in kg/s: ");
                //scanf("%lf", &MASS_FLOW_RATE);
                //printf("Enter the value of Temperature in Kelvin: ");
                //scanf("%lf", &TEMP);
                //printf("Enter the value of Molecular Weight in kg/kmol: ");
                //scanf("%lf", &MOLEQ_WEIGHT);
                //printf("Enter the value of ratio of Secific heat Gamma: ");
                //scanf("%lf", &GAMMA);

                MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
                MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
                TEMP = Convert.ToDouble(txtTEMP.Text);
                MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
                GAMMA = Convert.ToDouble(txtGAMMA.Text);

                // Viscosity will be calculated based on Temperature and Molecular Weight

                VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
                // However, User can edit the Viscosity 
                //printf("Enter the value of Viscoaity in Pas: ");
                //scanf("%lf", &VISC);

                VISC = Convert.ToDouble(txtVISC.Text);

                //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

                //printf("Enter the value of Pipe Diameter (Run Dia d0) in m: ");
                //scanf("%lf", &PIPE_DIA);
                //printf("Enter the Quantity: ");
                //scanf("%lf", &FITTING_QUANTITY);
                //printf("Enter the value of Roughness in m: ");
                //scanf("%lf", &ROUGHNESS);

                PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);
                FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);
                ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

                PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
                ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);
                //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

                //printf("Enter the value of Upsteam Pressure in kPa: ");
                //scanf("%lf", &PST0);

                PST0 = Convert.ToDouble(txtPST0.Text);
                PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

                // All input ends here
                // ofstream file6("Output_Data.txt");
                UNIVERSAL_GAS_CONST = 8314;
                A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
                SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
                MACH = VEL0 / SOUND_VEL;

                if (MACH >= 0.7)
                {
                    MessageBox.Show("Flow is compressible please Edit the input parameters");
                    return;
                }
                RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
                if (RE <= 2300)
                {
                    a1 = Math.Pow((RE / 2712), 8.4);
                    a = (1.0 / (1 + a1));
                    a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                    b1 = Math.Pow(a1, 1.8);
                    b = (1.0 / (1 + b1));
                    a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                    FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
                }
                else
                {
                    a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                    a3 = Math.Log(RE / (1.816 * a2));
                    b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                    FRIC_FACT = Math.Pow((-2 * b1), -2);
                }
                ZETA = FRIC_FACT * LBYD;
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

                C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A0;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                DELP = (PST1 - PST0 * 1000);
                PST1 = PST1 / 1000;

                ShowOutPut(PST1, DELP);
                //printf("Edit Mass Flow Rate in Next Fittings due to Branch Flow");
            }
            else if (cmbTypeT == "T Branching Flow Theta = 45 degree")
            {
                double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
                double VEL0, A0;
                double VISC;
                double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
                double ZETA, LENGTH_PIPE;
                double P_FRIC, C0, C1, C2;
                double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
                double FITTING_QUANTITY, LBYD = 15, K;
                // Input - Globle Variables: There should be button for the following globle inputs

                //printf("Enter the value of Mass flow rate in kg/s: ");
                //scanf("%lf", &MASS_FLOW_RATE);
                //printf("Enter the value of Temperature in Kelvin: ");
                //scanf("%lf", &TEMP);
                //printf("Enter the value of Molecular Weight in kg/kmol: ");
                //scanf("%lf", &MOLEQ_WEIGHT);
                //printf("Enter the value of ratio of Secific heat Gamma: ");
                //scanf("%lf", &GAMMA);

                MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
                MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
                TEMP = Convert.ToDouble(txtTEMP.Text);
                MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
                GAMMA = Convert.ToDouble(txtGAMMA.Text);

                // Viscosity will be calculated based on Temperature and Molecular Weight

                VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
                // However, User can edit the Viscosity 
                //printf("Enter the value of Viscoaity in Pas: ");
                //scanf("%lf", &VISC);

                VISC = Convert.ToDouble(txtVISC.Text);

                //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

                //printf("Enter the value of Pipe Diameter (Run Dia d0) in m: ");
                //scanf("%lf", &PIPE_DIA);
                //printf("Enter the Quantity: ");
                //scanf("%lf", &FITTING_QUANTITY);
                //printf("Enter the value of Roughness in m: ");
                //scanf("%lf", &ROUGHNESS);

                PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);
                FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);
                ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

                PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
                ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);

                //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

                //printf("Enter the value of Upsteam Pressure in kPa: ");
                //scanf("%lf", &PST0);

                PST0 = Convert.ToDouble(txtPST0.Text);
                PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

                // All input ends here
                // ofstream file6("Output_Data.txt");
                UNIVERSAL_GAS_CONST = 8314;
                A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
                SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
                MACH = VEL0 / SOUND_VEL;

                if (MACH >= 0.7)
                {
                    MessageBox.Show("Flow is compressible please Edit the input parameters");
                    return;
                }
                RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
                if (RE <= 2300)
                {
                    a1 = Math.Pow((RE / 2712), 8.4);
                    a = (1.0 / (1 + a1));
                    a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                    b1 = Math.Pow(a1, 1.8);
                    b = (1.0 / (1 + b1));
                    a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                    FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
                }
                else
                {
                    a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                    a3 = Math.Log(RE / (1.816 * a2));
                    b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                    FRIC_FACT = Math.Pow((-2 * b1), -2);
                }
                ZETA = FRIC_FACT * LBYD;
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

                C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A0;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                DELP = (PST1 - PST0 * 1000);
                PST1 = PST1 / 1000;

                ShowOutPut(PST1, DELP);
                //printf("Edit Mass Flow Rate in Next Fittings due to Branch Flow");
            }
            else if (cmbTypeT == "T Branching Flow Theta = 60 degree")
            {
                double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
                double VEL0, A0;
                double VISC;
                double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
                double ZETA, LENGTH_PIPE;
                double P_FRIC, C0, C1, C2;
                double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
                double FITTING_QUANTITY, LBYD = 25, K;
                // Input - Globle Variables: There should be button for the following globle inputs

                //printf("Enter the value of Mass flow rate in kg/s: ");
                //scanf("%lf", &MASS_FLOW_RATE);
                //printf("Enter the value of Temperature in Kelvin: ");
                //scanf("%lf", &TEMP);
                //printf("Enter the value of Molecular Weight in kg/kmol: ");
                //scanf("%lf", &MOLEQ_WEIGHT);
                //printf("Enter the value of ratio of Secific heat Gamma: ");
                //scanf("%lf", &GAMMA);

                MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
                MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
                TEMP = Convert.ToDouble(txtTEMP.Text);
                MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
                GAMMA = Convert.ToDouble(txtGAMMA.Text);


                // Viscosity will be calculated based on Temperature and Molecular Weight

                VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
                // However, User can edit the Viscosity 
                //printf("Enter the value of Viscoaity in Pas: ");
                //scanf("%lf", &VISC);

                VISC = Convert.ToDouble(txtVISC.Text);

                //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

                //printf("Enter the value of Pipe Diameter (Run Dia d0) in m: ");
                //scanf("%lf", &PIPE_DIA);
                //printf("Enter the Quantity: ");
                //scanf("%lf", &FITTING_QUANTITY);
                //printf("Enter the value of Roughness in m: ");
                //scanf("%lf", &ROUGHNESS);

                PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);
                FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);
                ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

                PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
                ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);
                //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

                //printf("Enter the value of Upsteam Pressure in kPa: ");
                //scanf("%lf", &PST0);

                PST0 = Convert.ToDouble(txtPST0.Text);
                PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

                // All input ends here
                // ofstream file6("Output_Data.txt");
                UNIVERSAL_GAS_CONST = 8314;
                A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
                SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
                MACH = VEL0 / SOUND_VEL;

                if (MACH >= 0.7)
                {
                    MessageBox.Show("Flow is compressible please Edit the input parameters");
                    return;
                }
                RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
                if (RE <= 2300)
                {
                    a1 = Math.Pow((RE / 2712), 8.4);
                    a = (1.0 / (1 + a1));
                    a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                    b1 = Math.Pow(a1, 1.8);
                    b = (1.0 / (1 + b1));
                    a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                    FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
                }
                else
                {
                    a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                    a3 = Math.Log(RE / (1.816 * a2));
                    b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                    FRIC_FACT = Math.Pow((-2 * b1), -2);
                }
                ZETA = FRIC_FACT * LBYD;
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

                C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A0;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                DELP = (PST1 - PST0 * 1000);
                PST1 = PST1 / 1000;

                ShowOutPut(PST1, DELP);
                //printf("Edit Mass Flow Rate in Next Fittings due to Branch Flow");
            }
            else if (cmbTypeT == "T Branching Flow Theta = 75 degree")
            {
                double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
                double VEL0, A0;
                double VISC;
                double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
                double ZETA, LENGTH_PIPE;
                double P_FRIC, C0, C1, C2;
                double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
                double FITTING_QUANTITY, LBYD = 40, K;
                // Input - Globle Variables: There should be button for the following globle inputs

                //printf("Enter the value of Mass flow rate in kg/s: ");
                //scanf("%lf", &MASS_FLOW_RATE);
                //printf("Enter the value of Temperature in Kelvin: ");
                //scanf("%lf", &TEMP);
                //printf("Enter the value of Molecular Weight in kg/kmol: ");
                //scanf("%lf", &MOLEQ_WEIGHT);
                //printf("Enter the value of ratio of Secific heat Gamma: ");
                //scanf("%lf", &GAMMA);

                MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
                MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
                TEMP = Convert.ToDouble(txtTEMP.Text);
                MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
                GAMMA = Convert.ToDouble(txtGAMMA.Text);


                // Viscosity will be calculated based on Temperature and Molecular Weight

                VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
                // However, User can edit the Viscosity 
                //printf("Enter the value of Viscoaity in Pas: ");
                //scanf("%lf", &VISC);

                VISC = Convert.ToDouble(txtVISC.Text);


                //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

                //printf("Enter the value of Pipe Diameter (Run Dia d0) in m: ");
                //scanf("%lf", &PIPE_DIA);
                //printf("Enter the Quantity: ");
                //scanf("%lf", &FITTING_QUANTITY);
                //printf("Enter the value of Roughness in m: ");
                //scanf("%lf", &ROUGHNESS);

                PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);
                FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);
                ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

                PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
                ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);
                //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

                //printf("Enter the value of Upsteam Pressure in kPa: ");
                //scanf("%lf", &PST0);

                PST0 = Convert.ToDouble(txtPST0.Text);
                PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

                // All input ends here
                // ofstream file6("Output_Data.txt");
                UNIVERSAL_GAS_CONST = 8314;
                A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
                SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
                MACH = VEL0 / SOUND_VEL;

                if (MACH >= 0.7)
                {
                    MessageBox.Show("Flow is compressible please Edit the input parameters");
                    return;
                }
                RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
                if (RE <= 2300)
                {
                    a1 = Math.Pow((RE / 2712), 8.4);
                    a = (1.0 / (1 + a1));
                    a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                    b1 = Math.Pow(a1, 1.8);
                    b = (1.0 / (1 + b1));
                    a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                    FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
                }
                else
                {
                    a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                    a3 = Math.Log(RE / (1.816 * a2));
                    b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                    FRIC_FACT = Math.Pow((-2 * b1), -2);
                }
                ZETA = FRIC_FACT * LBYD;
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

                C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A0;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                DELP = (PST1 - PST0 * 1000);
                PST1 = PST1 / 1000;

                ShowOutPut(PST1, DELP);
                //printf("Edit Mass Flow Rate in Next Fittings due to Branch Flow");
            }
            else if (cmbTypeT == "T Branching Flow Theta = 90 degree")
            {
                double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
                double VEL0, A0;
                double VISC;
                double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
                double ZETA, LENGTH_PIPE;
                double P_FRIC, C0, C1, C2;
                double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
                double FITTING_QUANTITY, LBYD = 60, K;
                // Input - Globle Variables: There should be button for the following globle inputs

                //printf("Enter the value of Mass flow rate in kg/s: ");
                //scanf("%lf", &MASS_FLOW_RATE);
                //printf("Enter the value of Temperature in Kelvin: ");
                //scanf("%lf", &TEMP);
                //printf("Enter the value of Molecular Weight in kg/kmol: ");
                //scanf("%lf", &MOLEQ_WEIGHT);
                //printf("Enter the value of ratio of Secific heat Gamma: ");
                //scanf("%lf", &GAMMA);

                MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
                MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
                TEMP = Convert.ToDouble(txtTEMP.Text);
                MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
                GAMMA = Convert.ToDouble(txtGAMMA.Text);

                // Viscosity will be calculated based on Temperature and Molecular Weight

                VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
                // However, User can edit the Viscosity 
                //printf("Enter the value of Viscoaity in Pas: ");
                //scanf("%lf", &VISC);

                VISC = Convert.ToDouble(txtVISC.Text);

                //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

                //printf("Enter the value of Pipe Diameter (Run Dia d0) in m: ");
                //scanf("%lf", &PIPE_DIA);
                //printf("Enter the Quantity: ");
                //scanf("%lf", &FITTING_QUANTITY);
                //printf("Enter the value of Roughness in m: ");
                //scanf("%lf", &ROUGHNESS);

                PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);
                FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);
                ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

                PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
                ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);
                //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

                //printf("Enter the value of Upsteam Pressure in kPa: ");
                //scanf("%lf", &PST0);

                PST0 = Convert.ToDouble(txtPST0.Text);
                PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

                // All input ends here
                // ofstream file6("Output_Data.txt");
                UNIVERSAL_GAS_CONST = 8314;
                A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
                SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
                MACH = VEL0 / SOUND_VEL;

                if (MACH >= 0.7)
                {
                    MessageBox.Show("Flow is compressible please Edit the input parameters");
                    return;
                }
                RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
                if (RE <= 2300)
                {
                    a1 = Math.Pow((RE / 2712), 8.4);
                    a = (1.0 / (1 + a1));
                    a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                    b1 = Math.Pow(a1, 1.8);
                    b = (1.0 / (1 + b1));
                    a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                    FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
                }
                else
                {
                    a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                    a3 = Math.Log(RE / (1.816 * a2));
                    b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                    FRIC_FACT = Math.Pow((-2 * b1), -2);
                }
                ZETA = FRIC_FACT * LBYD;
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

                C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A0;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                DELP = (PST1 - PST0 * 1000);
                PST1 = PST1 / 1000;

                ShowOutPut(PST1, DELP);
                //printf("Edit Mass Flow Rate in Next Fittings due to Branch Flow");
            }
        }

        private void T_Run_to_Branch_Flow()
        {
            string cmbTypeT = Convert.ToString(cmbType.Text);
            if (cmbTypeT == "T Branching Flow Theta = 0 degree")
            {
                double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
                double VEL0, A0;
                double VISC;
                double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
                double ZETA, LENGTH_PIPE;
                double P_FRIC, C0, C1, C2;
                double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
                double FITTING_QUANTITY, LBYD = 2, K;
                // Input - Globle Variables: There should be button for the following globle inputs

                //printf("Enter the value of Mass flow rate in kg/s: ");
                //scanf("%lf", &MASS_FLOW_RATE);
                //printf("Enter the value of Temperature in Kelvin: ");
                //scanf("%lf", &TEMP);
                //printf("Enter the value of Molecular Weight in kg/kmol: ");
                //scanf("%lf", &MOLEQ_WEIGHT);
                //printf("Enter the value of ratio of Secific heat Gamma: ");
                //scanf("%lf", &GAMMA);

                MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
                MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
                TEMP = Convert.ToDouble(txtTEMP.Text);
                MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
                GAMMA = Convert.ToDouble(txtGAMMA.Text);

                // Viscosity will be calculated based on Temperature and Molecular Weight

                VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
                // However, User can edit the Viscosity 
                //printf("Enter the value of Viscoaity in Pas: ");
                //scanf("%lf", &VISC);

                VISC = Convert.ToDouble(txtVISC.Text);

                //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

                //printf("Enter the value of Pipe Diameter (Branch Dia d0) in m: ");
                //scanf("%lf", &PIPE_DIA);
                //printf("Enter the Quantity: ");
                //scanf("%lf", &FITTING_QUANTITY);
                //printf("Enter the value of Roughness in m: ");
                //scanf("%lf", &ROUGHNESS);

                PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);
                FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);
                ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

                PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
                ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);
                //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

                //printf("Enter the value of Upsteam Pressure in kPa: ");
                //scanf("%lf", &PST0);

                PST0 = Convert.ToDouble(txtPST0.Text);
                PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

                // All input ends here
                // ofstream file6("Output_Data.txt");
                UNIVERSAL_GAS_CONST = 8314;
                A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
                SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
                MACH = VEL0 / SOUND_VEL;

                if (MACH >= 0.7)
                {
                    MessageBox.Show("Flow is compressible please Edit the input parameters");
                    return;
                }
                RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
                if (RE <= 2300)
                {
                    a1 = Math.Pow((RE / 2712), 8.4);
                    a = (1.0 / (1 + a1));
                    a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                    b1 = Math.Pow(a1, 1.8);
                    b = (1.0 / (1 + b1));
                    a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                    FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
                }
                else
                {
                    a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                    a3 = Math.Log(RE / (1.816 * a2));
                    b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                    FRIC_FACT = Math.Pow((-2 * b1), -2);
                }
                ZETA = FRIC_FACT * LBYD;
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

                C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A0;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                DELP = (PST1 - PST0 * 1000);
                PST1 = PST1 / 1000;

                ShowOutPut(PST1, DELP);

                //printf("Edit Mass Flow Rate in Next Fittings due to Branch Flow");
            }
            else if (cmbTypeT == "T Branching Flow Theta = 15 degree")
            {
                double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
                double VEL0, A0;
                double VISC;
                double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
                double ZETA, LENGTH_PIPE;
                double P_FRIC, C0, C1, C2;
                double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
                double FITTING_QUANTITY, LBYD = 4, K;
                // Input - Globle Variables: There should be button for the following globle inputs

                //printf("Enter the value of Mass flow rate in kg/s: ");
                //scanf("%lf", &MASS_FLOW_RATE);
                //printf("Enter the value of Temperature in Kelvin: ");
                //scanf("%lf", &TEMP);
                //printf("Enter the value of Molecular Weight in kg/kmol: ");
                //scanf("%lf", &MOLEQ_WEIGHT);
                //printf("Enter the value of ratio of Secific heat Gamma: ");
                //scanf("%lf", &GAMMA);

                MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
                MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
                TEMP = Convert.ToDouble(txtTEMP.Text);
                MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
                GAMMA = Convert.ToDouble(txtGAMMA.Text);

                // Viscosity will be calculated based on Temperature and Molecular Weight

                VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
                // However, User can edit the Viscosity 
                //printf("Enter the value of Viscoaity in Pas: ");
                //scanf("%lf", &VISC);

                VISC = Convert.ToDouble(txtVISC.Text);

                //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

                //printf("Enter the value of Pipe Diameter (Branch Dia d0) in m: ");
                //scanf("%lf", &PIPE_DIA);
                //printf("Enter the Quantity: ");
                //scanf("%lf", &FITTING_QUANTITY);
                //printf("Enter the value of Roughness in m: ");
                //scanf("%lf", &ROUGHNESS);

                PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);
                FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);
                ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

                PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
                ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);
                //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

                //printf("Enter the value of Upsteam Pressure in kPa: ");
                //scanf("%lf", &PST0);

                PST0 = Convert.ToDouble(txtPST0.Text);
                PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

                // All input ends here
                // ofstream file6("Output_Data.txt");
                UNIVERSAL_GAS_CONST = 8314;
                A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
                SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
                MACH = VEL0 / SOUND_VEL;

                if (MACH >= 0.7)
                {
                    MessageBox.Show("Flow is compressible please Edit the input parameters");
                    return;
                }
                RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
                if (RE <= 2300)
                {
                    a1 = Math.Pow((RE / 2712), 8.4);
                    a = (1.0 / (1 + a1));
                    a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                    b1 = Math.Pow(a1, 1.8);
                    b = (1.0 / (1 + b1));
                    a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                    FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
                }
                else
                {
                    a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                    a3 = Math.Log(RE / (1.816 * a2));
                    b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                    FRIC_FACT = Math.Pow((-2 * b1), -2);
                }
                ZETA = FRIC_FACT * LBYD;
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

                C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A0;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                DELP = (PST1 - PST0 * 1000);
                PST1 = PST1 / 1000;

                ShowOutPut(PST1, DELP);
                //printf("Edit Mass Flow Rate in Next Fittings due to Branch Flow");
            }
            else if (cmbTypeT == "T Branching Flow Theta = 30 degree")
            {
                double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
                double VEL0, A0;
                double VISC;
                double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
                double ZETA, LENGTH_PIPE;
                double P_FRIC, C0, C1, C2;
                double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
                double FITTING_QUANTITY, LBYD = 8, K;
                // Input - Globle Variables: There should be button for the following globle inputs

                //printf("Enter the value of Mass flow rate in kg/s: ");
                //scanf("%lf", &MASS_FLOW_RATE);
                //printf("Enter the value of Temperature in Kelvin: ");
                //scanf("%lf", &TEMP);
                //printf("Enter the value of Molecular Weight in kg/kmol: ");
                //scanf("%lf", &MOLEQ_WEIGHT);
                //printf("Enter the value of ratio of Secific heat Gamma: ");
                //scanf("%lf", &GAMMA);

                MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
                MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
                TEMP = Convert.ToDouble(txtTEMP.Text);
                MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
                GAMMA = Convert.ToDouble(txtGAMMA.Text);

                // Viscosity will be calculated based on Temperature and Molecular Weight

                VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
                // However, User can edit the Viscosity 
                //printf("Enter the value of Viscoaity in Pas: ");
                //scanf("%lf", &VISC);

                VISC = Convert.ToDouble(txtVISC.Text);

                //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

                //printf("Enter the value of Pipe Diameter (Branch Dia d0) in m: ");
                //scanf("%lf", &PIPE_DIA);
                //printf("Enter the Quantity: ");
                //scanf("%lf", &FITTING_QUANTITY);
                //printf("Enter the value of Roughness in m: ");
                //scanf("%lf", &ROUGHNESS);

                PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);
                FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);
                ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

                PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
                ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);
                //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

                //printf("Enter the value of Upsteam Pressure in kPa: ");
                //scanf("%lf", &PST0);

                PST0 = Convert.ToDouble(txtPST0.Text);
                PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

                // All input ends here
                // ofstream file6("Output_Data.txt");
                UNIVERSAL_GAS_CONST = 8314;
                A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
                SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
                MACH = VEL0 / SOUND_VEL;

                if (MACH >= 0.7)
                {
                    MessageBox.Show("Flow is compressible please Edit the input parameters");
                    return;
                }
                RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
                if (RE <= 2300)
                {
                    a1 = Math.Pow((RE / 2712), 8.4);
                    a = (1.0 / (1 + a1));
                    a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                    b1 = Math.Pow(a1, 1.8);
                    b = (1.0 / (1 + b1));
                    a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                    FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
                }
                else
                {
                    a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                    a3 = Math.Log(RE / (1.816 * a2));
                    b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                    FRIC_FACT = Math.Pow((-2 * b1), -2);
                }
                ZETA = FRIC_FACT * LBYD;
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

                C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A0;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                DELP = (PST1 - PST0 * 1000);
                PST1 = PST1 / 1000;

                ShowOutPut(PST1, DELP);
                //printf("Edit Mass Flow Rate in Next Fittings due to Branch Flow");
            }
            else if (cmbTypeT == "T Branching Flow Theta = 45 degree")
            {
                double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
                double VEL0, A0;
                double VISC;
                double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
                double ZETA, LENGTH_PIPE;
                double P_FRIC, C0, C1, C2;
                double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
                double FITTING_QUANTITY, LBYD = 15, K;
                // Input - Globle Variables: There should be button for the following globle inputs

                //printf("Enter the value of Mass flow rate in kg/s: ");
                //scanf("%lf", &MASS_FLOW_RATE);
                //printf("Enter the value of Temperature in Kelvin: ");
                //scanf("%lf", &TEMP);
                //printf("Enter the value of Molecular Weight in kg/kmol: ");
                //scanf("%lf", &MOLEQ_WEIGHT);
                //printf("Enter the value of ratio of Secific heat Gamma: ");
                //scanf("%lf", &GAMMA);

                MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
                MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
                TEMP = Convert.ToDouble(txtTEMP.Text);
                MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
                GAMMA = Convert.ToDouble(txtGAMMA.Text);

                // Viscosity will be calculated based on Temperature and Molecular Weight

                VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
                // However, User can edit the Viscosity 
                //printf("Enter the value of Viscoaity in Pas: ");
                //scanf("%lf", &VISC);

                VISC = Convert.ToDouble(txtVISC.Text);

                //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

                //printf("Enter the value of Pipe Diameter (Branch Dia d0) in m: ");
                //scanf("%lf", &PIPE_DIA);
                //printf("Enter the Quantity: ");
                //scanf("%lf", &FITTING_QUANTITY);
                //printf("Enter the value of Roughness in m: ");
                //scanf("%lf", &ROUGHNESS);

                PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);
                FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);
                ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

                PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
                ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);

                //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

                //printf("Enter the value of Upsteam Pressure in kPa: ");
                //scanf("%lf", &PST0);

                PST0 = Convert.ToDouble(txtPST0.Text);
                PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

                // All input ends here
                // ofstream file6("Output_Data.txt");
                UNIVERSAL_GAS_CONST = 8314;
                A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
                SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
                MACH = VEL0 / SOUND_VEL;

                if (MACH >= 0.7)
                {
                    MessageBox.Show("Flow is compressible please Edit the input parameters");
                    return;
                }
                RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
                if (RE <= 2300)
                {
                    a1 = Math.Pow((RE / 2712), 8.4);
                    a = (1.0 / (1 + a1));
                    a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                    b1 = Math.Pow(a1, 1.8);
                    b = (1.0 / (1 + b1));
                    a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                    FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
                }
                else
                {
                    a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                    a3 = Math.Log(RE / (1.816 * a2));
                    b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                    FRIC_FACT = Math.Pow((-2 * b1), -2);
                }
                ZETA = FRIC_FACT * LBYD;
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

                C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A0;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                DELP = (PST1 - PST0 * 1000);
                PST1 = PST1 / 1000;

                ShowOutPut(PST1, DELP);
                //printf("Edit Mass Flow Rate in Next Fittings due to Branch Flow");
            }
            else if (cmbTypeT == "T Branching Flow Theta = 60 degree")
            {
                double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
                double VEL0, A0;
                double VISC;
                double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
                double ZETA, LENGTH_PIPE;
                double P_FRIC, C0, C1, C2;
                double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
                double FITTING_QUANTITY, LBYD = 25, K;
                // Input - Globle Variables: There should be button for the following globle inputs

                //printf("Enter the value of Mass flow rate in kg/s: ");
                //scanf("%lf", &MASS_FLOW_RATE);
                //printf("Enter the value of Temperature in Kelvin: ");
                //scanf("%lf", &TEMP);
                //printf("Enter the value of Molecular Weight in kg/kmol: ");
                //scanf("%lf", &MOLEQ_WEIGHT);
                //printf("Enter the value of ratio of Secific heat Gamma: ");
                //scanf("%lf", &GAMMA);

                MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
                MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
                TEMP = Convert.ToDouble(txtTEMP.Text);
                MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
                GAMMA = Convert.ToDouble(txtGAMMA.Text);

                // Viscosity will be calculated based on Temperature and Molecular Weight

                VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
                // However, User can edit the Viscosity 
                //printf("Enter the value of Viscoaity in Pas: ");
                //scanf("%lf", &VISC);

                VISC = Convert.ToDouble(txtVISC.Text);

                //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

                //printf("Enter the value of Pipe Diameter (Branch Dia d0) in m: ");
                //scanf("%lf", &PIPE_DIA);
                //printf("Enter the Quantity: ");
                //scanf("%lf", &FITTING_QUANTITY);
                //printf("Enter the value of Roughness in m: ");
                //scanf("%lf", &ROUGHNESS);

                PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);
                FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);
                ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

                PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
                ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);
                //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

                //printf("Enter the value of Upsteam Pressure in kPa: ");
                //scanf("%lf", &PST0);

                PST0 = Convert.ToDouble(txtPST0.Text);
                PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

                // All input ends here
                // ofstream file6("Output_Data.txt");
                UNIVERSAL_GAS_CONST = 8314;
                A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
                SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
                MACH = VEL0 / SOUND_VEL;

                if (MACH >= 0.7)
                {
                    MessageBox.Show("Flow is compressible please Edit the input parameters");
                    return;
                }
                RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
                if (RE <= 2300)
                {
                    a1 = Math.Pow((RE / 2712), 8.4);
                    a = (1.0 / (1 + a1));
                    a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                    b1 = Math.Pow(a1, 1.8);
                    b = (1.0 / (1 + b1));
                    a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                    FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
                }
                else
                {
                    a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                    a3 = Math.Log(RE / (1.816 * a2));
                    b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                    FRIC_FACT = Math.Pow((-2 * b1), -2);
                }
                ZETA = FRIC_FACT * LBYD;
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

                C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A0;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                DELP = (PST1 - PST0 * 1000);
                PST1 = PST1 / 1000;

                ShowOutPut(PST1, DELP);
                //printf("Edit Mass Flow Rate in Next Fittings due to Branch Flow");
            }
            else if (cmbTypeT == "T Branching Flow Theta = 75 degree")
            {
                double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
                double VEL0, A0;
                double VISC;
                double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
                double ZETA, LENGTH_PIPE;
                double P_FRIC, C0, C1, C2;
                double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
                double FITTING_QUANTITY, LBYD = 40, K;
                // Input - Globle Variables: There should be button for the following globle inputs

                //printf("Enter the value of Mass flow rate in kg/s: ");
                //scanf("%lf", &MASS_FLOW_RATE);
                //printf("Enter the value of Temperature in Kelvin: ");
                //scanf("%lf", &TEMP);
                //printf("Enter the value of Molecular Weight in kg/kmol: ");
                //scanf("%lf", &MOLEQ_WEIGHT);
                //printf("Enter the value of ratio of Secific heat Gamma: ");
                //scanf("%lf", &GAMMA);

                MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
                MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
                TEMP = Convert.ToDouble(txtTEMP.Text);
                MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
                GAMMA = Convert.ToDouble(txtGAMMA.Text);

                // Viscosity will be calculated based on Temperature and Molecular Weight

                VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
                // However, User can edit the Viscosity 
                //printf("Enter the value of Viscoaity in Pas: ");
                //scanf("%lf", &VISC);

                VISC = Convert.ToDouble(txtVISC.Text);

                //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

                //printf("Enter the value of Pipe Diameter (Branch Dia d0) in m: ");
                //scanf("%lf", &PIPE_DIA);
                //printf("Enter the Quantity: ");
                //scanf("%lf", &FITTING_QUANTITY);
                //printf("Enter the value of Roughness in m: ");
                //scanf("%lf", &ROUGHNESS);

                PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);
                FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);
                ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

                PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
                ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);
                //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

                //printf("Enter the value of Upsteam Pressure in kPa: ");
                //scanf("%lf", &PST0);

                PST0 = Convert.ToDouble(txtPST0.Text);
                PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

                // All input ends here
                // ofstream file6("Output_Data.txt");
                UNIVERSAL_GAS_CONST = 8314;
                A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
                SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
                MACH = VEL0 / SOUND_VEL;

                if (MACH >= 0.7)
                {
                    MessageBox.Show("Flow is compressible please Edit the input parameters");
                    return;
                }
                RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
                if (RE <= 2300)
                {
                    a1 = Math.Pow((RE / 2712), 8.4);
                    a = (1.0 / (1 + a1));
                    a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                    b1 = Math.Pow(a1, 1.8);
                    b = (1.0 / (1 + b1));
                    a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                    FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
                }
                else
                {
                    a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                    a3 = Math.Log(RE / (1.816 * a2));
                    b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                    FRIC_FACT = Math.Pow((-2 * b1), -2);
                }
                ZETA = FRIC_FACT * LBYD;
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

                C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A0;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                DELP = (PST1 - PST0 * 1000);
                PST1 = PST1 / 1000;

                ShowOutPut(PST1, DELP);
                //printf("Edit Mass Flow Rate in Next Fittings due to Branch Flow");
            }
            else if (cmbTypeT == "T Branching Flow Theta = 90 degree")
            {
                double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
                double VEL0, A0;
                double VISC;
                double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
                double ZETA, LENGTH_PIPE;
                double P_FRIC, C0, C1, C2;
                double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
                double FITTING_QUANTITY, LBYD = 60, K;
                // Input - Globle Variables: There should be button for the following globle inputs

                //printf("Enter the value of Mass flow rate in kg/s: ");
                //scanf("%lf", &MASS_FLOW_RATE);
                //printf("Enter the value of Temperature in Kelvin: ");
                //scanf("%lf", &TEMP);
                //printf("Enter the value of Molecular Weight in kg/kmol: ");
                //scanf("%lf", &MOLEQ_WEIGHT);
                //printf("Enter the value of ratio of Secific heat Gamma: ");
                //scanf("%lf", &GAMMA);

                MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
                MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
                TEMP = Convert.ToDouble(txtTEMP.Text);
                MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
                GAMMA = Convert.ToDouble(txtGAMMA.Text);

                // Viscosity will be calculated based on Temperature and Molecular Weight

                VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
                // However, User can edit the Viscosity 
                //printf("Enter the value of Viscoaity in Pas: ");
                //scanf("%lf", &VISC);

                VISC = Convert.ToDouble(txtVISC.Text);

                //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

                //printf("Enter the value of Pipe Diameter (Branch Dia d0) in m: ");
                //scanf("%lf", &PIPE_DIA);
                //printf("Enter the Quantity: ");
                //scanf("%lf", &FITTING_QUANTITY);
                //printf("Enter the value of Roughness in m: ");
                //scanf("%lf", &ROUGHNESS);

                PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);
                FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);
                ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

                PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
                ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);

                //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

                //printf("Enter the value of Upsteam Pressure in kPa: ");
                //scanf("%lf", &PST0);

                PST0 = Convert.ToDouble(txtPST0.Text);
                PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

                // All input ends here
                // ofstream file6("Output_Data.txt");
                UNIVERSAL_GAS_CONST = 8314;
                A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
                SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
                MACH = VEL0 / SOUND_VEL;

                if (MACH >= 0.7)
                {
                    MessageBox.Show("Flow is compressible please Edit the input parameters");
                    return;
                }
                RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
                if (RE <= 2300)
                {
                    a1 = Math.Pow((RE / 2712), 8.4);
                    a = (1.0 / (1 + a1));
                    a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                    b1 = Math.Pow(a1, 1.8);
                    b = (1.0 / (1 + b1));
                    a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                    FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
                }
                else
                {
                    a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                    a3 = Math.Log(RE / (1.816 * a2));
                    b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                    FRIC_FACT = Math.Pow((-2 * b1), -2);
                }
                ZETA = FRIC_FACT * LBYD;
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

                C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A0;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                DELP = (PST1 - PST0 * 1000);
                PST1 = PST1 / 1000;

                ShowOutPut(PST1, DELP);
                //printf("Edit Mass Flow Rate in Next Fittings due to Branch Flow");
            }
        }

        private void Gate_Valve()
        {
            string cmbTypeT = Convert.ToString(cmbType.Text);
            if (cmbTypeT == "Gate Valve 1by2 Open")
            {
                double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
                double VEL0, A0;
                double VISC;
                double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
                double ZETA, LENGTH_PIPE;
                double P_FRIC, C0, C1, C2;
                double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
                double FITTING_QUANTITY, LBYD = 200, K;
                // Input - Globle Variables: There should be button for the following globle inputs

                //printf("Enter the value of Mass flow rate in kg/s: ");
                //scanf("%lf", &MASS_FLOW_RATE);
                //printf("Enter the value of Temperature in Kelvin: ");
                //scanf("%lf", &TEMP);
                //printf("Enter the value of Molecular Weight in kg/kmol: ");
                //scanf("%lf", &MOLEQ_WEIGHT);
                //printf("Enter the value of ratio of Secific heat Gamma: ");
                //scanf("%lf", &GAMMA);

                MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
                MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
                TEMP = Convert.ToDouble(txtTEMP.Text);
                MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
                GAMMA = Convert.ToDouble(txtGAMMA.Text);

                // Viscosity will be calculated based on Temperature and Molecular Weight

                VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
                // However, User can edit the Viscosity 
                //printf("Enter the value of Viscoaity in Pas: ");
                //scanf("%lf", &VISC);

                VISC = Convert.ToDouble(txtVISC.Text);

                //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

                //printf("Enter the value of Pipe Diameter D in m: ");
                //scanf("%lf", &PIPE_DIA);
                //printf("Enter the Quantity: ");
                //scanf("%lf", &FITTING_QUANTITY);
                //printf("Enter the value of Roughness in m: ");
                //scanf("%lf", &ROUGHNESS);

                PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);
                FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);
                ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

                PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
                ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);
                //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

                //printf("Enter the value of Upsteam Pressure in kPa: ");
                //scanf("%lf", &PST0);

                PST0 = Convert.ToDouble(txtPST0.Text);
                PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

                // All input ends here
                // ofstream file6("Output_Data.txt");
                UNIVERSAL_GAS_CONST = 8314;
                A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
                SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
                MACH = VEL0 / SOUND_VEL;

                if (MACH >= 0.7)
                {
                    MessageBox.Show("Flow is compressible please Edit the input parameters");
                    return;
                }
                RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
                if (RE <= 2300)
                {
                    a1 = Math.Pow((RE / 2712), 8.4);
                    a = (1.0 / (1 + a1));
                    a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                    b1 = Math.Pow(a1, 1.8);
                    b = (1.0 / (1 + b1));
                    a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                    FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
                }
                else
                {
                    a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                    a3 = Math.Log(RE / (1.816 * a2));
                    b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                    FRIC_FACT = Math.Pow((-2 * b1), -2);
                }
                ZETA = FRIC_FACT * LBYD;
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

                C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A0;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                DELP = (PST1 - PST0 * 1000);
                PST1 = PST1 / 1000;

                ShowOutPut(PST1, DELP);
            }
            else if (cmbTypeT == "Gate Valve 1by4 Open")
            {
                double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
                double VEL0, A0;
                double VISC;
                double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
                double ZETA, LENGTH_PIPE;
                double P_FRIC, C0, C1, C2;
                double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
                double FITTING_QUANTITY, LBYD = 800, K;
                // Input - Globle Variables: There should be button for the following globle inputs

                //printf("Enter the value of Mass flow rate in kg/s: ");
                //scanf("%lf", &MASS_FLOW_RATE);
                //printf("Enter the value of Temperature in Kelvin: ");
                //scanf("%lf", &TEMP);
                //printf("Enter the value of Molecular Weight in kg/kmol: ");
                //scanf("%lf", &MOLEQ_WEIGHT);
                //printf("Enter the value of ratio of Secific heat Gamma: ");
                //scanf("%lf", &GAMMA);

                MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
                MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
                TEMP = Convert.ToDouble(txtTEMP.Text);
                MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
                GAMMA = Convert.ToDouble(txtGAMMA.Text);

                // Viscosity will be calculated based on Temperature and Molecular Weight

                VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
                // However, User can edit the Viscosity 
                //printf("Enter the value of Viscoaity in Pas: ");
                //scanf("%lf", &VISC);

                VISC = Convert.ToDouble(txtVISC.Text);

                //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

                //printf("Enter the value of Pipe Diameter D in m: ");
                //scanf("%lf", &PIPE_DIA);
                //printf("Enter the Quantity: ");
                //scanf("%lf", &FITTING_QUANTITY);
                //printf("Enter the value of Roughness in m: ");
                //scanf("%lf", &ROUGHNESS);

                PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);
                FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);
                ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

                PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
                ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);

                //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

                //printf("Enter the value of Upsteam Pressure in kPa: ");
                //scanf("%lf", &PST0);

                PST0 = Convert.ToDouble(txtPST0.Text);
                PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

                // All input ends here
                // ofstream file6("Output_Data.txt");
                UNIVERSAL_GAS_CONST = 8314;
                A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
                SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
                MACH = VEL0 / SOUND_VEL;

                if (MACH >= 0.7)
                {
                    MessageBox.Show("Flow is compressible please Edit the input parameters");
                    return;
                }
                RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
                if (RE <= 2300)
                {
                    a1 = Math.Pow((RE / 2712), 8.4);
                    a = (1.0 / (1 + a1));
                    a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                    b1 = Math.Pow(a1, 1.8);
                    b = (1.0 / (1 + b1));
                    a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                    FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
                }
                else
                {
                    a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                    a3 = Math.Log(RE / (1.816 * a2));
                    b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                    FRIC_FACT = Math.Pow((-2 * b1), -2);
                }
                ZETA = FRIC_FACT * LBYD;
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

                C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A0;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                DELP = (PST1 - PST0 * 1000);
                PST1 = PST1 / 1000;

                ShowOutPut(PST1, DELP);

                //printf("%lf", PST1);
                // printf("%lf", DELP);
                // file6<<"Pressure at Upstream"<<"\t"<<PST1<<endl;
                //  file6<<"Pressure Drop in Pascal"<<"\t"<<DELP<<endl;
                //return (PST1, DELP, DEN1);
            }
            else if (cmbTypeT == "Gate Valve 3by4 Open")
            {
                double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
                double VEL0, A0;
                double VISC;
                double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
                double ZETA, LENGTH_PIPE;
                double P_FRIC, C0, C1, C2;
                double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
                double FITTING_QUANTITY, LBYD = 40, K;
                // Input - Globle Variables: There should be button for the following globle inputs

                //printf("Enter the value of Mass flow rate in kg/s: ");
                //scanf("%lf", &MASS_FLOW_RATE);
                //printf("Enter the value of Temperature in Kelvin: ");
                //scanf("%lf", &TEMP);
                //printf("Enter the value of Molecular Weight in kg/kmol: ");
                //scanf("%lf", &MOLEQ_WEIGHT);
                //printf("Enter the value of ratio of Secific heat Gamma: ");
                //scanf("%lf", &GAMMA);

                MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
                MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
                TEMP = Convert.ToDouble(txtTEMP.Text);
                MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
                GAMMA = Convert.ToDouble(txtGAMMA.Text);

                // Viscosity will be calculated based on Temperature and Molecular Weight

                VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
                // However, User can edit the Viscosity 
                //printf("Enter the value of Viscoaity in Pas: ");
                //scanf("%lf", &VISC);

                VISC = Convert.ToDouble(txtVISC.Text);


                //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

                //printf("Enter the value of Pipe Diameter D in m: ");
                //scanf("%lf", &PIPE_DIA);
                //printf("Enter the Quantity: ");
                //scanf("%lf", &FITTING_QUANTITY);
                //printf("Enter the value of Roughness in m: ");
                //scanf("%lf", &ROUGHNESS);

                PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);
                FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);
                ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

                PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
                ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);
                //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

                //printf("Enter the value of Upsteam Pressure in kPa: ");
                //scanf("%lf", &PST0);

                PST0 = Convert.ToDouble(txtPST0.Text);
                PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);


                // All input ends here
                // ofstream file6("Output_Data.txt");
                UNIVERSAL_GAS_CONST = 8314;
                A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
                SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
                MACH = VEL0 / SOUND_VEL;

                if (MACH >= 0.7)
                {
                    MessageBox.Show("Flow is compressible please Edit the input parameters");
                    return;
                }
                RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
                if (RE <= 2300)
                {
                    a1 = Math.Pow((RE / 2712), 8.4);
                    a = (1.0 / (1 + a1));
                    a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                    b1 = Math.Pow(a1, 1.8);
                    b = (1.0 / (1 + b1));
                    a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                    FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
                }
                else
                {
                    a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                    a3 = Math.Log(RE / (1.816 * a2));
                    b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                    FRIC_FACT = Math.Pow((-2 * b1), -2);
                }
                ZETA = FRIC_FACT * LBYD;
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

                C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A0;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                DELP = (PST1 - PST0 * 1000);
                PST1 = PST1 / 1000;

                ShowOutPut(PST1, DELP);
            }
            else if (cmbTypeT == "Gate Valve Fully Open")
            {
                double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
                double VEL0, A0;
                double VISC;
                double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
                double ZETA, LENGTH_PIPE;
                double P_FRIC, C0, C1, C2;
                double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
                double FITTING_QUANTITY, LBYD = 7, K;
                // Input - Globle Variables: There should be button for the following globle inputs

                //printf("Enter the value of Mass flow rate in kg/s: ");
                //scanf("%lf", &MASS_FLOW_RATE);
                //printf("Enter the value of Temperature in Kelvin: ");
                //scanf("%lf", &TEMP);
                //printf("Enter the value of Molecular Weight in kg/kmol: ");
                //scanf("%lf", &MOLEQ_WEIGHT);
                //printf("Enter the value of ratio of Secific heat Gamma: ");
                //scanf("%lf", &GAMMA);

                MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
                MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
                TEMP = Convert.ToDouble(txtTEMP.Text);
                MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
                GAMMA = Convert.ToDouble(txtGAMMA.Text);

                // Viscosity will be calculated based on Temperature and Molecular Weight

                VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
                // However, User can edit the Viscosity 
                //printf("Enter the value of Viscoaity in Pas: ");
                //scanf("%lf", &VISC);

                VISC = Convert.ToDouble(txtVISC.Text);


                //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

                //printf("Enter the value of Pipe Diameter D in m: ");
                //scanf("%lf", &PIPE_DIA);
                //printf("Enter the Quantity: ");
                //scanf("%lf", &FITTING_QUANTITY);
                //printf("Enter the value of Roughness in m: ");
                //scanf("%lf", &ROUGHNESS);

                PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);
                FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);
                ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

                PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
                ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);

                //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

                //printf("Enter the value of Upsteam Pressure in kPa: ");
                //scanf("%lf", &PST0);

                PST0 = Convert.ToDouble(txtPST0.Text);
                PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

                // All input ends here
                // ofstream file6("Output_Data.txt");
                UNIVERSAL_GAS_CONST = 8314;
                A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
                SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
                MACH = VEL0 / SOUND_VEL;

                if (MACH >= 0.7)
                {
                    MessageBox.Show("Flow is compressible please Edit the input parameters");
                    return;
                }
                RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
                if (RE <= 2300)
                {
                    a1 = Math.Pow((RE / 2712), 8.4);
                    a = (1.0 / (1 + a1));
                    a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                    b1 = Math.Pow(a1, 1.8);
                    b = (1.0 / (1 + b1));
                    a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                    FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
                }
                else
                {
                    a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                    a3 = Math.Log(RE / (1.816 * a2));
                    b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                    FRIC_FACT = Math.Pow((-2 * b1), -2);
                }
                ZETA = FRIC_FACT * LBYD;
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

                C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A0;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                DELP = (PST1 - PST0 * 1000);
                PST1 = PST1 / 1000;

                ShowOutPut(PST1, DELP);
            }
        }

        private void Ball_Valve()
        {
            double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
            double VEL0, A0;
            double VISC;
            double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
            double ZETA, LENGTH_PIPE;
            double P_FRIC, C0, C1, C2;
            double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
            double FITTING_QUANTITY, LBYD = 3, K;
            // Input - Globle Variables: There should be button for the following globle inputs

            //printf("Enter the value of Mass flow rate in kg/s: ");
            //scanf("%lf", &MASS_FLOW_RATE);
            //printf("Enter the value of Temperature in Kelvin: ");
            //scanf("%lf", &TEMP);
            //printf("Enter the value of Molecular Weight in kg/kmol: ");
            //scanf("%lf", &MOLEQ_WEIGHT);
            //printf("Enter the value of ratio of Secific heat Gamma: ");
            //scanf("%lf", &GAMMA);

            MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
            MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
            TEMP = Convert.ToDouble(txtTEMP.Text);
            MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
            GAMMA = Convert.ToDouble(txtGAMMA.Text);

            // Viscosity will be calculated based on Temperature and Molecular Weight

            VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
            // However, User can edit the Viscosity 
            //printf("Enter the value of Viscoaity in Pas: ");
            //scanf("%lf", &VISC);

            VISC = Convert.ToDouble(txtVISC.Text);

            //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

            //printf("Enter the value of Pipe Diameter D in m: ");
            //scanf("%lf", &PIPE_DIA);
            //printf("Enter the Quantity: ");
            //scanf("%lf", &FITTING_QUANTITY);
            //printf("Enter the value of Roughness in m: ");
            //scanf("%lf", &ROUGHNESS);

            PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);
            FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);
            ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

            PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
            ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);
            //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

            //printf("Enter the value of Upsteam Pressure in kPa: ");
            //scanf("%lf", &PST0);

            PST0 = Convert.ToDouble(txtPST0.Text);
            PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

            // All input ends here
            // ofstream file6("Output_Data.txt");
            UNIVERSAL_GAS_CONST = 8314;
            A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
            VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
            SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
            MACH = VEL0 / SOUND_VEL;

            if (MACH >= 0.7)
            {
                MessageBox.Show("Flow is compressible please Edit the input parameters");
                return;
            }
            RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
            NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
            if (RE <= 2300)
            {
                a1 = Math.Pow((RE / 2712), 8.4);
                a = (1.0 / (1 + a1));
                a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                b1 = Math.Pow(a1, 1.8);
                b = (1.0 / (1 + b1));
                a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
            }
            else
            {
                a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                a3 = Math.Log(RE / (1.816 * a2));
                b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                FRIC_FACT = Math.Pow((-2 * b1), -2);
            }
            ZETA = FRIC_FACT * LBYD;
            P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

            C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
            C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
            C2 = MASS_FLOW_RATE / A0;
            PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
            DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
            DELP = (PST1 - PST0 * 1000);
            PST1 = PST1 / 1000;

            ShowOutPut(PST1, DELP);
        }

        private void Globe_Valve()
        {
            string cmbTypeT = Convert.ToString(cmbType.Text);
            if (cmbTypeT == "Globe Valve 1by2 Open")
            {
                double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
                double VEL0, A0;
                double VISC;
                double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
                double ZETA, LENGTH_PIPE;
                double P_FRIC, C0, C1, C2;
                double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
                double FITTING_QUANTITY, LBYD = 470, K;
                // Input - Globle Variables: There should be button for the following globle inputs

                //printf("Enter the value of Mass flow rate in kg/s: ");
                //scanf("%lf", &MASS_FLOW_RATE);
                //printf("Enter the value of Temperature in Kelvin: ");
                //scanf("%lf", &TEMP);
                //printf("Enter the value of Molecular Weight in kg/kmol: ");
                //scanf("%lf", &MOLEQ_WEIGHT);
                //printf("Enter the value of ratio of Secific heat Gamma: ");
                //scanf("%lf", &GAMMA);

                MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
                MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
                TEMP = Convert.ToDouble(txtTEMP.Text);
                MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
                GAMMA = Convert.ToDouble(txtGAMMA.Text);

                // Viscosity will be calculated based on Temperature and Molecular Weight

                VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
                // However, User can edit the Viscosity 
                //printf("Enter the value of Viscoaity in Pas: ");
                //scanf("%lf", &VISC);

                VISC = Convert.ToDouble(txtVISC.Text);

                //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

                //printf("Enter the value of Pipe Diameter D in m: ");
                //scanf("%lf", &PIPE_DIA);
                //printf("Enter the Quantity: ");
                //scanf("%lf", &FITTING_QUANTITY);
                //printf("Enter the value of Roughness in m: ");
                //scanf("%lf", &ROUGHNESS);

                PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);
                FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);
                ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

                PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
                ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);

                //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

                //printf("Enter the value of Upsteam Pressure in kPa: ");
                //scanf("%lf", &PST0);

                PST0 = Convert.ToDouble(txtPST0.Text);
                PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

                // All input ends here
                // ofstream file6("Output_Data.txt");
                UNIVERSAL_GAS_CONST = 8314;
                A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
                SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
                MACH = VEL0 / SOUND_VEL;

                if (MACH >= 0.7)
                {
                    MessageBox.Show("Flow is compressible please Edit the input parameters");
                    return;
                }
                RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
                if (RE <= 2300)
                {
                    a1 = Math.Pow((RE / 2712), 8.4);
                    a = (1.0 / (1 + a1));
                    a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                    b1 = Math.Pow(a1, 1.8);
                    b = (1.0 / (1 + b1));
                    a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                    FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
                }
                else
                {
                    a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                    a3 = Math.Log(RE / (1.816 * a2));
                    b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                    FRIC_FACT = Math.Pow((-2 * b1), -2);
                }
                ZETA = FRIC_FACT * LBYD;
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

                C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A0;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                DELP = (PST1 - PST0 * 1000);
                PST1 = PST1 / 1000;

                ShowOutPut(PST1, DELP);
            }
            else if (cmbTypeT == "Globe Valve Fully Open")
            {
                double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
                double VEL0, A0;
                double VISC;
                double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
                double ZETA, LENGTH_PIPE;
                double P_FRIC, C0, C1, C2;
                double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
                double FITTING_QUANTITY, LBYD = 330, K;
                // Input - Globle Variables: There should be button for the following globle inputs

                //printf("Enter the value of Mass flow rate in kg/s: ");
                //scanf("%lf", &MASS_FLOW_RATE);
                //printf("Enter the value of Temperature in Kelvin: ");
                //scanf("%lf", &TEMP);
                //printf("Enter the value of Molecular Weight in kg/kmol: ");
                //scanf("%lf", &MOLEQ_WEIGHT);
                //printf("Enter the value of ratio of Secific heat Gamma: ");
                //scanf("%lf", &GAMMA);

                MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
                MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
                TEMP = Convert.ToDouble(txtTEMP.Text);
                MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
                GAMMA = Convert.ToDouble(txtGAMMA.Text);

                // Viscosity will be calculated based on Temperature and Molecular Weight

                VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
                // However, User can edit the Viscosity 
                //printf("Enter the value of Viscoaity in Pas: ");
                //scanf("%lf", &VISC);

                VISC = Convert.ToDouble(txtVISC.Text);

                //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

                //printf("Enter the value of Pipe Diameter D in m: ");
                //scanf("%lf", &PIPE_DIA);
                //printf("Enter the Quantity: ");
                //scanf("%lf", &FITTING_QUANTITY);
                //printf("Enter the value of Roughness in m: ");
                //scanf("%lf", &ROUGHNESS);

                PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);
                FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);
                ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

                PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
                ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);
                //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

                //printf("Enter the value of Upsteam Pressure in kPa: ");
                //scanf("%lf", &PST0);

                PST0 = Convert.ToDouble(txtPST0.Text);
                PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

                // All input ends here
                // ofstream file6("Output_Data.txt");
                UNIVERSAL_GAS_CONST = 8314;
                A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
                SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
                MACH = VEL0 / SOUND_VEL;

                if (MACH >= 0.7)
                {
                    MessageBox.Show("Flow is compressible please Edit the input parameters");
                    return;
                }
                RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
                if (RE <= 2300)
                {
                    a1 = Math.Pow((RE / 2712), 8.4);
                    a = (1.0 / (1 + a1));
                    a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                    b1 = Math.Pow(a1, 1.8);
                    b = (1.0 / (1 + b1));
                    a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                    FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
                }
                else
                {
                    a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                    a3 = Math.Log(RE / (1.816 * a2));
                    b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                    FRIC_FACT = Math.Pow((-2 * b1), -2);
                }
                ZETA = FRIC_FACT * LBYD;
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

                C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A0;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                DELP = (PST1 - PST0 * 1000);
                PST1 = PST1 / 1000;

                ShowOutPut(PST1, DELP);
            }
        }

        private void Butterfly_Valve()
        {
            string cmbTypeT = Convert.ToString(cmbType.Text);
            if (cmbTypeT == "Butterfly Valve theta = 5 degree")
            {
                double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
                double VEL0, A0;
                double VISC;
                double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
                double ZETA, LENGTH_PIPE;
                double P_FRIC, C0, C1, C2;
                double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
                double FITTING_QUANTITY, LBYD = 10, K;
                // Input - Globle Variables: There should be button for the following globle inputs

                //printf("Enter the value of Mass flow rate in kg/s: ");
                //scanf("%lf", &MASS_FLOW_RATE);
                //printf("Enter the value of Temperature in Kelvin: ");
                //scanf("%lf", &TEMP);
                //printf("Enter the value of Molecular Weight in kg/kmol: ");
                //scanf("%lf", &MOLEQ_WEIGHT);
                //printf("Enter the value of ratio of Secific heat Gamma: ");
                //scanf("%lf", &GAMMA);

                MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
                MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
                TEMP = Convert.ToDouble(txtTEMP.Text);
                MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
                GAMMA = Convert.ToDouble(txtGAMMA.Text);

                // Viscosity will be calculated based on Temperature and Molecular Weight

                VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
                // However, User can edit the Viscosity 
                //printf("Enter the value of Viscoaity in Pas: ");
                //scanf("%lf", &VISC);

                VISC = Convert.ToDouble(txtVISC.Text);

                //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

                //printf("Enter the value of Pipe Diameter D in m: ");
                //scanf("%lf", &PIPE_DIA);
                //printf("Enter the Quantity: ");
                //scanf("%lf", &FITTING_QUANTITY);
                //printf("Enter the value of Roughness in m: ");
                //scanf("%lf", &ROUGHNESS);

                PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);
                FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);
                ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

                PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
                ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);
                //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

                //printf("Enter the value of Upsteam Pressure in kPa: ");
                //scanf("%lf", &PST0);

                PST0 = Convert.ToDouble(txtPST0.Text);
                PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

                // All input ends here
                // ofstream file6("Output_Data.txt");
                UNIVERSAL_GAS_CONST = 8314;
                A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
                SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
                MACH = VEL0 / SOUND_VEL;

                if (MACH >= 0.7)
                {
                    MessageBox.Show("Flow is compressible please Edit the input parameters");
                    return;
                }
                RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
                if (RE <= 2300)
                {
                    a1 = Math.Pow((RE / 2712), 8.4);
                    a = (1.0 / (1 + a1));
                    a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                    b1 = Math.Pow(a1, 1.8);
                    b = (1.0 / (1 + b1));
                    a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                    FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
                }
                else
                {
                    a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                    a3 = Math.Log(RE / (1.816 * a2));
                    b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                    FRIC_FACT = Math.Pow((-2 * b1), -2);
                }
                ZETA = FRIC_FACT * LBYD;
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

                C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A0;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                DELP = (PST1 - PST0 * 1000);
                PST1 = PST1 / 1000;

                ShowOutPut(PST1, DELP);
            }
            else if (cmbTypeT == "Butterfly Valve theta = 10 degree")
            {
                double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
                double VEL0, A0;
                double VISC;
                double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
                double ZETA, LENGTH_PIPE;
                double P_FRIC, C0, C1, C2;
                double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
                double FITTING_QUANTITY, LBYD = 22, K;
                // Input - Globle Variables: There should be button for the following globle inputs

                //printf("Enter the value of Mass flow rate in kg/s: ");
                //scanf("%lf", &MASS_FLOW_RATE);
                //printf("Enter the value of Temperature in Kelvin: ");
                //scanf("%lf", &TEMP);
                //printf("Enter the value of Molecular Weight in kg/kmol: ");
                //scanf("%lf", &MOLEQ_WEIGHT);
                //printf("Enter the value of ratio of Secific heat Gamma: ");
                //scanf("%lf", &GAMMA);

                MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
                MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
                TEMP = Convert.ToDouble(txtTEMP.Text);
                MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
                GAMMA = Convert.ToDouble(txtGAMMA.Text);

                // Viscosity will be calculated based on Temperature and Molecular Weight

                VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
                // However, User can edit the Viscosity 
                //printf("Enter the value of Viscoaity in Pas: ");
                //scanf("%lf", &VISC);

                VISC = Convert.ToDouble(txtVISC.Text);

                //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

                //printf("Enter the value of Pipe Diameter D in m: ");
                //scanf("%lf", &PIPE_DIA);
                //printf("Enter the Quantity: ");
                //scanf("%lf", &FITTING_QUANTITY);
                //printf("Enter the value of Roughness in m: ");
                //scanf("%lf", &ROUGHNESS);

                PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);
                FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);
                ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

                PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
                ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);
                //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

                //printf("Enter the value of Upsteam Pressure in kPa: ");
                //scanf("%lf", &PST0);

                PST0 = Convert.ToDouble(txtPST0.Text);
                PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

                // All input ends here
                // ofstream file6("Output_Data.txt");
                UNIVERSAL_GAS_CONST = 8314;
                A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
                SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
                MACH = VEL0 / SOUND_VEL;

                if (MACH >= 0.7)
                {
                    MessageBox.Show("Flow is compressible please Edit the input parameters");
                    return;
                }
                RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
                if (RE <= 2300)
                {
                    a1 = Math.Pow((RE / 2712), 8.4);
                    a = (1.0 / (1 + a1));
                    a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                    b1 = Math.Pow(a1, 1.8);
                    b = (1.0 / (1 + b1));
                    a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                    FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
                }
                else
                {
                    a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                    a3 = Math.Log(RE / (1.816 * a2));
                    b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                    FRIC_FACT = Math.Pow((-2 * b1), -2);
                }
                ZETA = FRIC_FACT * LBYD;
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

                C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A0;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                DELP = (PST1 - PST0 * 1000);
                PST1 = PST1 / 1000;

                ShowOutPut(PST1, DELP);
            }
            else if (cmbTypeT == "Butterfly Valve theta = 20 degree")
            {
                double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
                double VEL0, A0;
                double VISC;
                double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
                double ZETA, LENGTH_PIPE;
                double P_FRIC, C0, C1, C2;
                double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
                double FITTING_QUANTITY, LBYD = 64, K;
                // Input - Globle Variables: There should be button for the following globle inputs

                //printf("Enter the value of Mass flow rate in kg/s: ");
                //scanf("%lf", &MASS_FLOW_RATE);
                //printf("Enter the value of Temperature in Kelvin: ");
                //scanf("%lf", &TEMP);
                //printf("Enter the value of Molecular Weight in kg/kmol: ");
                //scanf("%lf", &MOLEQ_WEIGHT);
                //printf("Enter the value of ratio of Secific heat Gamma: ");
                //scanf("%lf", &GAMMA);

                MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
                MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
                TEMP = Convert.ToDouble(txtTEMP.Text);
                MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
                GAMMA = Convert.ToDouble(txtGAMMA.Text);

                // Viscosity will be calculated based on Temperature and Molecular Weight

                VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
                // However, User can edit the Viscosity 
                //printf("Enter the value of Viscoaity in Pas: ");
                //scanf("%lf", &VISC);

                VISC = Convert.ToDouble(txtVISC.Text);

                //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

                //printf("Enter the value of Pipe Diameter D in m: ");
                //scanf("%lf", &PIPE_DIA);
                //printf("Enter the Quantity: ");
                //scanf("%lf", &FITTING_QUANTITY);
                //printf("Enter the value of Roughness in m: ");
                //scanf("%lf", &ROUGHNESS);

                PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);
                FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);
                ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

                PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
                ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);
                //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

                //printf("Enter the value of Upsteam Pressure in kPa: ");
                //scanf("%lf", &PST0);

                PST0 = Convert.ToDouble(txtPST0.Text);
                PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

                // All input ends here
                // ofstream file6("Output_Data.txt");
                UNIVERSAL_GAS_CONST = 8314;
                A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
                SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
                MACH = VEL0 / SOUND_VEL;

                if (MACH >= 0.7)
                {
                    MessageBox.Show("Flow is compressible please Edit the input parameters");
                    return;
                }
                RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
                if (RE <= 2300)
                {
                    a1 = Math.Pow((RE / 2712), 8.4);
                    a = (1.0 / (1 + a1));
                    a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                    b1 = Math.Pow(a1, 1.8);
                    b = (1.0 / (1 + b1));
                    a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                    FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
                }
                else
                {
                    a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                    a3 = Math.Log(RE / (1.816 * a2));
                    b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                    FRIC_FACT = Math.Pow((-2 * b1), -2);
                }
                ZETA = FRIC_FACT * LBYD;
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

                C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A0;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                DELP = (PST1 - PST0 * 1000);
                PST1 = PST1 / 1000;

                ShowOutPut(PST1, DELP);
            }
            else if (cmbTypeT == "Butterfly Valve theta = 40 degree")
            {
                double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
                double VEL0, A0;
                double VISC;
                double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
                double ZETA, LENGTH_PIPE;
                double P_FRIC, C0, C1, C2;
                double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
                double FITTING_QUANTITY, LBYD = 450, K;
                // Input - Globle Variables: There should be button for the following globle inputs

                //printf("Enter the value of Mass flow rate in kg/s: ");
                //scanf("%lf", &MASS_FLOW_RATE);
                //printf("Enter the value of Temperature in Kelvin: ");
                //scanf("%lf", &TEMP);
                //printf("Enter the value of Molecular Weight in kg/kmol: ");
                //scanf("%lf", &MOLEQ_WEIGHT);
                //printf("Enter the value of ratio of Secific heat Gamma: ");
                //scanf("%lf", &GAMMA);

                MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
                MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
                TEMP = Convert.ToDouble(txtTEMP.Text);
                MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
                GAMMA = Convert.ToDouble(txtGAMMA.Text);

                // Viscosity will be calculated based on Temperature and Molecular Weight

                VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
                // However, User can edit the Viscosity 
                //printf("Enter the value of Viscoaity in Pas: ");
                //scanf("%lf", &VISC);

                VISC = Convert.ToDouble(txtVISC.Text);

                //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

                //printf("Enter the value of Pipe Diameter D in m: ");
                //scanf("%lf", &PIPE_DIA);
                //printf("Enter the Quantity: ");
                //scanf("%lf", &FITTING_QUANTITY);
                //printf("Enter the value of Roughness in m: ");
                //scanf("%lf", &ROUGHNESS);

                PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);
                FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);
                ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

                PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
                ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);
                //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

                //printf("Enter the value of Upsteam Pressure in kPa: ");
                //scanf("%lf", &PST0);

                PST0 = Convert.ToDouble(txtPST0.Text);
                PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

                // All input ends here
                // ofstream file6("Output_Data.txt");
                UNIVERSAL_GAS_CONST = 8314;
                A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
                SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
                MACH = VEL0 / SOUND_VEL;

                if (MACH >= 0.7)
                {
                    MessageBox.Show("Flow is compressible please Edit the input parameters");
                    return;
                }
                RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
                if (RE <= 2300)
                {
                    a1 = Math.Pow((RE / 2712), 8.4);
                    a = (1.0 / (1 + a1));
                    a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                    b1 = Math.Pow(a1, 1.8);
                    b = (1.0 / (1 + b1));
                    a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                    FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
                }
                else
                {
                    a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                    a3 = Math.Log(RE / (1.816 * a2));
                    b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                    FRIC_FACT = Math.Pow((-2 * b1), -2);
                }
                ZETA = FRIC_FACT * LBYD;
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

                C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A0;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                DELP = (PST1 - PST0 * 1000);
                PST1 = PST1 / 1000;

                ShowOutPut(PST1, DELP);
            }
            else if (cmbTypeT == "Butterfly Valve theta = 60 degree")
            {
                double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
                double VEL0, A0;
                double VISC;
                double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
                double ZETA, LENGTH_PIPE;
                double P_FRIC, C0, C1, C2;
                double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
                double FITTING_QUANTITY, LBYD = 4920, K;
                // Input - Globle Variables: There should be button for the following globle inputs

                //printf("Enter the value of Mass flow rate in kg/s: ");
                //scanf("%lf", &MASS_FLOW_RATE);
                //printf("Enter the value of Temperature in Kelvin: ");
                //scanf("%lf", &TEMP);
                //printf("Enter the value of Molecular Weight in kg/kmol: ");
                //scanf("%lf", &MOLEQ_WEIGHT);
                //printf("Enter the value of ratio of Secific heat Gamma: ");
                //scanf("%lf", &GAMMA);

                MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
                MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
                TEMP = Convert.ToDouble(txtTEMP.Text);
                MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
                GAMMA = Convert.ToDouble(txtGAMMA.Text);

                // Viscosity will be calculated based on Temperature and Molecular Weight

                VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
                // However, User can edit the Viscosity 
                //printf("Enter the value of Viscoaity in Pas: ");
                //scanf("%lf", &VISC);

                VISC = Convert.ToDouble(txtVISC.Text);

                //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

                //printf("Enter the value of Pipe Diameter D in m: ");
                //scanf("%lf", &PIPE_DIA);
                //printf("Enter the Quantity: ");
                //scanf("%lf", &FITTING_QUANTITY);
                //printf("Enter the value of Roughness in m: ");
                //scanf("%lf", &ROUGHNESS);

                PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);
                FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);
                ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

                PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
                ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);
                //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

                //printf("Enter the value of Upsteam Pressure in kPa: ");
                //scanf("%lf", &PST0);

                PST0 = Convert.ToDouble(txtPST0.Text);
                PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

                // All input ends here
                // ofstream file6("Output_Data.txt");
                UNIVERSAL_GAS_CONST = 8314;
                A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
                SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
                MACH = VEL0 / SOUND_VEL;

                if (MACH >= 0.7)
                {
                    MessageBox.Show("Flow is compressible please Edit the input parameters");
                    return;
                }
                RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
                if (RE <= 2300)
                {
                    a1 = Math.Pow((RE / 2712), 8.4);
                    a = (1.0 / (1 + a1));
                    a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                    b1 = Math.Pow(a1, 1.8);
                    b = (1.0 / (1 + b1));
                    a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                    FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
                }
                else
                {
                    a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                    a3 = Math.Log(RE / (1.816 * a2));
                    b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                    FRIC_FACT = Math.Pow((-2 * b1), -2);
                }
                ZETA = FRIC_FACT * LBYD;
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

                C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A0;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                DELP = (PST1 - PST0 * 1000);
                PST1 = PST1 / 1000;

                ShowOutPut(PST1, DELP);
            }
        }


        private void Check_Valve()
        {
            string cmbTypeT = Convert.ToString(cmbType.Text);
            if (cmbTypeT == "Check Valve Ball")
            {
                double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
                double VEL0, A0;
                double VISC;
                double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
                double ZETA, LENGTH_PIPE;
                double P_FRIC, C0, C1, C2;
                double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
                double FITTING_QUANTITY, LBYD = 3500, K;
                // Input - Globle Variables: There should be button for the following globle inputs

                //printf("Enter the value of Mass flow rate in kg/s: ");
                //scanf("%lf", &MASS_FLOW_RATE);
                //printf("Enter the value of Temperature in Kelvin: ");
                //scanf("%lf", &TEMP);
                //printf("Enter the value of Molecular Weight in kg/kmol: ");
                //scanf("%lf", &MOLEQ_WEIGHT);
                //printf("Enter the value of ratio of Secific heat Gamma: ");
                //scanf("%lf", &GAMMA);

                MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
                MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
                TEMP = Convert.ToDouble(txtTEMP.Text);
                MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
                GAMMA = Convert.ToDouble(txtGAMMA.Text);

                // Viscosity will be calculated based on Temperature and Molecular Weight

                VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
                // However, User can edit the Viscosity 
                //printf("Enter the value of Viscoaity in Pas: ");
                //scanf("%lf", &VISC);

                VISC = Convert.ToDouble(txtVISC.Text);

                //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

                //printf("Enter the value of Pipe Diameter D in m: ");
                //scanf("%lf", &PIPE_DIA);
                //printf("Enter the Quantity: ");
                //scanf("%lf", &FITTING_QUANTITY);
                //printf("Enter the value of Roughness in m: ");
                //scanf("%lf", &ROUGHNESS);

                PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);
                FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);
                ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

                PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
                ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);
                //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

                //printf("Enter the value of Upsteam Pressure in kPa: ");
                //scanf("%lf", &PST0);

                PST0 = Convert.ToDouble(txtPST0.Text);
                PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

                // All input ends here
                // ofstream file6("Output_Data.txt");
                UNIVERSAL_GAS_CONST = 8314;
                A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
                SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
                MACH = VEL0 / SOUND_VEL;

                if (MACH >= 0.7)
                {
                    MessageBox.Show("Flow is compressible please Edit the input parameters");
                    return;
                }
                RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
                if (RE <= 2300)
                {
                    a1 = Math.Pow((RE / 2712), 8.4);
                    a = (1.0 / (1 + a1));
                    a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                    b1 = Math.Pow(a1, 1.8);
                    b = (1.0 / (1 + b1));
                    a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                    FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
                }
                else
                {
                    a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                    a3 = Math.Log(RE / (1.816 * a2));
                    b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                    FRIC_FACT = Math.Pow((-2 * b1), -2);
                }
                ZETA = FRIC_FACT * LBYD;
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

                C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A0;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                DELP = (PST1 - PST0 * 1000);
                PST1 = PST1 / 1000;

                ShowOutPut(PST1, DELP);
            }
            else if (cmbTypeT == "Check Valve Disc")
            {
                double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
                double VEL0, A0;
                double VISC;
                double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
                double ZETA, LENGTH_PIPE;
                double P_FRIC, C0, C1, C2;
                double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
                double FITTING_QUANTITY, LBYD = 500, K;
                // Input - Globle Variables: There should be button for the following globle inputs

                //printf("Enter the value of Mass flow rate in kg/s: ");
                //scanf("%lf", &MASS_FLOW_RATE);
                //printf("Enter the value of Temperature in Kelvin: ");
                //scanf("%lf", &TEMP);
                //printf("Enter the value of Molecular Weight in kg/kmol: ");
                //scanf("%lf", &MOLEQ_WEIGHT);
                //printf("Enter the value of ratio of Secific heat Gamma: ");
                //scanf("%lf", &GAMMA);

                MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
                MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
                TEMP = Convert.ToDouble(txtTEMP.Text);
                MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
                GAMMA = Convert.ToDouble(txtGAMMA.Text);

                // Viscosity will be calculated based on Temperature and Molecular Weight

                VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
                // However, User can edit the Viscosity 
                //printf("Enter the value of Viscoaity in Pas: ");
                //scanf("%lf", &VISC);

                VISC = Convert.ToDouble(txtVISC.Text);

                //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

                //printf("Enter the value of Pipe Diameter D in m: ");
                //scanf("%lf", &PIPE_DIA);
                //printf("Enter the Quantity: ");
                //scanf("%lf", &FITTING_QUANTITY);
                //printf("Enter the value of Roughness in m: ");
                //scanf("%lf", &ROUGHNESS);

                PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);
                FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);
                ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

                PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
                ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);
                //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

                //printf("Enter the value of Upsteam Pressure in kPa: ");
                //scanf("%lf", &PST0);

                PST0 = Convert.ToDouble(txtPST0.Text);
                PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

                // All input ends here
                // ofstream file6("Output_Data.txt");
                UNIVERSAL_GAS_CONST = 8314;
                A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
                SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
                MACH = VEL0 / SOUND_VEL;

                if (MACH >= 0.7)
                {
                    MessageBox.Show("Flow is compressible please Edit the input parameters");
                    return;
                }
                RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
                if (RE <= 2300)
                {
                    a1 = Math.Pow((RE / 2712), 8.4);
                    a = (1.0 / (1 + a1));
                    a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                    b1 = Math.Pow(a1, 1.8);
                    b = (1.0 / (1 + b1));
                    a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                    FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
                }
                else
                {
                    a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                    a3 = Math.Log(RE / (1.816 * a2));
                    b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                    FRIC_FACT = Math.Pow((-2 * b1), -2);
                }
                ZETA = FRIC_FACT * LBYD;
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

                C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A0;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                DELP = (PST1 - PST0 * 1000);
                PST1 = PST1 / 1000;

                ShowOutPut(PST1, DELP);
            }
            else if (cmbTypeT == "Check Valve Swing")
            {
                double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
                double VEL0, A0;
                double VISC;
                double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
                double ZETA, LENGTH_PIPE;
                double P_FRIC, C0, C1, C2;
                double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
                double FITTING_QUANTITY, LBYD = 110, K;
                // Input - Globle Variables: There should be button for the following globle inputs

                //printf("Enter the value of Mass flow rate in kg/s: ");
                //scanf("%lf", &MASS_FLOW_RATE);
                //printf("Enter the value of Temperature in Kelvin: ");
                //scanf("%lf", &TEMP);
                //printf("Enter the value of Molecular Weight in kg/kmol: ");
                //scanf("%lf", &MOLEQ_WEIGHT);
                //printf("Enter the value of ratio of Secific heat Gamma: ");
                //scanf("%lf", &GAMMA);

                MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
                MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
                TEMP = Convert.ToDouble(txtTEMP.Text);
                MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
                GAMMA = Convert.ToDouble(txtGAMMA.Text);

                // Viscosity will be calculated based on Temperature and Molecular Weight

                VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
                // However, User can edit the Viscosity 
                //printf("Enter the value of Viscoaity in Pas: ");
                //scanf("%lf", &VISC);

                VISC = Convert.ToDouble(txtVISC.Text);


                //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

                //printf("Enter the value of Pipe Diameter D in m: ");
                //scanf("%lf", &PIPE_DIA);
                //printf("Enter the Quantity: ");
                //scanf("%lf", &FITTING_QUANTITY);
                //printf("Enter the value of Roughness in m: ");
                //scanf("%lf", &ROUGHNESS);

                PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);
                FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);
                ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

                PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
                ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);
                //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

                //printf("Enter the value of Upsteam Pressure in kPa: ");
                //scanf("%lf", &PST0);

                PST0 = Convert.ToDouble(txtPST0.Text);
                PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

                // All input ends here
                // ofstream file6("Output_Data.txt");
                UNIVERSAL_GAS_CONST = 8314;
                A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
                SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
                MACH = VEL0 / SOUND_VEL;

                if (MACH >= 0.7)
                {
                    MessageBox.Show("Flow is compressible please Edit the input parameters");
                    return;
                }
                RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
                if (RE <= 2300)
                {
                    a1 = Math.Pow((RE / 2712), 8.4);
                    a = (1.0 / (1 + a1));
                    a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                    b1 = Math.Pow(a1, 1.8);
                    b = (1.0 / (1 + b1));
                    a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                    FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
                }
                else
                {
                    a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                    a3 = Math.Log(RE / (1.816 * a2));
                    b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                    FRIC_FACT = Math.Pow((-2 * b1), -2);
                }
                ZETA = FRIC_FACT * LBYD;
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

                C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A0;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                DELP = (PST1 - PST0 * 1000);
                PST1 = PST1 / 1000;

                ShowOutPut(PST1, DELP);
            }
        }

        private void Angle_Valve()
        {
            double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
            double VEL0, A0;
            double VISC;
            double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
            double ZETA, LENGTH_PIPE;
            double P_FRIC, C0, C1, C2;
            double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
            double FITTING_QUANTITY, LBYD = 150, K;
            // Input - Globle Variables: There should be button for the following globle inputs

            //printf("Enter the value of Mass flow rate in kg/s: ");
            //scanf("%lf", &MASS_FLOW_RATE);
            //printf("Enter the value of Temperature in Kelvin: ");
            //scanf("%lf", &TEMP);
            //printf("Enter the value of Molecular Weight in kg/kmol: ");
            //scanf("%lf", &MOLEQ_WEIGHT);
            //printf("Enter the value of ratio of Secific heat Gamma: ");
            //scanf("%lf", &GAMMA);

            MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
            MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
            TEMP = Convert.ToDouble(txtTEMP.Text);
            MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
            GAMMA = Convert.ToDouble(txtGAMMA.Text);

            // Viscosity will be calculated based on Temperature and Molecular Weight

            VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
            // However, User can edit the Viscosity 
            //printf("Enter the value of Viscoaity in Pas: ");
            //scanf("%lf", &VISC);

            VISC = Convert.ToDouble(txtVISC.Text);

            //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

            //printf("Enter the value of Pipe Diameter D in m: ");
            //scanf("%lf", &PIPE_DIA);
            //printf("Enter the Quantity: ");
            //scanf("%lf", &FITTING_QUANTITY);
            //printf("Enter the value of Roughness in m: ");
            //scanf("%lf", &ROUGHNESS);

            PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);
            FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);
            ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

            PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
            ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);
            //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

            //printf("Enter the value of Upsteam Pressure in kPa: ");
            //scanf("%lf", &PST0);

            PST0 = Convert.ToDouble(txtPST0.Text);
            PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

            // All input ends here
            // ofstream file6("Output_Data.txt");
            UNIVERSAL_GAS_CONST = 8314;
            A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
            VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
            SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
            MACH = VEL0 / SOUND_VEL;

            if (MACH >= 0.7)
            {
                MessageBox.Show("Flow is compressible please Edit the input parameters");
                return;
            }
            RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
            NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
            if (RE <= 2300)
            {
                a1 = Math.Pow((RE / 2712), 8.4);
                a = (1.0 / (1 + a1));
                a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                b1 = Math.Pow(a1, 1.8);
                b = (1.0 / (1 + b1));
                a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
            }
            else
            {
                a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                a3 = Math.Log(RE / (1.816 * a2));
                b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                FRIC_FACT = Math.Pow((-2 * b1), -2);
            }
            ZETA = FRIC_FACT * LBYD;
            P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

            C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
            C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
            C2 = MASS_FLOW_RATE / A0;
            PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
            DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
            DELP = (PST1 - PST0 * 1000);
            PST1 = PST1 / 1000;

            ShowOutPut(PST1, DELP);
        }

        private void Coupling()
        {
            double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
            double VEL0, A0;
            double VISC;
            double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
            double ZETA, LENGTH_PIPE;
            double P_FRIC, C0, C1, C2;
            double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
            double FITTING_QUANTITY, LBYD = 2, K;
            // Input - Globle Variables: There should be button for the following globle inputs

            //printf("Enter the value of Mass flow rate in kg/s: ");
            //scanf("%lf", &MASS_FLOW_RATE);
            //printf("Enter the value of Temperature in Kelvin: ");
            //scanf("%lf", &TEMP);
            //printf("Enter the value of Molecular Weight in kg/kmol: ");
            //scanf("%lf", &MOLEQ_WEIGHT);
            //printf("Enter the value of ratio of Secific heat Gamma: ");
            //scanf("%lf", &GAMMA);

            MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
            MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
            TEMP = Convert.ToDouble(txtTEMP.Text);
            MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
            GAMMA = Convert.ToDouble(txtGAMMA.Text);

            // Viscosity will be calculated based on Temperature and Molecular Weight

            VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
            // However, User can edit the Viscosity 
            //printf("Enter the value of Viscoaity in Pas: ");
            //scanf("%lf", &VISC);

            VISC = Convert.ToDouble(txtVISC.Text);

            //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

            //printf("Enter the value of Pipe Diameter D in m: ");
            //scanf("%lf", &PIPE_DIA);
            //printf("Enter the Quantity: ");
            //scanf("%lf", &FITTING_QUANTITY);
            //printf("Enter the value of Roughness in m: ");
            //scanf("%lf", &ROUGHNESS);

            PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);
            FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);
            ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

            PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
            ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);
            //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

            //printf("Enter the value of Upsteam Pressure in kPa: ");
            //scanf("%lf", &PST0);

            PST0 = Convert.ToDouble(txtPST0.Text);
            PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

            // All input ends here
            // ofstream file6("Output_Data.txt");
            UNIVERSAL_GAS_CONST = 8314;
            A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
            VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
            SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
            MACH = VEL0 / SOUND_VEL;

            if (MACH >= 0.7)
            {
                MessageBox.Show("Flow is compressible please Edit the input parameters");
                return;
            }
            RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
            NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
            if (RE <= 2300)
            {
                a1 = Math.Pow((RE / 2712), 8.4);
                a = (1.0 / (1 + a1));
                a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                b1 = Math.Pow(a1, 1.8);
                b = (1.0 / (1 + b1));
                a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
            }
            else
            {
                a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                a3 = Math.Log(RE / (1.816 * a2));
                b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                FRIC_FACT = Math.Pow((-2 * b1), -2);
            }
            ZETA = FRIC_FACT * LBYD;
            P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

            C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
            C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
            C2 = MASS_FLOW_RATE / A0;
            PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
            DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
            DELP = (PST1 - PST0 * 1000);
            PST1 = PST1 / 1000;

            ShowOutPut(PST1, DELP);
        }

        private void Diaphram_Valve()
        {
            string cmbTypeT = Convert.ToString(cmbType.Text);
            if (cmbTypeT == "Diaphram Valve 1by2 Open")
            {
                double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
                double VEL0, A0;
                double VISC;
                double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
                double ZETA, LENGTH_PIPE;
                double P_FRIC, C0, C1, C2;
                double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
                double FITTING_QUANTITY, LBYD = 235, K;
                // Input - Globle Variables: There should be button for the following globle inputs

                //printf("Enter the value of Mass flow rate in kg/s: ");
                //scanf("%lf", &MASS_FLOW_RATE);
                //printf("Enter the value of Temperature in Kelvin: ");
                //scanf("%lf", &TEMP);
                //printf("Enter the value of Molecular Weight in kg/kmol: ");
                //scanf("%lf", &MOLEQ_WEIGHT);
                //printf("Enter the value of ratio of Secific heat Gamma: ");
                //scanf("%lf", &GAMMA);

                MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
                MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
                TEMP = Convert.ToDouble(txtTEMP.Text);
                MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
                GAMMA = Convert.ToDouble(txtGAMMA.Text);

                // Viscosity will be calculated based on Temperature and Molecular Weight

                VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
                // However, User can edit the Viscosity 
                //printf("Enter the value of Viscoaity in Pas: ");
                //scanf("%lf", &VISC);

                VISC = Convert.ToDouble(txtVISC.Text);

                //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

                //printf("Enter the value of Pipe Diameter D in m: ");
                //scanf("%lf", &PIPE_DIA);
                //printf("Enter the Quantity: ");
                //scanf("%lf", &FITTING_QUANTITY);
                //printf("Enter the value of Roughness in m: ");
                //scanf("%lf", &ROUGHNESS);

                PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);
                FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);
                ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

                PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
                ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);
                //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

                //printf("Enter the value of Upsteam Pressure in kPa: ");
                //scanf("%lf", &PST0);

                PST0 = Convert.ToDouble(txtPST0.Text);
                PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

                // All input ends here
                // ofstream file6("Output_Data.txt");
                UNIVERSAL_GAS_CONST = 8314;
                A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
                SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
                MACH = VEL0 / SOUND_VEL;

                if (MACH >= 0.7)
                {
                    MessageBox.Show("Flow is compressible please Edit the input parameters");
                    return;
                }
                RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
                if (RE <= 2300)
                {
                    a1 = Math.Pow((RE / 2712), 8.4);
                    a = (1.0 / (1 + a1));
                    a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                    b1 = Math.Pow(a1, 1.8);
                    b = (1.0 / (1 + b1));
                    a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                    FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
                }
                else
                {
                    a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                    a3 = Math.Log(RE / (1.816 * a2));
                    b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                    FRIC_FACT = Math.Pow((-2 * b1), -2);
                }
                ZETA = FRIC_FACT * LBYD;
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

                C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A0;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                DELP = (PST1 - PST0 * 1000);
                PST1 = PST1 / 1000;

                ShowOutPut(PST1, DELP);
            }
            else if (cmbTypeT == "Diaphram Valve 1by4 Open")
            {
                double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
                double VEL0, A0;
                double VISC;
                double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
                double ZETA, LENGTH_PIPE;
                double P_FRIC, C0, C1, C2;
                double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
                double FITTING_QUANTITY, LBYD = 1140, K;
                // Input - Globle Variables: There should be button for the following globle inputs

                //printf("Enter the value of Mass flow rate in kg/s: ");
                //scanf("%lf", &MASS_FLOW_RATE);
                //printf("Enter the value of Temperature in Kelvin: ");
                //scanf("%lf", &TEMP);
                //printf("Enter the value of Molecular Weight in kg/kmol: ");
                //scanf("%lf", &MOLEQ_WEIGHT);
                //printf("Enter the value of ratio of Secific heat Gamma: ");
                //scanf("%lf", &GAMMA);

                MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
                MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
                TEMP = Convert.ToDouble(txtTEMP.Text);
                MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
                GAMMA = Convert.ToDouble(txtGAMMA.Text);

                // Viscosity will be calculated based on Temperature and Molecular Weight

                VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
                // However, User can edit the Viscosity 
                //printf("Enter the value of Viscoaity in Pas: ");
                //scanf("%lf", &VISC);

                VISC = Convert.ToDouble(txtVISC.Text);


                //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

                //printf("Enter the value of Pipe Diameter D in m: ");
                //scanf("%lf", &PIPE_DIA);
                //printf("Enter the Quantity: ");
                //scanf("%lf", &FITTING_QUANTITY);
                //printf("Enter the value of Roughness in m: ");
                //scanf("%lf", &ROUGHNESS);

                PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);
                FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);
                ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

                PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
                ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);
                //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

                //printf("Enter the value of Upsteam Pressure in kPa: ");
                //scanf("%lf", &PST0);

                PST0 = Convert.ToDouble(txtPST0.Text);
                PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

                // All input ends here
                // ofstream file6("Output_Data.txt");
                UNIVERSAL_GAS_CONST = 8314;
                A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
                SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
                MACH = VEL0 / SOUND_VEL;

                if (MACH >= 0.7)
                {
                    MessageBox.Show("Flow is compressible please Edit the input parameters");
                    return;
                }
                RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
                if (RE <= 2300)
                {
                    a1 = Math.Pow((RE / 2712), 8.4);
                    a = (1.0 / (1 + a1));
                    a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                    b1 = Math.Pow(a1, 1.8);
                    b = (1.0 / (1 + b1));
                    a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                    FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
                }
                else
                {
                    a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                    a3 = Math.Log(RE / (1.816 * a2));
                    b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                    FRIC_FACT = Math.Pow((-2 * b1), -2);
                }
                ZETA = FRIC_FACT * LBYD;
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

                C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A0;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                DELP = (PST1 - PST0 * 1000);
                PST1 = PST1 / 1000;

                ShowOutPut(PST1, DELP);
            }
            else if (cmbTypeT == "Diaphram Valve 3by4 Open")
            {
                double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
                double VEL0, A0;
                double VISC;
                double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
                double ZETA, LENGTH_PIPE;
                double P_FRIC, C0, C1, C2;
                double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
                double FITTING_QUANTITY, LBYD = 140, K;
                // Input - Globle Variables: There should be button for the following globle inputs

                //printf("Enter the value of Mass flow rate in kg/s: ");
                //scanf("%lf", &MASS_FLOW_RATE);
                //printf("Enter the value of Temperature in Kelvin: ");
                //scanf("%lf", &TEMP);
                //printf("Enter the value of Molecular Weight in kg/kmol: ");
                //scanf("%lf", &MOLEQ_WEIGHT);
                //printf("Enter the value of ratio of Secific heat Gamma: ");
                //scanf("%lf", &GAMMA);

                MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
                MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
                TEMP = Convert.ToDouble(txtTEMP.Text);
                MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
                GAMMA = Convert.ToDouble(txtGAMMA.Text);

                // Viscosity will be calculated based on Temperature and Molecular Weight

                VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
                // However, User can edit the Viscosity 
                //printf("Enter the value of Viscoaity in Pas: ");
                //scanf("%lf", &VISC);

                VISC = Convert.ToDouble(txtVISC.Text);

                //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

                //printf("Enter the value of Pipe Diameter D in m: ");
                //scanf("%lf", &PIPE_DIA);
                //printf("Enter the Quantity: ");
                //scanf("%lf", &FITTING_QUANTITY);
                //printf("Enter the value of Roughness in m: ");
                //scanf("%lf", &ROUGHNESS);

                PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);
                FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);
                ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

                PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
                ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);

                //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

                //printf("Enter the value of Upsteam Pressure in kPa: ");
                //scanf("%lf", &PST0);

                PST0 = Convert.ToDouble(txtPST0.Text);
                PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

                // All input ends here
                // ofstream file6("Output_Data.txt");
                UNIVERSAL_GAS_CONST = 8314;
                A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
                SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
                MACH = VEL0 / SOUND_VEL;

                if (MACH >= 0.7)
                {
                    MessageBox.Show("Flow is compressible please Edit the input parameters");
                    return;
                }
                RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
                if (RE <= 2300)
                {
                    a1 = Math.Pow((RE / 2712), 8.4);
                    a = (1.0 / (1 + a1));
                    a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                    b1 = Math.Pow(a1, 1.8);
                    b = (1.0 / (1 + b1));
                    a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                    FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
                }
                else
                {
                    a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                    a3 = Math.Log(RE / (1.816 * a2));
                    b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                    FRIC_FACT = Math.Pow((-2 * b1), -2);
                }
                ZETA = FRIC_FACT * LBYD;
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

                C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A0;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                DELP = (PST1 - PST0 * 1000);
                PST1 = PST1 / 1000;

                ShowOutPut(PST1, DELP);
            }
            else if (cmbTypeT == "Diaphram Valve Fully Open")
            {
                double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
                double VEL0, A0;
                double VISC;
                double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
                double ZETA, LENGTH_PIPE;
                double P_FRIC, C0, C1, C2;
                double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
                double FITTING_QUANTITY, LBYD = 125, K;
                // Input - Globle Variables: There should be button for the following globle inputs

                //printf("Enter the value of Mass flow rate in kg/s: ");
                //scanf("%lf", &MASS_FLOW_RATE);
                //printf("Enter the value of Temperature in Kelvin: ");
                //scanf("%lf", &TEMP);
                //printf("Enter the value of Molecular Weight in kg/kmol: ");
                //scanf("%lf", &MOLEQ_WEIGHT);
                //printf("Enter the value of ratio of Secific heat Gamma: ");
                //scanf("%lf", &GAMMA);

                MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
                MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
                TEMP = Convert.ToDouble(txtTEMP.Text);
                MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
                GAMMA = Convert.ToDouble(txtGAMMA.Text);

                // Viscosity will be calculated based on Temperature and Molecular Weight

                VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
                // However, User can edit the Viscosity 
                //printf("Enter the value of Viscoaity in Pas: ");
                //scanf("%lf", &VISC);

                VISC = Convert.ToDouble(txtVISC.Text);


                //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

                //printf("Enter the value of Pipe Diameter D in m: ");
                //scanf("%lf", &PIPE_DIA);
                //printf("Enter the Quantity: ");
                //scanf("%lf", &FITTING_QUANTITY);
                //printf("Enter the value of Roughness in m: ");
                //scanf("%lf", &ROUGHNESS);

                PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);
                FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);
                ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

                PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
                ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);
                //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

                //printf("Enter the value of Upsteam Pressure in kPa: ");
                //scanf("%lf", &PST0);

                PST0 = Convert.ToDouble(txtPST0.Text);
                PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

                // All input ends here
                // ofstream file6("Output_Data.txt");
                UNIVERSAL_GAS_CONST = 8314;
                A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
                SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
                MACH = VEL0 / SOUND_VEL;

                if (MACH >= 0.7)
                {
                    MessageBox.Show("Flow is compressible please Edit the input parameters");
                    return;
                }
                RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
                if (RE <= 2300)
                {
                    a1 = Math.Pow((RE / 2712), 8.4);
                    a = (1.0 / (1 + a1));
                    a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                    b1 = Math.Pow(a1, 1.8);
                    b = (1.0 / (1 + b1));
                    a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                    FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
                }
                else
                {
                    a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                    a3 = Math.Log(RE / (1.816 * a2));
                    b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                    FRIC_FACT = Math.Pow((-2 * b1), -2);
                }
                ZETA = FRIC_FACT * LBYD;
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

                C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A0;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                DELP = (PST1 - PST0 * 1000);
                PST1 = PST1 / 1000;

                ShowOutPut(PST1, DELP);
            }
        }

        private void Foot_Valve()
        {
            double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
            double VEL0, A0;
            double VISC;
            double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
            double ZETA, LENGTH_PIPE;
            double P_FRIC, C0, C1, C2;
            double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
            double FITTING_QUANTITY, LBYD = 705, K;
            // Input - Globle Variables: There should be button for the following globle inputs

            //printf("Enter the value of Mass flow rate in kg/s: ");
            //scanf("%lf", &MASS_FLOW_RATE);
            //printf("Enter the value of Temperature in Kelvin: ");
            //scanf("%lf", &TEMP);
            //printf("Enter the value of Molecular Weight in kg/kmol: ");
            //scanf("%lf", &MOLEQ_WEIGHT);
            //printf("Enter the value of ratio of Secific heat Gamma: ");
            //scanf("%lf", &GAMMA);

            MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
            MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
            TEMP = Convert.ToDouble(txtTEMP.Text);
            MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
            GAMMA = Convert.ToDouble(txtGAMMA.Text);

            // Viscosity will be calculated based on Temperature and Molecular Weight

            VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
            // However, User can edit the Viscosity 
            //printf("Enter the value of Viscoaity in Pas: ");
            //scanf("%lf", &VISC);

            VISC = Convert.ToDouble(txtVISC.Text);

            //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

            //printf("Enter the value of Pipe Diameter D in m: ");
            //scanf("%lf", &PIPE_DIA);
            //printf("Enter the Quantity: ");
            //scanf("%lf", &FITTING_QUANTITY);
            //printf("Enter the value of Roughness in m: ");
            //scanf("%lf", &ROUGHNESS);

            PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);
            FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);
            ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

            PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
            ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);

            //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

            //printf("Enter the value of Upsteam Pressure in kPa: ");
            //scanf("%lf", &PST0);

            PST0 = Convert.ToDouble(txtPST0.Text);
            PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

            // All input ends here
            // ofstream file6("Output_Data.txt");
            UNIVERSAL_GAS_CONST = 8314;
            A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
            VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
            SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
            MACH = VEL0 / SOUND_VEL;

            if (MACH >= 0.7)
            {
                MessageBox.Show("Flow is compressible please Edit the input parameters");
                return;
            }
            RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
            NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
            if (RE <= 2300)
            {
                a1 = Math.Pow((RE / 2712), 8.4);
                a = (1.0 / (1 + a1));
                a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                b1 = Math.Pow(a1, 1.8);
                b = (1.0 / (1 + b1));
                a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
            }
            else
            {
                a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                a3 = Math.Log(RE / (1.816 * a2));
                b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                FRIC_FACT = Math.Pow((-2 * b1), -2);
            }
            ZETA = FRIC_FACT * LBYD;
            P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

            C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
            C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
            C2 = MASS_FLOW_RATE / A0;
            PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
            DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
            DELP = (PST1 - PST0 * 1000);
            PST1 = PST1 / 1000;

            ShowOutPut(PST1, DELP);
        }

        private void Plug_Cock()
        {
            string cmbTypeT = Convert.ToString(cmbType.Text);
            if (cmbTypeT == "Plug Cock theta = 5 degree")
            {
                double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
                double VEL0, A0;
                double VISC;
                double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
                double ZETA, LENGTH_PIPE;
                double P_FRIC, C0, C1, C2;
                double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
                double FITTING_QUANTITY, LBYD = 3, K;
                // Input - Globle Variables: There should be button for the following globle inputs

                //printf("Enter the value of Mass flow rate in kg/s: ");
                //scanf("%lf", &MASS_FLOW_RATE);
                //printf("Enter the value of Temperature in Kelvin: ");
                //scanf("%lf", &TEMP);
                //printf("Enter the value of Molecular Weight in kg/kmol: ");
                //scanf("%lf", &MOLEQ_WEIGHT);
                //printf("Enter the value of ratio of Secific heat Gamma: ");
                //scanf("%lf", &GAMMA);

                MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
                MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
                TEMP = Convert.ToDouble(txtTEMP.Text);
                MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
                GAMMA = Convert.ToDouble(txtGAMMA.Text);

                // Viscosity will be calculated based on Temperature and Molecular Weight

                VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
                // However, User can edit the Viscosity 
                //printf("Enter the value of Viscoaity in Pas: ");
                //scanf("%lf", &VISC);

                VISC = Convert.ToDouble(txtVISC.Text);

                //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

                //printf("Enter the value of Pipe Diameter D in m: ");
                //scanf("%lf", &PIPE_DIA);
                //printf("Enter the Quantity: ");
                //scanf("%lf", &FITTING_QUANTITY);
                //printf("Enter the value of Roughness in m: ");
                //scanf("%lf", &ROUGHNESS);

                PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);
                FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);
                ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

                PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
                ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);
                //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

                //printf("Enter the value of Upsteam Pressure in kPa: ");
                //scanf("%lf", &PST0);

                PST0 = Convert.ToDouble(txtPST0.Text);
                PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

                // All input ends here
                // ofstream file6("Output_Data.txt");
                UNIVERSAL_GAS_CONST = 8314;
                A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
                SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
                MACH = VEL0 / SOUND_VEL;

                if (MACH >= 0.7)
                {
                    MessageBox.Show("Flow is compressible please Edit the input parameters");
                    return;
                }
                RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
                if (RE <= 2300)
                {
                    a1 = Math.Pow((RE / 2712), 8.4);
                    a = (1.0 / (1 + a1));
                    a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                    b1 = Math.Pow(a1, 1.8);
                    b = (1.0 / (1 + b1));
                    a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                    FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
                }
                else
                {
                    a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                    a3 = Math.Log(RE / (1.816 * a2));
                    b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                    FRIC_FACT = Math.Pow((-2 * b1), -2);
                }
                ZETA = FRIC_FACT * LBYD;
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

                C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A0;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                DELP = (PST1 - PST0 * 1000);
                PST1 = PST1 / 1000;

                ShowOutPut(PST1, DELP);
            }
            else if (cmbTypeT == "Plug Cock theta = 10 degree")
            {
                double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
                double VEL0, A0;
                double VISC;
                double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
                double ZETA, LENGTH_PIPE;
                double P_FRIC, C0, C1, C2;
                double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
                double FITTING_QUANTITY, LBYD = 16, K;
                // Input - Globle Variables: There should be button for the following globle inputs

                //printf("Enter the value of Mass flow rate in kg/s: ");
                //scanf("%lf", &MASS_FLOW_RATE);
                //printf("Enter the value of Temperature in Kelvin: ");
                //scanf("%lf", &TEMP);
                //printf("Enter the value of Molecular Weight in kg/kmol: ");
                //scanf("%lf", &MOLEQ_WEIGHT);
                //printf("Enter the value of ratio of Secific heat Gamma: ");
                //scanf("%lf", &GAMMA);

                MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
                MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
                TEMP = Convert.ToDouble(txtTEMP.Text);
                MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
                GAMMA = Convert.ToDouble(txtGAMMA.Text);

                // Viscosity will be calculated based on Temperature and Molecular Weight

                VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
                // However, User can edit the Viscosity 
                //printf("Enter the value of Viscoaity in Pas: ");
                //scanf("%lf", &VISC);

                VISC = Convert.ToDouble(txtVISC.Text);

                //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

                //printf("Enter the value of Pipe Diameter D in m: ");
                //scanf("%lf", &PIPE_DIA);
                //printf("Enter the Quantity: ");
                //scanf("%lf", &FITTING_QUANTITY);
                //printf("Enter the value of Roughness in m: ");
                //scanf("%lf", &ROUGHNESS);

                PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);
                FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);
                ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

                PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
                ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);

                //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

                //printf("Enter the value of Upsteam Pressure in kPa: ");
                //scanf("%lf", &PST0);

                PST0 = Convert.ToDouble(txtPST0.Text);
                PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

                // All input ends here
                // ofstream file6("Output_Data.txt");
                UNIVERSAL_GAS_CONST = 8314;
                A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
                SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
                MACH = VEL0 / SOUND_VEL;

                if (MACH >= 0.7)
                {
                    MessageBox.Show("Flow is compressible please Edit the input parameters");
                    return;
                }
                RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
                if (RE <= 2300)
                {
                    a1 = Math.Pow((RE / 2712), 8.4);
                    a = (1.0 / (1 + a1));
                    a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                    b1 = Math.Pow(a1, 1.8);
                    b = (1.0 / (1 + b1));
                    a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                    FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
                }
                else
                {
                    a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                    a3 = Math.Log(RE / (1.816 * a2));
                    b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                    FRIC_FACT = Math.Pow((-2 * b1), -2);
                }
                ZETA = FRIC_FACT * LBYD;
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

                C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A0;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                DELP = (PST1 - PST0 * 1000);
                PST1 = PST1 / 1000;
                ShowOutPut(PST1, DELP);
            }
            else if (cmbTypeT == "Plug Cock theta = 20 degree")
            {
                double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
                double VEL0, A0;
                double VISC;
                double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
                double ZETA, LENGTH_PIPE;
                double P_FRIC, C0, C1, C2;
                double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
                double FITTING_QUANTITY, LBYD = 85, K;
                // Input - Globle Variables: There should be button for the following globle inputs

                //printf("Enter the value of Mass flow rate in kg/s: ");
                //scanf("%lf", &MASS_FLOW_RATE);
                //printf("Enter the value of Temperature in Kelvin: ");
                //scanf("%lf", &TEMP);
                //printf("Enter the value of Molecular Weight in kg/kmol: ");
                //scanf("%lf", &MOLEQ_WEIGHT);
                //printf("Enter the value of ratio of Secific heat Gamma: ");
                //scanf("%lf", &GAMMA);

                MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
                MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
                TEMP = Convert.ToDouble(txtTEMP.Text);
                MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
                GAMMA = Convert.ToDouble(txtGAMMA.Text);

                // Viscosity will be calculated based on Temperature and Molecular Weight

                VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
                // However, User can edit the Viscosity 
                //printf("Enter the value of Viscoaity in Pas: ");
                //scanf("%lf", &VISC);

                VISC = Convert.ToDouble(txtVISC.Text);

                //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

                //printf("Enter the value of Pipe Diameter D in m: ");
                //scanf("%lf", &PIPE_DIA);
                //printf("Enter the Quantity: ");
                //scanf("%lf", &FITTING_QUANTITY);
                //printf("Enter the value of Roughness in m: ");
                //scanf("%lf", &ROUGHNESS);

                PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);
                FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);
                ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

                PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
                ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);
                //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

                //printf("Enter the value of Upsteam Pressure in kPa: ");
                //scanf("%lf", &PST0);

                PST0 = Convert.ToDouble(txtPST0.Text);
                PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

                // All input ends here
                // ofstream file6("Output_Data.txt");
                UNIVERSAL_GAS_CONST = 8314;
                A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
                SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
                MACH = VEL0 / SOUND_VEL;

                if (MACH >= 0.7)
                {
                    MessageBox.Show("Flow is compressible please Edit the input parameters");
                    return;
                }
                RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
                if (RE <= 2300)
                {
                    a1 = Math.Pow((RE / 2712), 8.4);
                    a = (1.0 / (1 + a1));
                    a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                    b1 = Math.Pow(a1, 1.8);
                    b = (1.0 / (1 + b1));
                    a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                    FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
                }
                else
                {
                    a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                    a3 = Math.Log(RE / (1.816 * a2));
                    b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                    FRIC_FACT = Math.Pow((-2 * b1), -2);
                }
                ZETA = FRIC_FACT * LBYD;
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

                C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A0;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                DELP = (PST1 - PST0 * 1000);
                PST1 = PST1 / 1000;
                ShowOutPut(PST1, DELP);
            }
            else if (cmbTypeT == "Plug Cock theta = 40 degree")
            {
                double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
                double VEL0, A0;
                double VISC;
                double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
                double ZETA, LENGTH_PIPE;
                double P_FRIC, C0, C1, C2;
                double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
                double FITTING_QUANTITY, LBYD = 950, K;
                // Input - Globle Variables: There should be button for the following globle inputs

                //printf("Enter the value of Mass flow rate in kg/s: ");
                //scanf("%lf", &MASS_FLOW_RATE);
                //printf("Enter the value of Temperature in Kelvin: ");
                //scanf("%lf", &TEMP);
                //printf("Enter the value of Molecular Weight in kg/kmol: ");
                //scanf("%lf", &MOLEQ_WEIGHT);
                //printf("Enter the value of ratio of Secific heat Gamma: ");
                //scanf("%lf", &GAMMA);

                MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
                MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
                TEMP = Convert.ToDouble(txtTEMP.Text);
                MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
                GAMMA = Convert.ToDouble(txtGAMMA.Text);

                // Viscosity will be calculated based on Temperature and Molecular Weight

                VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
                // However, User can edit the Viscosity 
                //printf("Enter the value of Viscoaity in Pas: ");
                //scanf("%lf", &VISC);

                VISC = Convert.ToDouble(txtVISC.Text);

                //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

                //printf("Enter the value of Pipe Diameter D in m: ");
                //scanf("%lf", &PIPE_DIA);
                //printf("Enter the Quantity: ");
                //scanf("%lf", &FITTING_QUANTITY);
                //printf("Enter the value of Roughness in m: ");
                //scanf("%lf", &ROUGHNESS);

                PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);
                FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);
                ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

                PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
                ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);
                //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

                //printf("Enter the value of Upsteam Pressure in kPa: ");
                //scanf("%lf", &PST0);

                PST0 = Convert.ToDouble(txtPST0.Text);
                PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

                // All input ends here
                // ofstream file6("Output_Data.txt");
                UNIVERSAL_GAS_CONST = 8314;
                A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
                SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
                MACH = VEL0 / SOUND_VEL;

                if (MACH >= 0.7)
                {
                    MessageBox.Show("Flow is compressible please Edit the input parameters");
                    return;
                }
                RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
                if (RE <= 2300)
                {
                    a1 = Math.Pow((RE / 2712), 8.4);
                    a = (1.0 / (1 + a1));
                    a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                    b1 = Math.Pow(a1, 1.8);
                    b = (1.0 / (1 + b1));
                    a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                    FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
                }
                else
                {
                    a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                    a3 = Math.Log(RE / (1.816 * a2));
                    b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                    FRIC_FACT = Math.Pow((-2 * b1), -2);
                }
                ZETA = FRIC_FACT * LBYD;
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

                C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A0;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                DELP = (PST1 - PST0 * 1000);
                PST1 = PST1 / 1000;
                ShowOutPut(PST1, DELP);
            }
            else if (cmbTypeT == "Plug Cock theta = 60 degree")
            {
                double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
                double VEL0, A0;
                double VISC;
                double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
                double ZETA, LENGTH_PIPE;
                double P_FRIC, C0, C1, C2;
                double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
                double FITTING_QUANTITY, LBYD = 11200, K;
                // Input - Globle Variables: There should be button for the following globle inputs

                //printf("Enter the value of Mass flow rate in kg/s: ");
                //scanf("%lf", &MASS_FLOW_RATE);
                //printf("Enter the value of Temperature in Kelvin: ");
                //scanf("%lf", &TEMP);
                //printf("Enter the value of Molecular Weight in kg/kmol: ");
                //scanf("%lf", &MOLEQ_WEIGHT);
                //printf("Enter the value of ratio of Secific heat Gamma: ");
                //scanf("%lf", &GAMMA);

                MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
                MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
                TEMP = Convert.ToDouble(txtTEMP.Text);
                MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
                GAMMA = Convert.ToDouble(txtGAMMA.Text);

                // Viscosity will be calculated based on Temperature and Molecular Weight

                VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
                // However, User can edit the Viscosity 
                //printf("Enter the value of Viscoaity in Pas: ");
                //scanf("%lf", &VISC);

                VISC = Convert.ToDouble(txtVISC.Text);

                //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

                //printf("Enter the value of Pipe Diameter D in m: ");
                //scanf("%lf", &PIPE_DIA);
                //printf("Enter the Quantity: ");
                //scanf("%lf", &FITTING_QUANTITY);
                //printf("Enter the value of Roughness in m: ");
                //scanf("%lf", &ROUGHNESS);

                PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);
                FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);
                ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

                PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
                ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);
                //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

                //printf("Enter the value of Upsteam Pressure in kPa: ");
                //scanf("%lf", &PST0);

                PST0 = Convert.ToDouble(txtPST0.Text);
                PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

                // All input ends here
                // ofstream file6("Output_Data.txt");
                UNIVERSAL_GAS_CONST = 8314;
                A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
                SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
                MACH = VEL0 / SOUND_VEL;

                if (MACH >= 0.7)
                {
                    MessageBox.Show("Flow is compressible please Edit the input parameters");
                    return;
                }
                RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
                NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
                if (RE <= 2300)
                {
                    a1 = Math.Pow((RE / 2712), 8.4);
                    a = (1.0 / (1 + a1));
                    a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                    b1 = Math.Pow(a1, 1.8);
                    b = (1.0 / (1 + b1));
                    a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                    FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
                }
                else
                {
                    a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                    a3 = Math.Log(RE / (1.816 * a2));
                    b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                    FRIC_FACT = Math.Pow((-2 * b1), -2);
                }
                ZETA = FRIC_FACT * LBYD;
                P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

                C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
                C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
                C2 = MASS_FLOW_RATE / A0;
                PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
                DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
                DELP = (PST1 - PST0 * 1000);
                PST1 = PST1 / 1000;
                ShowOutPut(PST1, DELP);
            }
        }

        private void Union()
        {
            double PIPE_DIA, MOLEQ_WEIGHT, MASS_FLOW_RATE, TEMP, UNIVERSAL_GAS_CONST, PST0, PST1, DELP, RE, FRIC_FACT, DEN0, DEN1;
            double VEL0, A0;
            double VISC;
            double a, b, ROUGHNESS, NONDIM_ROUGHNESS;
            double ZETA, LENGTH_PIPE;
            double P_FRIC, C0, C1, C2;
            double a1, a2, a3, b1, GAMMA, SOUND_VEL, MACH;
            double FITTING_QUANTITY, LBYD = 2, K;
            // Input - Globle Variables: There should be button for the following globle inputs

            //printf("Enter the value of Mass flow rate in kg/s: ");
            //scanf("%lf", &MASS_FLOW_RATE);
            //printf("Enter the value of Temperature in Kelvin: ");
            //scanf("%lf", &TEMP);
            //printf("Enter the value of Molecular Weight in kg/kmol: ");
            //scanf("%lf", &MOLEQ_WEIGHT);
            //printf("Enter the value of ratio of Secific heat Gamma: ");
            //scanf("%lf", &GAMMA);

            MASS_FLOW_RATE = Convert.ToDouble(txtMASS_FLOW_RATE.Text);
            MASS_FLOW_RATE = ConvertMASS_FLOW_RATEAsPerMeasure(MASS_FLOW_RATE);
            TEMP = Convert.ToDouble(txtTEMP.Text);
            MOLEQ_WEIGHT = Convert.ToDouble(txtMOLEQ_WEIGHT.Text);
            GAMMA = Convert.ToDouble(txtGAMMA.Text);

            // Viscosity will be calculated based on Temperature and Molecular Weight

            VISC = (((1.936 - 8.85e-3 * MOLEQ_WEIGHT) * Math.Pow(10, -5) * (1.8 * TEMP - 59.67)) + (1.0 / (76.179 + 1.032 * MOLEQ_WEIGHT))) * 0.001;
            // However, User can edit the Viscosity 
            //printf("Enter the value of Viscoaity in Pas: ");
            //scanf("%lf", &VISC);

            VISC = Convert.ToDouble(txtVISC.Text);

            //Input - Specific Variables pertaining to fittings: There should be popup button for the following specific inputs

            //printf("Enter the value of Pipe Diameter D in m: ");
            //scanf("%lf", &PIPE_DIA);
            //printf("Enter the Quantity: ");
            //scanf("%lf", &FITTING_QUANTITY);
            //printf("Enter the value of Roughness in m: ");
            //scanf("%lf", &ROUGHNESS);

            PIPE_DIA = Convert.ToDouble(txtPIPE_DIA.Text);
            FITTING_QUANTITY = Convert.ToDouble(txtFITTING_QUANTITY.Text);
            ROUGHNESS = Convert.ToDouble(txtROUGHNESS.Text);

            PIPE_DIA = ConvertMtoMMPerMeasure(PIPE_DIA, cmbPipeDiaMes);
            ROUGHNESS = ConvertMtoMMPerMeasure(ROUGHNESS, cmbRoughMes);
            //Input - required to be taken from the output of the previous subroutine: Input by the User in the first subroutine

            //printf("Enter the value of Upsteam Pressure in kPa: ");
            //scanf("%lf", &PST0);

            PST0 = Convert.ToDouble(txtPST0.Text);
            PST0 = ConvertPST0AsPerMeasure(PST0, cmbPST0Mes);

            // All input ends here
            // ofstream file6("Output_Data.txt");
            UNIVERSAL_GAS_CONST = 8314;
            A0 = (PI * PIPE_DIA * PIPE_DIA / 4); DEN0 = (PST0 * 1000 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
            VEL0 = (MASS_FLOW_RATE / (A0 * DEN0));
            SOUND_VEL = Math.Sqrt(GAMMA * UNIVERSAL_GAS_CONST * TEMP / MOLEQ_WEIGHT);
            MACH = VEL0 / SOUND_VEL;

            if (MACH >= 0.7)
            {
                MessageBox.Show("Flow is compressible please Edit the input parameters");
                return;
            }
            RE = (DEN0 * VEL0 * PIPE_DIA) / VISC;
            NONDIM_ROUGHNESS = ROUGHNESS / PIPE_DIA;
            if (RE <= 2300)
            {
                a1 = Math.Pow((RE / 2712), 8.4);
                a = (1.0 / (1 + a1));
                a1 = ((RE / 150) * NONDIM_ROUGHNESS);
                b1 = Math.Pow(a1, 1.8);
                b = (1.0 / (1 + b1));
                a1 = Math.Pow((64 / RE), a); a2 = 0.75 * (Math.Log(RE / 5.37)); a3 = 0.88 * (Math.Log(6.82 / NONDIM_ROUGHNESS));
                FRIC_FACT = (a1) * (Math.Pow(a2, (2 * b * (a - 1)))) * (Math.Pow(a3, (2 * (1 - b) * (a - 1))));
            }
            else
            {
                a1 = Math.Log(1 + 1.1 * RE); a2 = Math.Log(1.1 * RE / a1);
                a3 = Math.Log(RE / (1.816 * a2));
                b1 = (1.0 / 2.303) * Math.Log((2.18 * a3 / RE) + (NONDIM_ROUGHNESS / 3.71));
                FRIC_FACT = Math.Pow((-2 * b1), -2);
            }
            ZETA = FRIC_FACT * LBYD;
            P_FRIC = 0.5 * ZETA * DEN0 * VEL0 * VEL0 * FITTING_QUANTITY;

            C0 = PST0 * 1000 + 0.5 * DEN0 * VEL0 * VEL0 + P_FRIC;
            C1 = MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP);
            C2 = MASS_FLOW_RATE / A0;
            PST1 = (C0 + Math.Sqrt((C0 * C0) - (4 * C2 * C2 / (2 * C1)))) / 2;
            DEN1 = (PST1 * MOLEQ_WEIGHT / (UNIVERSAL_GAS_CONST * TEMP));
            DELP = (PST1 - PST0 * 1000);
            PST1 = PST1 / 1000;
            ShowOutPut(PST1, DELP);
        }

        private void btnFinalOP_Click(object sender, EventArgs e)
        {
            dgvGridFinalOutPut.DataSource = null;
            this.Width = 1540;
            DataTable dt = new DataTable();
            dt.Columns.Add("Fitting");
            dt.Columns.Add("Type");
            dt.Columns.Add("PST0");
            dt.Columns.Add("DELP");
            double sumPST0 = 0;
            double sumDELP = 0;
            for (int i = 0; i < CommonClass.objFitting.Count; i++)
            {
                DataRow dr = dt.NewRow();
                dr["Fitting"] = Convert.ToString(CommonClass.objFitting[i]);
                dr["Type"] = Convert.ToString(CommonClass.objType[i]);
                dr["PST0"] = Convert.ToString(CommonClass.objPST0[i]);
                dr["DELP"] = Convert.ToString(CommonClass.objDELP[i]);
                dt.Rows.Add(dr);
                try
                {
                    sumPST0 = sumPST0 + Convert.ToDouble(dr["PST0"]);
                    sumDELP = sumDELP + Convert.ToDouble(dr["DELP"]);
                }
                catch (Exception)
                {
                }
            }

            DataRow dr1 = dt.NewRow();
            dr1["Fitting"] = "";
            dr1["Type"] = "TOTAL : ";
            dr1["PST0"] = Convert.ToString(sumPST0);
            dr1["DELP"] = Convert.ToString(sumDELP);
            dt.Rows.Add(dr1);

            dgvGridFinalOutPut.DataSource = dt;
        }
    }
}
